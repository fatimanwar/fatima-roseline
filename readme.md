# Overview #

The project aims to develop a system stack that enables new ways for clock hardware, OS, network services, and applications to learn, maintain and exchange information about time, influence component behavior, and robustly adapt to dynamic QoT requirements, as well as to benign and adversarial changes in operating conditions.  

Application areas that will benefit from Quality of Time will include: smart grids, networked and coordinated control of aerospace systems, underwater sensing, and industrial automation. The broader impact is to build a robust and tunable quality of time that can be applied across a broad spectrum of applications that pervade modern life. 

# License #

Copyright (c) Regents of the University of California, 2015.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Installation and Usage #

Please refer to the [wiki](https://bitbucket.org/rose-line/roseline/wiki).

# Contributors #

**University of California Los Angeles (UCLA)**

* Mani Srivastava
* Sudhakar Pamarti
* Andrew Symington
* Lucas Wanner
* Hani Ismaelzadeh
* Amr Alanwar
* Fatima Muhammad
* Paul Martin

**Carnegie-Mellon University (CMU)**

* Anthony Rowe
* Raj Rajkumar
* Adwait Dongare

**University of California San Diego (UCSD)**

* Rajesh Gupta
* Balkrishnan Narayanaswamy
* Sean Hamilton
* Fang Zhou
* Muhammad Adnan

**University of Utah**

* Neal Patwari
* Thomas Schmid
* Anh Luong

**University of California Santa Barbara (UCSB)**

* Joao Hespanha
* Justin Pearson

# Folder layout #

* catkin - ROS-based application code for the QoT stack
* core - kernel images, bootloader, patches, modules and root filesystem
* misc - programming examples for the QoT stack
* scheduler - google go application programmer interface for QoT
* timesync - time synchronization application

# Support #

Questions can be directed to Andrew Symington (asymingt@ucla.edu).