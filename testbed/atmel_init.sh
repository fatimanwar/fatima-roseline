#!/bin/bash
rmmod at86rf230
rmmod dw1000
modprobe at86rf230
iwpan wpan0 set pan_id 0xae70
iwpan wpan0 set short_addr $1
ip link add link wpan0 name lowpan0 type lowpan
ip addr add feed:feed:feed:feed::$1/64 dev lowpan0
ip link set wpan0 up
ip link set lowpan0 up
