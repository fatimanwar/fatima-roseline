#ifndef QOT_IOCTL_H
#define QOT_IOCTL_H

#include <linux/ioctl.h>
#include <linux/idr.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/posix-clock.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/hashtable.h>
#include <linux/types.h>
#include <linux/hash.h>

/** clock source structure **/
typedef struct qot_clock_t {
	uint16_t timeline;			// UUID of reference timeline shared among all collaborating entities
	struct timespec accuracy;	// range of acceptable deviation from the reference timeline
	struct timespec resolution;	// required clock resolution
	uint16_t binding_id;		// id returned once the clock is iniatialized
} qot_clock;

/** scheduling parameters for an event **/
typedef struct qot_sched_t {
	uint16_t clk_timeline;		// schedule against this clock
	struct timespec event_time;	// time for which event is scheduled
	uint64_t event_cycles;		// wait cycles
	struct timespec high;		// time for high pulse
	struct timespec low;		// time for low pulse
	uint16_t limit;				// number of event repetitions
} qot_sched;

/** structure used to query time and return current time **/
typedef struct qot_time_t {
	uint16_t clk_timeline;			// timeline to get time from
	struct timespec current_time;	// timespec storing current time
} qot_time;

/** clock data used by the kernel driver **/
struct qot_timeline_data {
    struct posix_clock clock;
    struct device *dev;
    qot_clock *info;						// clock metadata
	int references;							// number of references to the timeline
    dev_t devid;
    int index;                      		// index into clocks.map
    int defunct;                    		// tells readers to go away when clock is being removed
    struct hlist_node accuracy_sort_hash;	// pointer to next sorted clock in the list w.r.t accuracy
    struct list_head resolution_sort_list;	// pointer to next sorted clock in the list w.r.t resolution
};

/** function calls to create / destory QoT clock device **/

extern struct qot_timeline_data *qot_clock_register(struct qot_clock *info);
extern int qot_clock_unregister(struct qot_timeline_data *qotclk);
extern int qot_clock_index(struct qot_timeline_data *qotclk);

/** unique code for ioctl **/
#define MAGIC_CODE 0xAB

/** read / write clock and schedule parameters **/
#define QOT_INIT_CLOCK  		_IOW(MAGIC_CODE, 0, qot_clock*)
#define QOT_RETURN_BINDING  	_IOR(MAGIC_CODE, 1, uint16_t*)
#define QOT_RELEASE_CLOCK       _IOW(MAGIC_CODE, 2, uint16_t*)
#define QOT_SET_ACCURACY 	    _IOW(MAGIC_CODE, 3, qot_clock*)
#define QOT_SET_RESOLUTION 	    _IOW(MAGIC_CODE, 4, qot_clock*)
#define QOT_SET_SCHED 	        _IOW(MAGIC_CODE, 5, qot_sched*)
#define QOT_GET_SCHED       	_IOR(MAGIC_CODE, 6, qot_sched*)
#define QOT_WAIT				_IOW(MAGIC_CODE, 7, qot_sched*)
#define QOT_WAIT_CYCLES			_IOW(MAGIC_CODE, 8, qot_sched*)

/** read clock metadata from virtual qot clocks **/
#define QOT_GET_TIMELINE_ID		_IOR(MAGIC_CODE, 9, uint16_t*)
#define QOT_GET_ACCURACY		_IOR(MAGIC_CODE, A, timespec*)
#define QOT_GET_RESOLUTION		_IOR(MAGIC_CODE, B, timespec*)
	
#endif
