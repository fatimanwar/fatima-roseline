/*
 *  kmod-usrcc.c - Enables user-space access to ARM cycle counter.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */
#define DRIVER_AUTHOR "Sean Hamilton <skhamilt@eng.ucsd.edu>"
#define DRIVER_DESC   "Enables user-space access to ARM cycle counter"
#define DRIVER_VER    "0.1.1"
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_VERSION(DRIVER_VER);

/*
 * Get rid of taint message by declaring code as GPL.
 */
MODULE_LICENSE("GPL");

/* asm() usage guide
 * asm(code : output operand list : input operand list : clobber list)
 * code 		-	string containing valid assembly instructions
 * output		-	optional comma seperated list of output operands
 *						[result] "=r" (y)
 * input 		-	optional comma seperated list of input operands
 *						[value] "r" (x)
 * clobber	- optional list of clobbered registers (Optimization)
 */

 /* To use the cycle counter, the following functions can be used
  *
	*	static inline unsigned int get_cyclecount(void)
	*	{
	*		unsigned int value;
	*
	*		// Read CCNT Register
	*		asm volatile ("MRC p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
	*
	*		return value;
	*	}

	*	static inline void init_perfcounters(int32_t do_reset, int32_t enable_divider)
	*	{
	*		// in general enable all counters(including cycle counter)
	*		int32_t value = 1;

	*		// peform reset:
	*		if(do_reset)
	*		{
	*			value |= 2;     // reset all counters to zero.
	*			value |= 4;     // reset cycle counter to zero.
	*		}

	*		if(enable_divider)
	*			value |= 8;     // enable "by 64" divider for CCNT.

	*		value |= 16;

	*		// program the performance-counter control-register:
	*		asm volatile ("MCR p15, 0, %0, c9, c12, 0\t\n" :: "r"(value));

	*		// enable all counters:
	*		asm volatile ("MCR p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000000f));

	*		// clear overflows:
	*		asm volatile ("MCR p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000000f));
	*	}
  */


int init_module(void)
{
	printk(KERN_INFO "Loading kmod-usrcc\n");

	/* enable user-mode access to the performance counter */
	asm ("MCR p15, 0, %0, C9, C14, 0\n\t" :: "r"(1));

	/* disable counter overflow interrupts (just in case)*/
	asm ("MCR p15, 0, %0, C9, C14, 2\n\t" :: "r"(0x8000000f));

	/*
	 * A non 0 return means init_module failed; module can't be loaded.
	 */
	return 0;
}

void cleanup_module(void)
{
	/* disable user-mode access to the performance counter */
	asm ("MCR p15, 0, %0, C9, C14, 0\n\t" :: "r"(1));

	printk(KERN_INFO "Unloading kmod-usrcc\n");
}
