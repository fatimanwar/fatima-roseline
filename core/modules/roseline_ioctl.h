#ifndef ROSELINE_IOCTL_H
#define ROSELINE_IOCTL_H

#include <linux/ioctl.h>

// Timer lookups
typedef enum {
	IOTIMER_A  = 0,
	IOTIMER_B  = 1,
	NUM_TIMERS = 2
} TimerName;

typedef enum {
	TIMER4_REG = 0x48040000,	// MMAP: hardware register for Timer 4
	TIMER5_REG = 0x48060000,	// MMAP: hardware register for Timer 5
	TIMER6_REG = 0x48080000,	// MMAP: hardware register for Timer 6
	TIMER7_REG = 0x480A0000		// MMAP: hardware register for Timer 7
} TimerReg;

// Capture type
typedef struct roseline_capt_t {
	uint8_t  id;						// Timer ID (must be specified for requesr)
	uint32_t frequency;					// Frequency of oscillator
	uint32_t capture;					// Number of captures
	uint32_t overflow;					// Overflow
	uint32_t count_at_request;			// Current count
	uint32_t count_at_interrupt;		// (Overflow << 8) | count_at_interrupt
	uint32_t count_at_capture;			// (Overflow << 8) | count_at_capture
}  roseline_capt;

// Capture type
typedef struct roseline_comp_t {
	uint8_t  id;						// Timer ID
	uint8_t  enable;					// [0] = disable, [1] = enabled
	uint64_t next;						// Time of next event (clock ticks)
	uint32_t cycles_high;				// High cycle time (clock ticks)
	uint32_t cycles_low;				// Low cycle time (clock ticks)
	uint32_t limit;						// Number of repetition (0 = infinite)
}  roseline_comp;

// Sync type
typedef struct roseline_sync_t {
	uint8_t id;							// Timer ID (must be specified for request)
	uint64_t seq;						// Sequence number
	uint32_t overflow;					// Current overflow (needed for timer MMAP)
	uint32_t frequency;					// Nominal frequency (needed for timer MMAP)
	double t0;							// Reference epoch
	double pars[3];						// Parameters
	double vars[3];						// Variances
}  roseline_sync;

// Parameters type
typedef struct roseline_pars_t {
	uint8_t id;							// Timer ID (must be specified for request)
	uint64_t seq;						// Sequence number
	double error;						// Target error
} roseline_pars;

// This must be a numebr unique to our code
#define MAGIC_CODE 0xEF

// Get and set radio data
#define ROSELINE_GET_CAPTURE  		_IOR(MAGIC_CODE, 0, roseline_capt*)
#define ROSELINE_SET_COMPARE        _IOR(MAGIC_CODE, 1, roseline_comp*)
#define ROSELINE_GET_SYNC 	        _IOR(MAGIC_CODE, 2, roseline_sync*)
#define ROSELINE_SET_SYNC       	_IOW(MAGIC_CODE, 3, roseline_sync*)
#define ROSELINE_GET_PARS 	        _IOR(MAGIC_CODE, 4, roseline_pars*)
#define ROSELINE_SET_PARS       	_IOW(MAGIC_CODE, 5, roseline_pars*)

#endif