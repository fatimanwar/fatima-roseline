#!/bin/bash

# Download toolchain and kernel
echo "> Downloading compiler..."
if [ ! -f gcc-linaro-4.9-2014.11-x86_64_arm-linux-gnueabihf.tar.xz ]; then
	wget https://releases.linaro.org/14.11/components/toolchain/binaries/arm-linux-gnueabihf/gcc-linaro-4.9-2014.11-x86_64_arm-linux-gnueabihf.tar.xz
fi

# Unzip and create symbolic links
echo "> Removing old builds..."
if [ -d compiler ]; then
	mv compiler compiler-old
fi

# Unzip and create symbolic links
echo "> Unzipping compiler..."
tar -xJf gcc-linaro-4.9-2014.11-x86_64_arm-linux-gnueabihf.tar.xz
mv gcc-linaro-4.9-2014.11-x86_64_arm-linux-gnueabihf compiler

# Success!
echo "> DONE"

