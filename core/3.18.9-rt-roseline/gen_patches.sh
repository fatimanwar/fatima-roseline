#!/bin/bash                               
FOLDER_OLD="./linux-3.18.9"
FOLDER_NEW="./linux"
FOLDER_PATCHES="./patchset"

# Download toolchain and kernel
echo "> Downloading vanilla kernel..."
if [ ! -f linux-3.18.9.tar.xz ]; then
        wget https://www.kernel.org/pub/linux/kernel/v3.x/linux-3.18.9.tar.xz
fi
if [ ! -f patch-3.18.9-rt4.patch.xz ]; then
        wget https://www.kernel.org/pub/linux/kernel/projects/rt/3.18/older/patch-3.18.9-rt4.patch.xz
fi

# Housekeeping
echo "> Removing old kernel version..."
if [ -d linux-3.18.9 ]; then
    rm -rf linux-3.18.9
fi
# Unzip and create symbolic links
echo "> Unzipping archives..."
rm -rf $FOLDER_OLD
mkdir -p $FOLDER_OLD
tar -xJf linux-3.18.9.tar.xz -C $FOLDER_OLD --strip 1

# Get into the Linux directory
pushd $FOLDER_OLD

# Apply RT patchset to vanilla kernel
echo "> Applying RT patch..."
xzcat ../patch-3.18.9-rt4.patch.xz | patch -p1

# Get out of the directory
popd 

# Generate a changelog
echo "Generating changelog for $FOLDER_OLD --> $FOLDER_NEW"
diff -qdr ${FOLDER_OLD} ${FOLDER_NEW} | sed "s/^.* and \(.*\) differ/\1/" | sort > newchanges.txt

mkdir -p ${FOLDER_PATCHES}                                                                                                           
for newfile in `find $FOLDER_NEW -type f`
do
sufix=${newfile#${FOLDER_NEW}/}
oldfile="${FOLDER_OLD}/${sufix}"

if [ -f $oldfile ]; then
    patchfile="${FOLDER_PATCHES}/`echo $sufix |sed 's/\//_/g'`.patch"
    if [ "`diff $oldfile $newfile`" != "" ]; then
        echo "Make patch ${patchfile}"
        diff -dupN "$oldfile" "$newfile" > "$patchfile"
    fi
else
patchfile="${FOLDER_PATCHES}/ADDONS/${newfile#$FOLDER_NEW}"
    echo "Copy $newfile --> $patchfile"
    mkdir -p "`dirname ${patchfile}`"
    cp $newfile $patchfile
fi
done