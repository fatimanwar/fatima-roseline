#!/bin/bash

# Download toolchain and kernel
echo "> Downloading archives..."
if [ ! -f linux-3.18.9.tar.xz ]; then
	wget https://www.kernel.org/pub/linux/kernel/v3.x/linux-3.18.9.tar.xz
fi
if [ ! -f patch-3.18.9-rt4.patch.xz ]; then
	wget https://www.kernel.org/pub/linux/kernel/projects/rt/3.18/older/patch-3.18.9-rt4.patch.xz
fi

# Unzip and create symbolic links
echo "> Removing old builds..."
if [ -f linux ]; then
	mv linux linux-old
fi

# Unzip and create symbolic links
echo "> Unzipping archives..."
tar -xJf linux-3.18.9.tar.xz
mv linux-3.18.9 linux

# Get into the Linux directory
pushd linux

# Apply RT patchset to vanilla kernel
echo "> Applying RT patch..."
xzcat ../patch-3.18.9-rt4.patch.xz | patch -p1

# Apply patchset to kernel to enable timers
echo "> Applying ROSELINE patch set..."
for i in ../patchset/*.patch; do patch -p2 < $i; done

# Download beaglebone black firmware
echo "> Downloading BBB firmware..."
wget http://arago-project.org/git/projects/?p=am33x-cm3.git\;a=blob_plain\;f=bin/am335x-pm-firmware.bin\;hb=HEAD -O firmware/am335x-pm-firmware.bin

# Copy over kernel configuration
echo "> Copying kernel configuration..."
cp ../kernel.config .config
make ARCH=arm oldconfig

# Build kernel, modules and device tree overlay
echo "> Building kernel, dts and modules..."
make ARCH=arm zImage dtbs modules -j4

# Get back in the base directory
popd

# Success
echo "> DONE!"
