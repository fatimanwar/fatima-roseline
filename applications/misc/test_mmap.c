#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>

int main ( int argc, char **argv )
{
	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	volatile uint32_t *t4r = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x48040000); 
	volatile uint32_t *t5r = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x48046000); 
	volatile uint32_t *t6r = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x48048000); 
	printf("T4: %lu\n",(long unsigned int)t4r[0x3c/4]);
	printf("T5: %lu\n",(long unsigned int)t5r[0x3c/4]);
	printf("T6: %lu\n",(long unsigned int)t6r[0x3c/4]);
	close(fd);
	return 0;
}
