// ROSELINE demo ultrasonic detector

// KNOWN BUGS:
// every once in awhile the RMS value is very large
// but this only occurs when there is a signal
// so it will probably not be an issue.

// TODO:
// Characterize this system's processing delay between
// a 40kHz signal sent and when the GPIO pin is set HIGH

// NOTES:
// currently there is no filter of the signal that occurs
// in software but could be added if it is necessary.
// Though we may not have enough processing power to perform
// filtering too. Currently a new sample interrupt fires
// every 6.5 us. This leaves us with 104 cycles to compute
// everything before a new sample is ready with the uC 
// operating at 16MHz. I can't sample any slower because
// the next step down is below Nyquist rate.

// many macro values are defined in <avr/io.h> and
// <avr/interrupts.h>, which are included automatically by
// the Arduino interface

#define ZEROING_OFFSET 511
#define SAMPLE_THRESHOLD 10
#define PRESENT_COUNT 12
#define LOST_COUNT 2


// Sample Window and current front index
volatile uint16_t window = 0;
volatile uint8_t   count = 0;

// Initialization
void setup(){
  // setup GPIO pin for detected signal, if we need this to be
  // active LOW then we can reverse this easily
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);
  
  // reset to all registers for ADC
  ADMUX = ADCSRA = ADCSRB = B00000000;
    
  // Set REFS1..0 in ADMUX (0x7C) to change reference voltage to the
  // proper source (01)
  ADMUX |= B01000000;
  
  // Set MUX3..0 in ADMUX (0x7C) to read from ADC1 (Pin A4)
  ADMUX |= B00000001;
  
  // Set ADATE in ADCSRA (0x7A) to enable auto-triggering.
  ADCSRA |= B00100000;
  
  // Set the Prescaler to 2
  ADCSRA |= B00000011;
  
    // Set ADHSM in ADCSRB for High-Speed Mode and set ADTS
  // to 0 for free run mode 
  ADCSRB |= B10000000;
  
  // Set ADIE in ADCSRA (0x7A) to enable the ADC interrupt.
  // Without this, the internal interrupt will not trigger.
  ADCSRA |= B00001000;
  
  // Set ADEN in ADCSRA (0x7A) to enable the ADC.
  // Note, this instruction takes 12 ADC clocks to execute
  ADCSRA |= B10000000;
  
  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  ADCSRA |= B01000000;
}


// Processor loop
void loop(){
}


// Interrupt service routine for the ADC completion
ISR(ADC_vect) {
  int16_t value = (ADCL | (ADCH << 8)) - ZEROING_OFFSET;
  
  // make value postive, if negative
  // multiplications take too long
  if(value < 0)
  {
    value ^= 0xFFFF;
    value += 1;
  }
  
  if( value >= SAMPLE_THRESHOLD)
  {
    if(!(window & 0x8000)) count += 1;
    window = (window<<1) | 1;
  }
  else
  {
    if(window & 0x8000) count -= 1;
    window = window << 1 | 0;
  }
  
  if ( count >= PRESENT_COUNT ) 
  {
    PORTD  |= B00000001;
  } 
  else if (count <= LOST_COUNT) 
  {
    PORTD  &= B11111110;
  }
}
