# Minimum version of CMake required to build this file
CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(misc)

# Cross-compile properties
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR arm) 
SET(CMAKE_SYSTEM_VERSION 1)

# Specify the cross compiler (need to work out )
SET(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
SET(CMAKE_FIND_ROOT_PATH /export/rootfs)

# Search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# Hints for finding boost in the rootfs
SET(BOOST_INCLUDEDIR "/export/rootfs/usr/include")
SET(BOOST_LIBRARYDIR "/export/rootfs/usr/lib/arm-linux-gnueabihf")

# We'll be using C++11 functionality in this project
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
#
# Preprocessor directive required to link boost trivial logging
FIND_PACKAGE(Boost 1.55 REQUIRED COMPONENTS thread system program_options)
FIND_PACKAGE(Threads REQUIRED)

# Location of header files for the entire project
INCLUDE_DIRECTORIES(
	${Boost_INCLUDE_DIR} 
	${CMAKE_CURRENT_SOURCE_DIR} 
	"/export/rootfs/usr/include/arm-linux-gnueabihf" 
	"/export/rootfs/usr/include"
)

# Location of boost libraries
LINK_DIRECTORIES(
	${Boost_LIBRARY_DIRS} 
	"/export/rootfs/usr/lib/arm-linux-gnueabihf" 
	"/export/rootfs/lib/arm-linux-gnueabihf"
)

add_executable(test_ioctl test_ioctl.c)
target_link_libraries(test_ioctl)

add_executable(test_mmap test_mmap.c)
target_link_libraries(test_mmap)

add_executable(test_txtimestamp test_txtimestamp.c)
target_link_libraries(test_txtimestamp)

add_executable(test_wpan test_wpan.c)
target_link_libraries(test_wpan)

add_executable(test_eth test_eth.c)
target_link_libraries(test_eth)