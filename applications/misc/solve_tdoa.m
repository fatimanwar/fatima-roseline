function F = solve_tdoa(x,tdoa,pos_anc,v)
    dist = pos_anc - repmat(x,length(tdoa),1);
    dist = sqrt(sum(dist.^2,2));
    tof  = dist / v;
    tdap = tof - min(tof);
    F    = tdap - tdoa;    
end