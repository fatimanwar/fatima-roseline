#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/ioctl.h>
 
#include "../../core/modules/roseline_ioctl.h"

int main(int argc, char *argv[])
{
    // Open 
    int fd = open("/dev/roseline", O_RDWR);
    if (fd == -1)
    {
        perror("ROSELINE ioctl open");
        return 2;
    }

    // Specify a cycle count
    int c = (argc > 1 ? atoi(argv[1]) : 0);
    int h = (argc > 2 ? atoi(argv[2]) : 24000);
    int l = (argc > 3 ? atoi(argv[3]) : 24000);
    int e = (argc > 4 ? atoi(argv[4]) : 1);
    int n = (argc > 5 ? atoi(argv[5]) : 0);

    // Start 1s timer
    roseline_comp data;
    data.id          = IOTIMER_A;       // Timer to use
    data.enable      = e;               // Enable this timer
    data.next        = n;               // Time of first event
    data.cycles_high = h;               // High duration
    data.cycles_low  = l;               // Low dutration
    data.limit       = c;               // Keep going
    if (ioctl(fd, ROSELINE_SET_COMPARE, &data) == -1)
        perror("Problem sending compare instruction");
    
    // Close
    close (fd);
    return 0;
}