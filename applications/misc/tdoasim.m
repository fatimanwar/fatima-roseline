% Parameters
k       = 1;       % Number of runs
n       = 15;        % Number of anchors
v       = 343.59;    % Speed of sound
volume  = [10 5 2]; 
%e       = [1e-9 1e-8 1e-7 1e-6 1e-5 1e-4 1e-3 1e-2 1e-1 1];
e       = [0];
% Options for nonlinear solver
options = optimset('TolFun',1e-9);

% Iterate over experiments
err = zeros(length(e),k);
for i = 1:length(e)
    for j = 1:k
        pos_sen = rand(1,3).* volume;                           % Sensor
        pos_anc = rand(n,3).* repmat([volume(:,1:2) 0],n,1);    % Anchors on table
        dist    = pos_anc - repmat(pos_sen,n,1);                % Displacement
        dist    = sqrt(sum(dist.^2,2));                         % Distance
        tof     = dist / v;                                     % Perturbed TOF
        tdoa    = tof - min(tof) + e(i) * randn(n,1);           % TDOA 
        %x = lsqnonlin(@(x)solve_tdoa(x,tdoa,pos_anc,v),pos_sen,[],[],options);
        % err(i,j) = pdist([pos_sen; x]);
    end
end

