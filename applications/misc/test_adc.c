#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "bbbio/BBBiolib.h"

#define AUDIO_BUFFER_SIZE 48000

unsigned int buffer[AUDIO_BUFFER_SIZE] ={0};
int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		printf("Usage: ./test_adc <binary_file> <seconds>\n");
		printf("   eg: ./test_adc test.bin 10\n");
		return 0;
	}

	int i ,j;
	unsigned int *buf_ptr = &buffer[0];

	/* BBBIOlib init*/
	iolib_init();

	/* using ADC_CALC toolkit to decide the ADC module argument for 48000Hz
	 *
	 *	#./ADC_CALC -f 48000 -t 30
	 *
	 *	Suggest Solution :
	 *		Clock Divider : 25 ,    Open Dly : 5 ,  Sample Average : 1 ,    Sample Dly : 1
	 */
	const int clk_div = 25;
	const int open_dly = 5;
	const int sample_dly = 1;

	BBBIO_ADCTSC_module_ctrl(clk_div);
	BBBIO_ADCTSC_channel_ctrl(BBBIO_ADC_AIN2, BBBIO_ADC_STEP_MODE_SW_CONTINUOUS, open_dly, sample_dly, BBBIO_ADC_STEP_AVG_1, buffer, AUDIO_BUFFER_SIZE);

	struct timeval t_start,t_end;
 	float mTime =0;

 	// Open a binary file for writing
   	FILE *fh = fopen(argv[1], "w");
    	if (fh==NULL)
    		goto finished;
    
   	// Capture number of seconds
	for(i = 0 ; i < atoi(argv[2]); i++)
	{
		/* fetch data from ADC */
		BBBIO_ADCTSC_channel_enable(BBBIO_ADC_AIN2);

		gettimeofday(&t_start, NULL);
		BBBIO_ADCTSC_work(AUDIO_BUFFER_SIZE);
		gettimeofday(&t_end, NULL);

		mTime = (t_end.tv_sec -t_start.tv_sec)*1000000.0 +(t_end.tv_usec -t_start.tv_usec);
		mTime /=1000000.0f;
		printf("Sampling finish , fetch [%d] samples in %lfs\n", AUDIO_BUFFER_SIZE, mTime);

		// Write the data
		for (int i = 0; i < AUDIO_BUFFER_SIZE; i++)
	        	fprintf(fh,"%u\n",buffer[i]);
	}

	// Close file
	fclose (fh);

finished:
	iolib_free();
	return 0;
}