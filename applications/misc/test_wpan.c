#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <poll.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <asm/types.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <linux/sockios.h>

#include "ieee802154.h"

/* globals */
int sk_tx_timeout = 1;
static short sk_events  = POLLPRI;
static short sk_revents = POLLPRI;

int fetch_timestamp(int fd, struct msghdr *msg, int flags)
{
	int cnt = 0, res = 0, level, type;
	struct cmsghdr *cm;
	struct timespec *ts = NULL;
	struct timespec tv;

	if (flags & MSG_ERRQUEUE)
	{
		struct pollfd pfd = { fd, sk_events, 0 };
		res = poll(&pfd, 1, sk_tx_timeout);
		if (res < 1) {
			printf("ERROR: poll for tx timestamp failed. driver bug?\n");
			return res;
		} 
		else if (!(pfd.revents & sk_revents))
		{
			printf("ERROR: poll for tx timestamp woke up on non ERR event\n");
			return -1;
		}
	}

	cnt = recvmsg(fd, msg, flags);
	for (cm = CMSG_FIRSTHDR(msg); cm != NULL; cm = CMSG_NXTHDR(msg, cm))
	{
		level = cm->cmsg_level;
		type  = cm->cmsg_type;
		if (SOL_SOCKET == level && SO_TIMESTAMPNS == type) {
			ts = (struct timespec *) CMSG_DATA(cm);
			printf("-- SW TIMESTAMP %ld.%09ld\n", (long)ts[0].tv_sec, (long)ts[0].tv_nsec);
		}
		if (SOL_SOCKET == level && SCM_TIMESTAMPING == type) {
			ts = (struct timespec *) CMSG_DATA(cm);
			printf("-- HW TIMESTAMP %ld.%09ld\n", (long)ts[2].tv_sec, (long)ts[2].tv_nsec);
		}
	}
	if (!ts)
		return cnt;
	
	return 0;
}

/* Main entry point of application */
int main(int argc, char* argv[])
{
	// Helper function
	if (argc < 3)
	{
		printf("Usage: %s <iface> <short_addr> <is_tx>\n",argv[0]);
		return 1;
	}

	int sd, ret, flags, len, enabled;	// Device descriptor
	uint16_t addr, pan;					// My address and pan
	uint8_t buffer[1024];				// Packet buffer (RX)
	struct sockaddr_ieee802154 server_addr, client_addr;
	struct ifreq hwtstamp;
	struct hwtstamp_config hwconfig, hwconfig_requested;
	struct timeval tv;

	sd = socket(PF_IEEE802154, SOCK_DGRAM, 0);
	if (sd < 0)
	{
		printf("Could not open the socket\n");
		return 1;
	}
	
	// Set up the hardware timestamping
	memset(&hwtstamp, 0, sizeof(hwtstamp));
	strncpy(hwtstamp.ifr_name, argv[1], sizeof(hwtstamp.ifr_name));
	hwtstamp.ifr_data = (void *)&hwconfig;
	memset(&hwconfig, 0, sizeof(hwconfig));
	hwconfig.tx_type   = HWTSTAMP_TX_ON;
	hwconfig.rx_filter = HWTSTAMP_FILTER_ALL;
	hwconfig_requested = hwconfig;
	if (ioctl(sd, SIOCSHWTSTAMP, &hwtstamp) < 0)
		printf("ERROR: SIOCSHWTSTAMP\n");
	else
		printf("SIOCSHWTSTAMP: tx_type %d requested, got %d; rx_filter %d requested, got %d\n",
	      hwconfig_requested.tx_type, hwconfig.tx_type, hwconfig_requested.rx_filter, hwconfig.rx_filter);

	// Set socket options to enable time stamping
	enabled = 1;
	if (setsockopt(sd, SOL_SOCKET, SO_TIMESTAMPNS, &enabled, sizeof(enabled)) < 0)
		printf("ERROR: setsockopt SO_TIMESTAMPNS\n");
	flags 	= SOF_TIMESTAMPING_TX_HARDWARE
			| SOF_TIMESTAMPING_RX_HARDWARE 
			| SOF_TIMESTAMPING_TX_SOFTWARE
			| SOF_TIMESTAMPING_RX_HARDWARE 
			| SOF_TIMESTAMPING_RAW_HARDWARE;
	if (setsockopt(sd, SOL_SOCKET, SO_TIMESTAMPING, &flags, sizeof(flags)) < 0)
		printf("ERROR: setsockopt SO_TIMESTAMPING\n");
	enabled = 1;
	if (setsockopt(sd, SOL_SOCKET, SO_SELECT_ERR_QUEUE, &enabled, sizeof(enabled)) < 0)
		printf("ERROR: SO_SELECT_ERR_QUEUE failed\n");

	// Mapinpulate trecieve timeout to achieve broadcast rate 
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	if (setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval)) < 0)
		printf("Timeout error\n");

  	// Set the PAN and address
	server_addr.family = AF_IEEE802154;
	server_addr.addr.addr_type = IEEE802154_ADDR_SHORT;
	server_addr.addr.pan_id = 0xAE70;
	server_addr.addr.short_addr = atoi(argv[2]);
	if (bind(sd, (struct sockaddr *)&server_addr, sizeof(server_addr)))
	{
		printf("Could not bind to the socket with address %x\n",server_addr.addr.short_addr);
		return 1;
	}
	server_addr.addr.short_addr = 0xFFFF;

	// Preallocation for speed
	char data[1024];
	struct msghdr msg;
	struct iovec entry;
	struct sockaddr_in from_addr;
	struct {
		struct cmsghdr cm;
		char control[2014];
	} control;
	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = &entry;
	msg.msg_iovlen = 1;
	entry.iov_base = data;
	entry.iov_len = sizeof(data);
	msg.msg_name = (caddr_t)&from_addr;
	msg.msg_namelen = sizeof(from_addr);
	msg.msg_control = &control;
	msg.msg_controllen = sizeof(control);

	// TX only: send data at 1Hz
	if (atoi(argv[3]))
	{
		while(1)
		{
		    if (sendto(sd, buffer, 10, 0,(struct sockaddr *)&server_addr,sizeof(server_addr)) < 0)
	      		printf("ERROR: sendto failed");
		  	else
			{
				printf("SENT MESSAGE\n");
				fetch_timestamp(sd, &msg, MSG_ERRQUEUE);
			}
			usleep(100000);
		}
	}
	// RX only: receive data
	else
	{
		while(1)
		{
			ret = recvmsg(sd, &msg, 0);
			if (ret > 0)
			{
				printf("RECEIVED MESSAGE\n");
				fetch_timestamp(sd, &msg, 0);
			}
		}
	}

	return 0;
}