void setup()
{
  //Serial.begin(115200);
  
  // setup pin 10 to input, when push send short burst
  pinMode(10,INPUT);
  
  // setup pin 6 to output 40kHz signal for testing
  pinMode(11, OUTPUT);
  digitalWrite(11, LOW);
  TCCR2A = _BV(WGM01) | _BV(WGM00);
  TCCR2B = _BV(WGM02) | _BV(CS00);
  OCR2A = B11000111; // 199, so timer2 counts from 0 to 199 (200 cycles at 16 MHz)
}

void loop()
{
  if(digitalRead(10) == HIGH)
  {
    //Serial.println("Button Pressed, begin sending...");
    TCCR2A |= 0x40; // set COM0A0 bit
    delayMicroseconds(500); //send for 100 us
    TCCR2A &= ~0x40; // clear COM0A0 bit
    //Serial.println("Sending Completed!");
    
    delay(500); // now wait 500 ms to ensure the button is not boucing still or not released yet
  
    if(digitalRead(10) == HIGH)
    {
      //Serial.println("Button still held down");
      while(digitalRead(10) == HIGH); // while still HIGH just wait, to fix holding button down
      //Serial.println("Button Released");
    }
  }
}
