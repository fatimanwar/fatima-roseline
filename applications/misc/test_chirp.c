#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "bbbio/BBBiolib.h"

int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		printf("Usage: ./test_chirp <number> <chirp_ms> <pause_ms>\n");
		printf("   eg: ./test_chirp 10 100 900\n");
		return 0;
	}

	// Enable fast IO
	iolib_init();
	BBBIO_sys_Enable_GPIO(BBBIO_GPIO1);
	BBBIO_GPIO_set_dir(
		BBBIO_GPIO1, 		// bank
		0x0, 			// input
		BBBIO_GPIO_PIN_17	// output
	);

	// Turn off transmitter by default
	BBBIO_GPIO_high(BBBIO_GPIO1, BBBIO_GPIO_PIN_17);

	// Get the chirp / quiet time
	long chirp = (long) atoi(argv[2]) * 1000;
	long quiet = (long) atoi(argv[3]) * 1000;

	// Perform the number of chirps
	for (int i = 0; i < atoi(argv[1]); i++)
	{
		printf("Sending chirp %d\n", i+1);
		BBBIO_GPIO_low(BBBIO_GPIO1, BBBIO_GPIO_PIN_17);
		usleep(chirp);
		BBBIO_GPIO_high(BBBIO_GPIO1, BBBIO_GPIO_PIN_17);
		usleep(quiet);
	}

	// Clear up BBBIO
	iolib_free();

	// Success
	return 0;
}