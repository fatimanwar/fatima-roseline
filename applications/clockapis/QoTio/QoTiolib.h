#ifndef QOTIOLIB_H
#define QOTIOLIB_H

#include "../QoTinterface/QoTclock.h"

#define MAX_CLOCKS 5


// synchronization parameters, calculated and returned by the underlying synchronization algorithm running behind the QoT service
typedef struct qot_clock_sync_pars_t {
	uint8_t local_clock;					// local timer with underlying oscillator synchronized to the reference clock
	double sync_pars[7];					// polynomial coefficients and variances for synchronization algorithm
	double sync_caps[3];					// latency, energy and bandwidth resources utilization
	struct timespec actual_precision;		// actual precision achieved by the underlying synchronization algorithm
	qot_clock *clk;							// reference clock source
	struct qot_clock_sync_pars_t *next;		// pointer to next event in the list
} qot_clock_sync_pars;

// states determine the current synchronization step
typedef enum {
	STATE_WAIT = 0x1,				// configure/bind with reference clock
	STATE_SYNC = 0x2,				// synchronization procedure
	STATE_SYNCED = 0x3,				// synced with reference clock within required precision
	STATE_RELEASE = 0x4,			// release the current clock source
	STATE_SCHEDULE_HEAD = 0x5,		// schedule only the first event in the list
	STATE_SCHEDULE_ALL = 0x6,		// schedule all events in the list
	STATE_SCHEDULED = 0X7			// event sheduled
} qot_sync_state;

// qot library message types
enum {
	QOT_ADD_CLOCK,					// Add a new clock
	QOT_SET_CLOCK,					// Update a clock
	QOT_RELEASE_CLOCK,				// release the clock
	QOT_SET_SCHED,					// schedule and event for a clock
	QOT_DEL_SCHED,					// delete a schedule for a clock
	QOT_SET_SCHED_PARS,				// notify that event has been shceduled
	QOT_GET_SYNC_PARS,				// get the synchronization parameters for a clock
	QOT_SET_SYNC_PARS				// set the synchronization parameters for a clock
};

// function to pass arguments from QoT lib to QoT service and vice versa
void qot_libio_handler(unsigned int mode, qot_clock_sync_pars *syncp);

// function to pass arguments from QoT interface to QoT lib and vice versa
void qot_clock_handler(unsigned int mode, qot_clock *clk);

#endif
