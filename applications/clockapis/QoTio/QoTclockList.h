#ifndef QOTCLOCKLIST_H
#define QOTCLOCKLIST_H

#include "QoTiolib.h"

// --------- list functions for clock ------------

/* this initialises a clock, allocates memory, and returns   */
/* a pointer to the new clock */
qot_clock_sync_pars * init_clock(qot_clock *clk)
{
	qot_clock_sync_pars *ptr;
	ptr = (qot_clock_sync_pars *) calloc( 1, sizeof(qot_clock_sync_pars));
	if( ptr == NULL )
		return (qot_clock_sync_pars *) NULL;
	else {
		ptr->clk = clk;
		return ptr;
	}
}

/* this adds a clock to the end of the list. You must allocate a clock,	*/
/* then pass its address to this function, adding to end of list		*/
void add_clock(qot_clock_sync_pars *new, qot_clock_sync_pars *head, qot_clock_sync_pars *end)
{
	if( head == NULL )      /* if there are no clocks in list, then         */
		head = new;         /* set head to this new clock                   */
	end->next = new;        /* link in the new clock to the end of the list */
	new->next = NULL;       /* set next field to signify the end of list	*/
	end = new;              /* adjust end to point to the last clock        */
}

/* search the list for a reference timeline name, and return a pointer to the found clock	*/
/* accepts a name to search for, and a pointer from which to start. If						*/
/* you pass the pointer as 'head', it searches from the start of the list					*/
qot_clock_sync_pars * search_clock_list(qot_clock_sync_pars *given_ptr, const char * reference_timeline)
{
	qot_clock_sync_pars *ptr;
	ptr = given_ptr;
	while(strcmp(reference_timeline, ptr->clk->reference_timeline ) != 0 ) {	/* whilst name not found */
		ptr = ptr->next;		/* goto the next clock	*/
		if( ptr == NULL )		/* stop if we are at the*/
			break;				/* end of the list		*/
	}
	return ptr;
}

/* deletes the specified clock pointed to by 'ptr' from the list */
void delete_clock(qot_clock_sync_pars *ptr, qot_clock_sync_pars *head, qot_clock_sync_pars *end )
{
	qot_clock_sync_pars *temp, *prev;
	temp = ptr;    /* clock to be deleted */
	prev = head;   /* start of the list, will cycle to clock before temp    */
	
	if( temp == prev ) {                    /* are we deleting first clock  */
		head = head->next;                  /* moves head to next clock     */
		if( end == temp )                   /* is it end, only one clock?   */
			end = end->next;				/* adjust end as well			*/
		ptr = ptr->next;					/* increment current ptr as well*/
		free( temp );                       /* free space occupied by clock */
	}
	else {                                  /* if not the first clock, then */
		while( prev->next != temp ) {       /* move prev to the clock before*/
			prev = prev->next;              /* the one to be deleted		*/
		}
		prev->next = temp->next;            /* link previous clock to next  */
		ptr = prev;
		if( end == temp )                   /* if this was the end clock,   */
			end = prev;
		free( temp );                       /* free space occupied by clock */
	}
}

#endif
