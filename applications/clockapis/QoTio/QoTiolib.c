#include "QoTiolib.h"
#include "QoTclockList.h"

// Waiting for a clock reference to bind to
qot_sync_state state = STATE_WAIT;

// pointers to the start of clocks list, the end of clocks list and current position in the clocks list
qot_clock_sync_pars *head = (qot_clock_sync_pars *) NULL;			// head in the clock list
qot_clock_sync_pars *end = (qot_clock_sync_pars *) NULL;			// end in the clock list
qot_clock_sync_pars *ptr = (qot_clock_sync_pars *) NULL;			// current clock in the clock list

//--------- getter / setter for QoT service & interface --------------

void qot_libio_handler(unsigned int mode, qot_clock_sync_pars *syncp)
{
	switch(mode)
	{
		/* only called when the parameters have to be returned after successful synchronization round */
		case QOT_SET_SYNC_PARS:			// QoT service sets all the sync parameters for a particular clock

			/* the pointer will be pointing to the same strcuture */
			//ptr = search_clock_list(head, sync_pars->clk->reference_timeline);
			
			ptr->local_clock = syncp->local_clock;
			memcpy(ptr->sync_pars, syncp->sync_pars, sizeof(double) * 7);
			memcpy(ptr->sync_caps, syncp->sync_caps, sizeof(double) * 3);
			ptr->actual_precision = syncp->actual_precision;
			state = STATE_SYNCED;
			
		case QOT_GET_SYNC_PARS:			// QoT service requests for sync parameters of current clock
			syncp = ptr;
			break;
			
			/* only called when the sched parameters have to be returned after successful scheduling */
		case QOT_SET_SCHED_PARS:		// QoT service updates the status of a schedule
			
			ptr->clk = syncp->clk;
			state = STATE_SCHEDULED;	// QoT service finished scheduling
	}
}

void qot_clock_handler(unsigned int mode, qot_clock *clk)
{
	if (ptr != NULL)
		ptr = search_clock_list(head, clk->reference_timeline);
	
	switch(mode)
	{
		case QOT_ADD_CLOCK:				// QoT interface sets up a new clock
			
			ptr = init_clock(clk);
			
			// insert new clock at the end of clock list
			add_clock(ptr, head, end);
			state = STATE_SYNC;			// Polling QoT service should start synch for current ptr
			break;
			
		case QOT_SET_CLOCK:				// QoT interface updates a clock through new precision or resolution
			
			ptr->clk = clk;
			state = STATE_SYNC;			// Polling QoT service should start synch for current ptr
			break;
			
		case QOT_RELEASE_CLOCK:			// QoT interface releases the clock
			
			delete_clock(ptr, head, end);
			state = STATE_RELEASE;		// Polling QoT service should release clocksource for current ptr
			break;
			
		case QOT_SET_SCHED:				// QoT interface updates a clock's schedule
			
			ptr->clk = clk;
			state = STATE_SCHEDULE_HEAD;// Polling QoT service should schedule (only first event) for current ptr
			break;
			
		case QOT_DEL_SCHED:				// QoT interface deletes a clock's schedule
			
			ptr->clk = clk;
			state = STATE_SCHEDULE_ALL;// Polling QoT service should schedule all events for current ptr

	}
}


