#ifndef QOTSCHEDLIST_H
#define QOTSCHEDLIST_H

#include "QoTclock.h"

// --------- list functions for schedules ------------

/* this prints the details of a schedule */
void print_sched(qot_sched_params *ptr)
{
	printf("event time: %ld.%09ld\n", ptr->event_time.tv_sec, ptr->event_time.tv_nsec);
	printf("high time: %ld.%09ld\n", ptr->high.tv_sec, ptr->high.tv_nsec);
	printf("low time: %ld.%09ld\n", ptr->low.tv_sec, ptr->low.tv_nsec);
	printf("low time: %i\n", ptr->limit);
}

/* this prints all schedules from the current address passed to it. If you	*/
/* pass it 'head', then it prints out the entire list, by cycling through	*/
/* each schedule and calling 'printschedule' to print each schedule found	*/
void print_sched_list( qot_sched_params *ptr_given )
{
	qot_sched_params *ptr;
	ptr = ptr_given;
	while( ptr != NULL )			/* continue whilst there are schedules left	*/
	{
		print_sched( ptr );			/* print out the current schedule			*/
		ptr = ptr->next;            /* goto the next schedule in the list       */
	}
/*this is test commit*/
}

/* this initialises a schedule, allocates memory for the schedule, and returns	*/
/* a pointer to the new schedule. Must pass it the schedule details				*/
qot_sched_params * init_sched(struct timespec eventtime, struct timespec high, struct timespec low, uint16_t limit)
{
	qot_sched_params *ptr;
	ptr = (qot_sched_params *) calloc( 1, sizeof(qot_sched_params) );
	if( ptr == NULL )
		return (qot_sched_params *) NULL;
	else {
		ptr->event_time = eventtime;
		ptr->high = high;
		ptr->low = low;
		ptr->limit = limit;
		ptr->scheduled = false;
		return ptr;
	}
}

/* this adds a schedule to the end of the list. You must allocate a schedule and	*/
/* then pass its address to this function,adding to end of list						*/
void add_sched(qot_sched_params *new, qot_sched_params *head, qot_sched_params *end)
{
	if( head == NULL )      /* if there are no schedules in list, then			*/
		head = new;         /* set head to this new schedule					*/
	end->next = new;        /* link in the new schedule to the end of the list	*/
	new->next = NULL;       /* set next field to signify the end of list		*/
	end = new;              /* adjust end to point to the last schedule			*/
}

/* search the list for the event time, and return a pointer to the found schedule	*/
/* accepts a name to search for, and a pointer from which to start. If				*/
/* you pass the pointer as 'head', it searches from the start of the list			*/
qot_sched_params * search_sched(qot_sched_params *given_ptr, struct timespec eventtime)
{
	qot_sched_params *ptr;
	ptr = given_ptr;
	uint64_t given_nsec = eventtime.tv_sec * 1000000000 + eventtime.tv_nsec;
	uint64_t nsec = ptr->event_time.tv_sec * 1000000000 + ptr->event_time.tv_nsec;
	while(nsec != given_nsec) {	/* whilst event not found	*/
		ptr = ptr->next;		/* goto the next schedule	*/
		if( ptr == NULL )		/* stop if we are at the	*/
			break;				/* of the list				*/
		nsec = ptr->event_time.tv_sec * 1000000000 + ptr->event_time.tv_nsec;
	}
	return ptr;
}

/* inserts a new schedule, uses event time field to align schedule as alphabetical list	*/
/* pass it the address of the new schedule to be inserted, with details all filled in   */
void insert_sched(qot_sched_params *new, qot_sched_params *head, qot_sched_params *end )
{
	qot_sched_params *temp, *prev;	/* similar to deleteschedule	*/
	
	if( head == NULL ) {			/* if an empty list,			*/
		head = new;					/* set 'head' to it				*/
		end = new;
		head->next = NULL;			/* set end of list to NULL		*/
		return;						/* and finish					*/
	}
	
	temp = head;					/* start at beginning of list	*/
	uint64_t temp_nsec = temp->event_time.tv_sec * 1000000000 + temp->event_time.tv_nsec;
	uint64_t new_nsec = new->event_time.tv_sec * 1000000000 + new->event_time.tv_nsec;
	
	
	while(temp_nsec < new_nsec) {	/* whilst current event time < new event time	*/
		temp = temp->next;			/* goto the next schedule in list				*/
		if( temp == NULL )			/* dont go past end of list						*/
			break;
		temp_nsec = temp->event_time.tv_sec * 1000000000 + temp->event_time.tv_nsec;
	}
	
	/* we are at the point to insert, we need previous schedule before we insert*/
	/* first check to see if its inserting before the first schedule!			*/
	if( temp == head ) {
		new->next = head;             /* link next field to original list		*/
		head = new;                   /* head adjusted to new schedule			*/
	}
	else {				/* its not the first schedule, a different approach		*/
		prev = head;	/* start of the list, will cycle to schedule before temp*/
		while( prev->next != temp ) {
			prev = prev->next;
		}
		prev->next = new;			/* insert schedule between prev and next	*/
		new->next = temp;
		if( end == prev )			/* if the new schedule is inserted at the	*/
			end = new;				/* end of the list then adjust 'end'		*/
	}
}

/* deletes the specified schedule pointed to by 'ptr' from the list				*/
void delete_sched(qot_sched_params *ptr, qot_sched_params *head, qot_sched_params *end )
{
	qot_sched_params *temp, *prev;
	temp = ptr;    /* schedule to be deleted */
	prev = head;   /* start of the list, will cycle to schedule before temp		*/
	
	if( temp == prev ) {                    /* are we deleting first schedule	*/
		head = head->next;                  /* moves head to next schedule		*/
		if( end == temp )                   /* is it end, only one schedule?	*/
			end = end->next;                /* adjust end as well				*/
		free( temp );                       /* free space occupied by schedule	*/
	}
	else {                                  /* if not the first schedule, then	*/
		while( prev->next != temp ) {       /* move prev to the schedule before	*/
			prev = prev->next;              /* the one to be deleted			*/
		}
		prev->next = temp->next;            /* link previous schedule to next	*/
		if( end == temp )                   /* if this was the end schedule,	*/
			end = prev;                     /* then reset the end pointer		*/
		free( temp );                       /* free space occupied by schedule	*/
	}
}

/* this deletes all schedules from the place specified by ptr	*/
/* if you pass it head, it will free up entire list				*/
void delete_sched_list(qot_sched_params *ptr, qot_sched_params *head, qot_sched_params *end )
{
	qot_sched_params *temp;
	
	if( head == NULL ) return;	/* dont try to delete an empty list		*/
	
	if( ptr == head ) {			/* if we are deleting the entire list	*/
		head = NULL;			/* then reset head and end to signify 	*/
		end = NULL;				/* empty list							*/
	}
	else {
		temp = head;				/* if its not the entire list, readjust end	*/
		while( temp->next != ptr )	/* locate previous schedule to ptr			*/
			temp = temp->next;
		end = temp;					/* set end to schedule before ptr			*/
	}
	
	while( ptr != NULL ) {		/* whilst there are still schedules to delete	*/
		temp = ptr->next;		/* record address of next schedule				*/
		free( ptr );			/* free this schedule							*/
		ptr = temp;				/* point to next schedule to be deleted			*/
	}
}

#endif
