#include "QoTclock.h"
#include "../QoTio/QoTiolib.h"

#include "QoTschedList.h"

// ----------------- Functions ---------------------
qot_clock qot_init_clock(const char *reftimeline, struct timespec minprecision, struct timespec maxprecision, struct timespec resolution)
{
	
	qot_clock clk;
	clk.reference_timeline = reftimeline;
	clk.target_precision[0] = minprecision;
	clk.target_precision[1] = maxprecision;
	clk.target_resolution = resolution;
	clk.sched_head = (qot_sched_params *) NULL;
	clk.sched_end = (qot_sched_params *) NULL;
	
	// add this clock to the qot library
	qot_clock_handler(QOT_ADD_CLOCK, &clk);
	
	return clk;
}

void qot_release_clock(qot_clock clk)
{
	// delete all schedules associated to this clock
	delete_sched_list(clk.sched_head, clk.sched_head, clk.sched_end);
	
	// delete this clock from the qot library
	qot_clock_handler(QOT_RELEASE_CLOCK, &clk);
}

void qot_set_precision(qot_clock clk, struct timespec minprecision, struct timespec maxprecision)
{
	clk.target_precision[0] = minprecision;
	clk.target_precision[1] = maxprecision;
	
	// update this clock in the qot library
	qot_clock_handler(QOT_SET_CLOCK, &clk);
}

void qot_set_resolution(qot_clock clk, struct timespec resolution)
{
	clk.target_resolution = resolution;
	
	// update this clock in the qot library
	qot_clock_handler(QOT_SET_CLOCK, &clk);
}

/** TBD
struct timespec qot_get_time(qot_clock *clk)
{

}
 **/

void qot_set_ref_timeline(qot_clock clk, const char *reftimeline)
{
	clk.reference_timeline = reftimeline;
	
	// update this clock in the qot library
	qot_clock_handler(QOT_SET_CLOCK, &clk);
}

void qot_schedule_event(qot_clock clk, struct timespec eventtime, struct timespec high, struct timespec low, uint16_t repetitions)
{
	qot_sched_params *new;
	new = init_sched(eventtime, high, low, repetitions);
	insert_sched(new, clk.sched_head, clk.sched_end);
	
	// update this clock in the qot library with new schedule
	qot_clock_handler(QOT_SET_SCHED, &clk);
}

struct timespec qot_get_next_event(qot_clock clk)
{
	// prints the upcoming schedule's details
	print_sched(clk.sched_head);
}

void qot_get_scheduled_events(qot_clock clk)
{
	// prints all the schedules for this clock
	print_sched_list(clk.sched_head);
}

void qot_cancel_event(qot_clock clk, struct timespec eventtime)
{
	qot_sched_params *event;
	event = search_sched(clk.sched_head, eventtime);
	if(event == NULL) {
		printf("Event not found\n");
	}
	else
		delete_sched(event, clk.sched_head, clk.sched_end);
	
	// update this clock in the qot library with deleted schedule
	qot_clock_handler(QOT_DEL_SCHED, &clk);
}


