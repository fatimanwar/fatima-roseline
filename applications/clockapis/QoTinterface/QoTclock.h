#ifndef QOTCLOCK_H
#define QOTCLOCK_H

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/timex.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

//-------- User specified input parameters for creating a clock  -----

// linked list to store scheduling parameters for an event
typedef struct qot_sched_params_t {
	struct timespec event_time;			// time for which event is scheduled
	struct timespec high;				// time for high pulse
	struct timespec low;				// time for low pulse
	uint16_t limit;						// number of event repetitions
	bool scheduled;						// scheduled or not
	struct qot_sched_params_t *next;	// pointer to next event in the list
} qot_sched_params;

// main clock source structure
typedef struct qot_clock_t {
	const char * reference_timeline;	// Name of reference timeline shared among all collaborating entities
	struct timespec target_precision[2];// min and max synchronization error bound
	struct timespec target_resolution;	// required clock  resolution
	qot_sched_params *sched_head;		// pointer to the linked list's head for all the scheduled events
	qot_sched_params *sched_end;		// pointer to the linked list's end
} qot_clock;

//-------- function calls for clock  --------

// Phase 1: initialize or release a clock 
qot_clock qot_init_clock(const char *reftimeline, struct timespec minprecision, struct timespec maxprecision, struct timespec resolution);
void qot_release_clock(qot_clock clk);

// Phase 2: dynamically change and access the quality metrics of a registered clock
void qot_set_precision(qot_clock clk, struct timespec minprecision, struct timespec maxprecision);	// sets precision of a registered clock
void qot_set_resolution(qot_clock clk, struct timespec resolution);	// sets resolution
struct timespec qot_get_time(qot_clock clk);						// returns nanoseconds of the clock  time or timespec
void qot_set_ref_timeline(qot_clock clk, const char *reftimeline);	// dynamically bind and unbind from a reference clock

// Phase 3: Event scheduling, ordering and tracking
void qot_schedule_event(qot_clock clk, struct timespec eventtime, struct timespec high, struct timespec low, uint16_t repetitions);	// schedule an event at eventtime according to the given clock
struct timespec qot_get_next_event(qot_clock clk);					// returns the time for the upcoming event according to the clock
void qot_get_scheduled_events(qot_clock clk);	// gets scheduled event list for this clock
void qot_cancel_event(qot_clock clk, struct timespec eventtime);	// cancels the specified scheduled event??

struct timespec qot_track_event(qot_clock clk, struct timespec eventtime);	// track an event locally which was timestamped according to the given clock

void qot_order_event(qot_clock clk);							// order events locally??


#endif
