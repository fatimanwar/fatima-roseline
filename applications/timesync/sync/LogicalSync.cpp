// Base type
#include "LogicalSync.hpp"

// Subtypes
//#include "PulseSync.hpp"
#include "FTSP.hpp"

using namespace timesync;

// All syng algorithms get parameter updates from the roseline module
LogicalSync::LogicalSync(TimerName timer) : flag(true)
{
	// Open an IOCTL handle
	const char *file_name = "/dev/roseline";
	fd = open(file_name, O_RDWR);
	if (fd == -1)
		std::cerr << "FTSP::ERROR IOCTL open() problem" << std::endl;

	// Protocol to listen for time sync updates
	thread = boost::thread(boost::bind(&LogicalSync::worker,this,timer));
}

LogicalSync::~LogicalSync()
{
	// Stop and join thread
	this->flag = false;
	thread.join();

	// Close ioctl
	close(fd);
}

// Worket thread to poll for new parameters
void LogicalSync::worker(TimerName timer)
{
	// Parameter data gets dumped into this structure
	roseline_pars tmp; tmp.id = timer;
	roseline_pars par; par.id = timer;	
	bool first;

	// Some code to monitor the 
	while (this->flag)
	{
		// Perform ioctl to find new parameters, and cache them if found
		if (ioctl(fd, ROSELINE_GET_PARS, &tmp) != -1)
		{
			// If we have new parameters
			if (tmp.seq > par.seq || first)
			{
				// Copy them over
				memcpy(&par,&tmp,sizeof(roseline_pars));

				// Notify the syncrhonization algorithm
				this->configure(par);

				// First flag
				first = false;
			}
		}

		// Sleep for a little bit between checks
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	}
}

// Worket thread to poll for new parameters
void LogicalSync::broadcast(const roseline_sync &sync)
{
	// Broadcast to the world
	if (ioctl(fd, ROSELINE_SET_SYNC, &sync) != -1)
	{
		//std::cout << "TSYNC:: Broadcast new sync parameters" << std::endl;
		//std::cout << " - skew   : " << sync.est_skew     << std::endl;
		//std::cout << " - offset : " << sync.est_offset   << std::endl;
		//std::cout << " - local  : " << sync.est_local << std::endl;
	}
}

// Factory method
boost::shared_ptr<LogicalSync> LogicalSync::Factory(
			LogicalSyncType type,			// Synchronization algorithm
			boost::asio::io_service *io,		// IO service
			boost::shared_ptr<LogicalClock> clk,	// Clock discipline type
			boost::shared_ptr<LogicalRadio> radio,	// Radio type
			boost::shared_ptr<LogicalStamp> stamp,	// Stamping type
			bool master)				// Are we the master node?
{
	// Instantiate a new logical sync algorithm
	switch(type)
	{
		//case SYNC_PULSESYNC: 	return boost::shared_ptr<LogicalSync>((LogicalSync*) new PulseSync(io,clock,stamp,radio));
		case SYNC_FTSP: 	return boost::shared_ptr<LogicalSync>((LogicalSync*) new FTSP(io,clk,radio,stamp,master));
	}
	return boost::shared_ptr<LogicalSync>((LogicalSync*) new FTSP(io,clk,radio,stamp,master));
}