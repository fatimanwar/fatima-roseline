#ifndef LOGICAL_SYNC_H
#define LOGICAL_SYNC_H

// Prerequisite abstractions
#include "../clock/LogicalClock.hpp"
#include "../radio/LogicalRadio.hpp"
#include "../stamp/LogicalStamp.hpp"

// Standard integer types
#include <cstdint>

// Required for logging and shared pointers
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

namespace timesync
{
        // Filter type
        enum LogicalSyncType
        {
		//SYNC_PULSESYNC,
		SYNC_FTSP
        };

        // Base functionality provided by filters
        class LogicalSync
        {
        	// Constructor and destructor
        	public: LogicalSync(TimerName timer);
        	public: ~LogicalSync();

        	// Used by all sync algorithms
        	private: int fd;								// ioctl file descriptor
        	private: bool flag;								// Thread termination flag
        	private: boost::thread thread;							// Thread handle
        	private: void worker(TimerName timer);						// Thread worker
		protected: void broadcast(const roseline_sync &sync);				// Broadcast sync parameters

        	// Perform the synchronization event
		public: virtual void event(const boost::system::error_code& e) = 0;						// Run one event of the algorithm

		// Reset the synchronization algorithm
		public: virtual void reset() = 0;						// Reset syncronization
		public: virtual bool start(float interval) = 0;					// Start syncronization
		public: virtual bool stop() = 0;						// Stop synchronization
		public: virtual void configure(const roseline_pars &pars) = 0;			// Configure synchronization
		
		// At any time, query the statistics about the synchronization algorith
		public: virtual void stats() = 0;
		
		// Get all of the sync parameters
		public: virtual uint32_t getsync(roseline_sync &sync) = 0;
		
		// Factory method to produce a handle to a sync algorithm
		public: static boost::shared_ptr<LogicalSync> Factory(
			LogicalSyncType type,    		// Synchronization algorithm
			boost::asio::io_service *io,		// IO service
			boost::shared_ptr<LogicalClock> clk,	// Clock discipline type
			boost::shared_ptr<LogicalRadio> radio,	// Radio type
			boost::shared_ptr<LogicalStamp> stamp,	// Stamping type			
			bool master
		);
        };
}

#endif