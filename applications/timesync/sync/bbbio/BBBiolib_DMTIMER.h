#ifndef BBBIO_DMTIMER_H
#define BBBIO_DMTIMER_H

#include <stdint.h>

// The various timers
#define BBBIO_DMTIMER_TIMER2 0x1
#define BBBIO_DMTIMER_TIMER3 0x2
#define BBBIO_DMTIMER_TIMER4 0x3
#define BBBIO_DMTIMER_TIMER5 0x4
#define BBBIO_DMTIMER_TIMER6 0x5
#define BBBIO_DMTIMER_TIMER7 0x6

// Initialise the timer subsystem
int BBBIO_DMTIMER_Init();

// Fetch the current time = (overflow << 8) | count 
uint32_t BBBIO_DMTIMER_Work(int timer);

#endif