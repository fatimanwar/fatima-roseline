#ifndef FTSP_H
#define FTSP_H

#include "LogicalSync.hpp"

namespace timesync
{
	// Message types
	enum {
	    MSG_TYPE_FTSP_STROBE = 0x1,
	};

	// Message strobe (sent by root)
	typedef struct __attribute__ ((__packed__)) StrobeMsg_t {
		uint16_t root;			// Who it thinks is the root
		uint32_t seq;			// The sequence number of this strobe
		uint64_t stamp;			// The time stamp of the SEQ-1 strobe
	} StrobeMsg;

	// Message types
	typedef enum {
	    STATE_INIT   	        = 0x1,	// B: No root elected
	    STATE_SENDING_STROBE1       = 0x2,	// R: Busy sending strobes
	    STATE_SENDING_STROBE2       = 0x3,	// R: Busy sending strobes
	    STATE_SENDING_FINISHED      = 0x4,	// R: Busy sending strobes
	    STATE_ROOT_WAIT_CAPTURE1    = 0x5,	// R: Busy sending strobes
	    STATE_ROOT_WAIT_CAPTURE2    = 0x6,	// R: Busy sending strobes
	    STATE_WAITING_FOR_RADIO1    = 0x7,	// B: Waiting on a radio RX event
	    STATE_WAITING_FOR_RADIO2    = 0x8,	// B: Waiting on a radio RX event
	    STATE_WAITING_FOR_CAPTURE1  = 0x9,	// B: Waiting on a stamp event
	    STATE_WAITING_FOR_CAPTURE2  = 0xA	// B: Waiting on a stamp event
	} State;

	// Message types
	enum {
	    CONFIG_STROBE_MS    = 150,		// Send out a strobe every 1ms
	    CONFIG_TIMEOUT_MS   = 5,		// If there is no strobe in 100ms, assume we are finished the round
	    CONFIG_LED_MS       = 1000,		// LED on time on strobe end
	    CONFIG_NUM_STROBES  = 2 		// Max number strobes sent by the root
	};

	// Our modification of the FTSP algorithm. We assume that all nodes are in communication
	// range of the root, and hence we don't really have to worry (for now) about bulding a
	// tree, and distributing measurements down that tree. Since we have a rather large time
	// stamp uncertainty, we must go through a fast strobing round to find a radio transmit
	// event that best links the sender and receiver clocks. ie, we 
	//
	// TIME : COMMUNICATION   : MSG CONTENT : NODE MEASURES : NODE CALCULATES
	// t1 	: r1 -> n1 ... nN : [MSG:xx  ] 	: t_n1          : xxx
	// t2 	: r1 -> n1 ... nN : [MSG:t_r1]  : t_n2          : e2 = t_r1 - t_n1
	// t3 	: r1 -> n1 ... nN : [MSG:t_r2]  : t_n3          : e3 = t_r2 - t_n2
	// t4 	: r1 -> n1 ... nN : [MSG:t_r3]  : t_n4          : e4 = t_r3 - t_n3
	// t5 	: r1 -> n1 ... nN : [MSG:t_r4]  : t_n5          : e5 = t_r4 - t_n4
	//
	// We can feasibly fire off strobes at around 100 Hz, meaning that the strobing takes about
	// one second to obtain 100 error measurements. Under the assumption that the clock drift 
	// doesn't change substantially through this period, then we find E = min(e2,...e99) - k 
	// where k is the constant 9us latency between TX/RX events.

        class FTSP : public LogicalSync
        {
        	// Are we in an active state?
        	private: bool active, toggle;						
		private: roseline_sync sync;						// Sync parameters

        	// The clock, time stamping system and radio 
		private: boost::shared_ptr<LogicalClock> clk;				// Clock discipline type
		private: boost::shared_ptr<LogicalStamp> stamp;				// Stamping type
		private: boost::shared_ptr<LogicalRadio> radio;				// Radio type)
	
        	// Current state of FTSP
        	private: State state;
        	private: boost::asio::deadline_timer timer;				// Timer for dithering, timeout
        	private: boost::asio::deadline_timer timer_event;			// Timer for update event
        	private: boost::asio::deadline_timer timer_led;				// Timer for LED event
        	private: long interval_ms;						// Timer interval
        	private: uint64_t radio_stamp[CONFIG_NUM_STROBES];			// Strobe cache (for nodes)
        	private: uint64_t input_stamp[CONFIG_NUM_STROBES];			// Strobe cache (for nodes)
        	private: uint64_t valid_stamp[CONFIG_NUM_STROBES];			// Data validity
        	private: uint64_t cached_radio;
        	private: uint64_t cached_capture;
        	private: uint32_t count;						// Strobe count
        	private: StrobeMsg msg;							// Local message copy
        	private: bool root;							// Are we master / root?
        	private: double error;

        	// Constructor
        	public: FTSP(
        		boost::asio::io_service *io,					// ASIO handle	
        		boost::shared_ptr<LogicalClock> _clk,				// Clock discipline type
			boost::shared_ptr<LogicalRadio> _radio,				// Radio type)
			boost::shared_ptr<LogicalStamp> _stamp,				// Stamping type
			bool master							// Are we the master node
		);
        	public: ~FTSP();

        	// Impelmentation-specific functions
		private: void cb_stamp(bool success, uint64_t t);			// Stamp callback
		private: void cb_radio(uint8_t id, uint8_t* payload, uint32_t len);	// Radio callback
        	private: void process();						// Process a received messsage
        	private: void timeout();						// Strobe timeout function (node and root)
        	private: void led_on();							// Flash an LED
        	private: void led_off();						// Flash an LED

		// Inherited methods
		public: void reset();							// Reset syncronization
		public: bool start(float interval);					// Start syncronization
		public: bool stop();							// Stop synchronization
		public: void event(const boost::system::error_code& e);			// Run one step of the algorithm
		public: void stats();							// Debug information
		public: void configure(const roseline_pars &pars);			// Configure synchronization
		public: uint32_t getsync(roseline_sync &sync);				// Get sync parameters
        };
}

#endif
