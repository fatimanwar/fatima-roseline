#include "FTSP.hpp"

// std::setprecision
#include <iomanip>      
#include <fstream>

using namespace timesync;

#define DEBUG true
#define TEST  true

FTSP::FTSP(	boost::asio::io_service *io,		// ASIO handle
		boost::shared_ptr<LogicalClock> _clk,	// Clock discipline type
		boost::shared_ptr<LogicalRadio> _radio,	// Radio type
		boost::shared_ptr<LogicalStamp> _stamp,	// Stamp type
		bool master)
	: LogicalSync(_stamp->timerid()), clk(_clk), stamp(_stamp), radio(_radio), active(false), root(master),
		timer(*io, boost::posix_time::milliseconds(CONFIG_TIMEOUT_MS)),
		timer_event(*io, boost::posix_time::milliseconds(CONFIG_TIMEOUT_MS)),
		timer_led(*io, boost::posix_time::milliseconds(CONFIG_LED_MS))
{
	// Issue a reset by default to setup callbacks
	reset();
}

FTSP::~FTSP()
{

}

void FTSP::reset() 
{
	if (DEBUG) std::cout << "FTSP: reset()" << std::endl;

	// Reset counters
	memset(input_stamp,0x0,CONFIG_NUM_STROBES*sizeof(uint64_t));
	memset(radio_stamp,0x0,CONFIG_NUM_STROBES*sizeof(uint64_t));
	memset(valid_stamp,0x0,CONFIG_NUM_STROBES*sizeof(uint64_t));

	// Reset the clock
	clk->reset();

	// Forget the root
	msg.root = radio->addr_bc();

        // Set the state to initializing
        state = STATE_INIT;

	// Bind the radio and stamp callback to some local function
	radio->callback(boost::bind(&FTSP::cb_radio, this, _1, _2, _3));
	stamp->callback(boost::bind(&FTSP::cb_stamp, this, _1, _2));
}

bool FTSP::start(float interval)
{
	if (DEBUG) std::cout << "FTSP: start()" << std::endl;
	if (active) return false;
		active = true;
	radio->start();

	// Convert from seconds to milliseconds
	interval_ms = static_cast<uint64_t>(interval * 1e3);

	// Call the first event manually
     	timer_event.expires_from_now(boost::posix_time::milliseconds(0));
        timer_event.async_wait(boost::function<void (const boost::system::error_code&)>( boost::bind( &FTSP::event, this, _1 ) ));

	// Success
	return true;
}

bool FTSP::stop()
{
	if (DEBUG) std::cout << "FTSP: stop()" << std::endl;
	if (!active) return false;
	active = false;
	radio->stop();
	return true;
}


void FTSP::cb_stamp(bool success, uint64_t t)
{
	if (!active) return;

	// ROOT: we have just sent out a strobe
	switch(state)
	{

	// ROOT
	case STATE_ROOT_WAIT_CAPTURE1:
	
		// Update the message with the latest time and sequence number
		if (DEBUG) std::cout << "FTSP::ROOT capture 1 received" << std::endl;	
		
		// Cache the stamp and save the sequence number
		cached_radio = t;

		state = STATE_SENDING_STROBE2;

		// Reschedule the next event
	     	timer.expires_from_now(boost::posix_time::milliseconds(CONFIG_STROBE_MS));
	        timer.async_wait(boost::bind(&FTSP::timeout, this));
	
		break;
	
	// ROOT
	case STATE_ROOT_WAIT_CAPTURE2:

		if (DEBUG) std::cout << "FTSP::ROOT capture 2 received" << std::endl;	

		// Set top zero
		for (int i = 0; i < 3; i++)
			sync.pars[i] = sync.vars[i] = 0.0;
		sync.id = stamp->timerid();
		broadcast(sync);

		state = STATE_SENDING_FINISHED;

		break;

	case STATE_WAITING_FOR_RADIO1:

		if (DEBUG) std::cout << "FTSP::NODE waiting for radio 1 but capture received, so ignoring " << t << std::endl;	
		state = STATE_WAITING_FOR_RADIO1;
		break;

	case STATE_WAITING_FOR_RADIO2:

		if (DEBUG) std::cout << "FTSP::NODE waiting for radio 2 but capture received, so ignoring " << t << std::endl;	
		state = STATE_WAITING_FOR_RADIO1;
		break;

	// NODE: normal behaviour
	case STATE_WAITING_FOR_CAPTURE1:

		if (DEBUG) std::cout << "FTSP::NODE capture 1 received " << t << std::endl;	

		cached_capture = t;

		state = STATE_WAITING_FOR_RADIO2;

		break;

	case STATE_WAITING_FOR_CAPTURE2:

		if (DEBUG) std::cout << "FTSP::NODE capture 2 received " << t << std::endl;	

		//state = STATE_WAITING_FOR_RADIO1;

		timeout();
		
		break;

	}
	
}

void FTSP::cb_radio(uint8_t type, uint8_t* payload, uint32_t len)
{
	// If this node is not active, or this is not a FTSP strobe message
	if (!active || type != MSG_TYPE_FTSP_STROBE)
		return;

	// We know this, base don the message type
	StrobeMsg* strobe = (StrobeMsg*) payload;

	// If we are not waiting for a measurement, or this is not the right message type
	switch (state)
	{

	// NODE: normal behaviour
	case STATE_WAITING_FOR_CAPTURE1:

		if (DEBUG) std::cout << "FTSP::NODE Radio received, but waiting for capture 1, so ignoring" << std::endl;
		state = STATE_WAITING_FOR_RADIO1;
		break;

	// NODE: normal behaviour
	case STATE_WAITING_FOR_CAPTURE2:

		if (DEBUG) std::cout << "FTSP::NODE Radio received, but waiting for capture 2, so ignoring" << std::endl;
		state = STATE_WAITING_FOR_RADIO1;
		break;

	// NODE: normal behaviour
	case STATE_WAITING_FOR_RADIO1:
		
		if (DEBUG) std::cout << "FTSP::NODE Radio 1 received" << std::endl;
		if (strobe->seq == 0)
		{
			state = STATE_WAITING_FOR_CAPTURE1;
			stamp->poll();
		}
		else
			if (DEBUG) std::cout << "FTSP::NODE Waiting on Radio 1 but got Radio 2 " << strobe->stamp << std::endl;

		break;

	case STATE_WAITING_FOR_RADIO2:
		
		if (DEBUG) std::cout << "FTSP::NODE Radio 2 received " << strobe->stamp << std::endl;
		if (strobe->seq == 1)
		{
			cached_radio = strobe->stamp;
			state = STATE_WAITING_FOR_CAPTURE2;
			stamp->poll();
		}
		else
		{
			if (DEBUG) std::cout << "FTSP::NODE Waiting on Radio 2 but got Radio 1 " << strobe->stamp << std::endl;
			state = STATE_WAITING_FOR_RADIO1;
		}

		
		break;
	}

}

void FTSP::event(const boost::system::error_code& e)
{
	if (!active || e) return;

	// Reschedule the next event
     	timer_event.expires_from_now(boost::posix_time::milliseconds(interval_ms));
        timer_event.async_wait(boost::function<void (const boost::system::error_code&)>( boost::bind( &FTSP::event, this, _1 ) ));
 	


	// We are now root
	if (root)
	{
		if (state == STATE_INIT) std::cout << "FTSP::INIT I am root " << std::endl;
		if (state == STATE_INIT) msg.root == radio->addr_id();
		state = STATE_SENDING_STROBE1;
		timeout();
	}
	else
	{
		if (state == STATE_INIT) std::cout << "FTSP::INIT I am a node " << std::endl;
		if (state == STATE_INIT) state = STATE_WAITING_FOR_RADIO1;
	}
}

void FTSP::timeout()
{
	// ROOT: start a strobe
	if (root)
	{
		if (DEBUG) std::cout << "FTSP::ROOT tx " << cached_radio << std::endl;

		msg.stamp = cached_radio;

		// Prepare the timestamp mechanism to poll
		if (state == STATE_SENDING_STROBE2) 
		{
			msg.seq = 1;
			state = STATE_ROOT_WAIT_CAPTURE2;
		}
		else
		{
			msg.seq = 0;
			state = STATE_ROOT_WAIT_CAPTURE1;
		}

		// Send a radio frame
		radio->send(
			(uint8_t) MSG_TYPE_FTSP_STROBE, 
			(uint8_t*) &msg, 
			(uint32_t) sizeof(msg)
		);

	}
	// NODE: this signals the end of a strobe, so we must calculate sync parameters
	else
	{
		// Flash LED to signal end of strobe
		led_on();

		if (DEBUG) std::cout << "FTSP::NODE timeout() -> Strobe finished " << count << std::endl;
		if (DEBUG) std::cout << std::setprecision(12);

		//cached_capture -= stamp->rxlatency();
		//cached_radio   -= stamp->txlatency();

		if (!TEST || toggle)
		{
			// Discipline the logical clock
			clk->add(cached_capture,cached_radio);

			// Data for stroring the sync parameters
			uint32_t num = clk->getsync(sync);
			sync.id = stamp->timerid();

			// Send these new sync values to the world!
			broadcast(sync);
		}
		else
		{
			// Predict the global time
			double test_l = cached_capture;
			double test_g = test_l 														// Reference
			              + sync.pars[0] 												// Offset
			              + sync.pars[1] * (test_l - sync.t0)							// Drift
			              + sync.pars[2] * (test_l - sync.t0) * (test_l - sync.t0);		// Drift rate

			// Let's assume that we are looking to trigger a common event every 1 second. Let's
			// find the global trigger count for the next event.
			double trig_g = (cached_radio / 24000000.0 + 1.0) * 24000000.0;

			// Using the quadratic solution, find the expected error
			double a = sync.pars[2];
			double b = 1.0 + sync.pars[1] - 2.0*sync.pars[2]*sync.t0;
			double c = sync.pars[2] * sync.t0 * sync.t0 - sync.pars[1] * sync.t0 + sync.pars[0] - trig_g;
			double x = (-b + sqrt(b*b - 4.0*a*c)) / (2 * a);
			double y = x  + sync.pars[0] 										// Offset
			              + sync.pars[1] * (x - sync.t0)						// Drift
			              + sync.pars[2] * (x - sync.t0) * (x - sync.t0);		// Drift rate

			// Now work out the local time
			uint64_t trig_l = round(x);

			// Test the trigger
			stamp->trigger(trig_l);

			// Compare prediction to actual to determine accuracy
			std::cout << "CURR : " << cached_radio << std::endl;
			std::cout << "NEXT : " << trig_g << std::endl;
			std::cout << "YVAL : " << y << std::endl;
			std::cout << "ERROR : " << (cached_radio - test_g) << std::endl;
			for (int i = 0; i < 3; i++)
				std::cout << "X[" << i << "] : " << sync.pars[i] << " P[" << i << "] : " << sync.vars[i] << std::endl;
			std::cout << "TRIGR : " << trig_l << std::endl;
			std::cout << "X : " << x << std::endl;

			// Cache the error
			//error = cached_radio - test_g;
		}

		toggle = !toggle;

		state = STATE_WAITING_FOR_RADIO1;
	}
	
}

void FTSP::stats()
{
	// Data for stroring the sync parameters
	roseline_sync sync;

	// Query the sync information
	uint32_t num = clk->getsync(sync);

	// Some bud info
	if (DEBUG) std::cout << "FTSP: Using " << num << " measurements " << std::endl;
}

uint32_t FTSP::getsync(roseline_sync &sync)
{
	return clk->getsync(sync);
}

void FTSP::configure(const roseline_pars &pars)
{
	// Does this timing information refer to us?
	if (pars.id == stamp->timerid())
	{
		std::cout << "New configuration sync duration request: " << pars.error << " ms" << std::endl;
		if (pars.error >= 100)
		{
			interval_ms = pars.error;
     			timer_event.expires_from_now(boost::posix_time::milliseconds(interval_ms));
     			timer_event.async_wait(boost::function<void (const boost::system::error_code&)>( boost::bind( &FTSP::event, this, _1 ) ));
     		}
	}
}

void FTSP::led_on()
{
	std::fstream fs;
	fs.open ("/sys/class/leds/beaglebone:green:heartbeat/brightness", std::fstream::out);
	fs << "1";
	fs.close();
	timer_led.expires_from_now(boost::posix_time::milliseconds(CONFIG_LED_MS));
	timer_led.async_wait(boost::bind(&FTSP::led_off, this));
}

void FTSP::led_off()
{
	std::fstream fs;
	fs.open ("/sys/class/leds/beaglebone:green:heartbeat/brightness", std::fstream::out);
	fs << "0";
	fs.close();
}