#ifndef AT86RF233_H
#define AT86RF233_H

#define AT86RF233_MAX_PACKET_LEN 127

#include <boost/thread.hpp>

#include "LogicalRadio.hpp"

namespace timesync
{
        class AT86RF233 : public LogicalRadio
        {
        	// Constructor / destructor
        	public: AT86RF233(
        		boost::asio::io_service *io,				// ASIO service
        		const uint16_t &mypan, 					// PAN
        		const uint16_t &myaddr					// ID
        	);
        	public: ~AT86RF233();

		// Inherited methods 
        	public: bool start();						// Start radio
		public: bool stop();						// Stop radio
		public: uint16_t addr_id();					// Retrieve my ID
		public: uint16_t addr_bc();					// Retrieve broadcast ID
		public: bool send(uint8_t type, uint8_t *data, uint32_t len);	// Send data
		public: void callback(RxCallback cb);				// Stop radio

		// Receive callback
		public: RxCallback cb_rcv;					// Callback on recevied data			

		// Implementation-specific methods
		private: void listen();						// Listen work
		private: boost::thread lthread;					// Listen thread
		private: int sd;						// Device descriptor
		private: uint16_t addr, pan;					// My address and pan
		private: uint8_t txbuff[AT86RF233_MAX_PACKET_LEN];		// Packet buffer (TX)
		private: uint8_t rxbuff[AT86RF233_MAX_PACKET_LEN];		// Packet buffer (RX)
        };
}

#endif
