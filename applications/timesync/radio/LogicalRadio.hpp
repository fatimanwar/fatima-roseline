#ifndef LOGICAL_RADIO_H
#define LOGICAL_RADIO_H

// Standard integer types
#include <cstdint>

// Required for logging and shared pointers
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

// Radio callback
typedef boost::function<void(uint8_t type, uint8_t *data, uint32_t len)> RxCallback;	

namespace timesync
{
        // Filter type
        enum LogicalRadioType
        {
		RADIO_AT86RF233,
		//RADIO_CC2620
        };

        // Base functionality provided by filters
        class LogicalRadio
        {
		public: virtual bool start() = 0;						// Start radio
		public: virtual bool stop() = 0;						// Stop radio
		public: virtual uint16_t addr_id() = 0;						// Retrieve my ID
		public: virtual uint16_t addr_bc() = 0;						// Retrieve broadcast ID
		public: virtual bool send(uint8_t type, uint8_t *data, uint32_t len) = 0;	// Retrieve broadcast ID
		public: virtual void callback(RxCallback cb) = 0;				// Set the RX callback
		public: static boost::shared_ptr<LogicalRadio> Factory(
			LogicalRadioType type, 							// The hardware type
			boost::asio::io_service *io,						// IO service
			const uint16_t &mypan, 							// PAN
			const uint16_t &myaddr							// ADDRESS
		);
        };
}

#endif