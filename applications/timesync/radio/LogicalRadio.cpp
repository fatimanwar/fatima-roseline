// Base type
#include "LogicalRadio.hpp"

// Subtypes
#include "AT86RF233.hpp"
//#include "CC2620.hpp"

using namespace timesync;

boost::shared_ptr<LogicalRadio> LogicalRadio::Factory(
	LogicalRadioType type, 							// The hardware type
	boost::asio::io_service *io,						// IO service
	const uint16_t &mypan, 							// PAN
	const uint16_t &myaddr)							// ADDRESS

{
	switch(type)
	{
		case RADIO_AT86RF233: 	return boost::shared_ptr<LogicalRadio>((LogicalRadio*) new AT86RF233(io,mypan,myaddr)); 	break;
		//case RADIO_CC2620: 	return boost::shared_ptr<LogicalRadio>((LogicalRadio*) new CC2620(mypan,myaddr));	break;
	}
	return boost::shared_ptr<LogicalRadio>((LogicalRadio*) new AT86RF233(io,mypan,myaddr));

}