#include "AT86RF233.hpp"

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Needed for sockets
#include "ieee802154.h"

#define DEBUG 0

using namespace timesync;

// Initialise with the callback
AT86RF233::AT86RF233(boost::asio::io_service *io, const uint16_t &mypan, const uint16_t &myaddr)
	: pan(mypan), addr(myaddr)
{
	// Nothing yet
}

// Initialise with the callback
AT86RF233::~AT86RF233()
{
	stop();
}

void AT86RF233::callback(RxCallback cb)
{
	if (DEBUG) std::cout << "AT86RF233: callback()" << std::endl;
	cb_rcv = cb;
}

void AT86RF233::listen()
{
	if (DEBUG) std::cout << "AT86RF233: listen()" << std::endl;

	// Keep going until the thread is interrupted
	try
	{
		// 500ms  maxtimeout on blocking (this can be shortened)
		struct timeval timeout = { .tv_sec = 0, .tv_usec = 500000 };

		// Keep going until blocked
		while (1)
		{
			fd_set rs, ws, xs;
			int32_t ret;

			FD_ZERO(&rs);
			FD_ZERO(&ws);
			FD_ZERO(&xs);
			FD_SET(sd, &rs);
			FD_SET(sd, &xs);
			FD_SET(0, &rs);

			// Wait for incoming packets
			ret = select(sd + 1, &rs, &ws, &xs, NULL);
			if (FD_ISSET(sd, &rs))
			{
				ret = read(sd, &rxbuff, sizeof(rxbuff));
				//std::cout << "Something arrived of size " << ret << std::endl;
				if (ret > 0 && cb_rcv) 
					cb_rcv(rxbuff[0], &rxbuff[1], ret-1);
			}
		}
	}
 	catch(boost::thread_interrupted const&)
    	{
    		// Nothing left to do here
        	return;
    	}
}

bool AT86RF233::start()
{
	if (DEBUG) std::cout << "AT86RF233: start()" << std::endl;

	// Return status and socket
	struct sockaddr_ieee802154 a;

	// Open an IEEE 802.15.4 socket
	sd = socket(PF_IEEE802154, SOCK_DGRAM, 0);
	if (sd < 0)
	{
		std::cerr << "Could not open the socket: " << sd << std::endl;
		return false;
	}

	// Set the PAN and address
	a.family = AF_IEEE802154;
	a.addr.addr_type = IEEE802154_ADDR_SHORT;
	a.addr.pan_id = 0xAE70;
	a.addr.short_addr = addr;
	if (bind(sd, (struct sockaddr *)&a, sizeof(a)))
	{
		std::cerr << "Could not bind to the socket" << std::endl;
		return false;
	}

	// Set broadcast address (all neighbours should respond)
	a.addr.short_addr = 0xFFFF;
	if (connect(sd,(struct sockaddr *)&a, sizeof(a)))
	{
		std::cerr << "Could not connect to the socket" << std::endl;
		return false;
	}

	// Thread to continually listen for packets
	lthread = boost::thread(boost::bind(&AT86RF233::listen, this));
    
	// Success!
	return true;
}

bool AT86RF233::stop()
{
	if (DEBUG) std::cout << "AT86RF233: stop()" << std::endl;

	// Interrupt the thread
	lthread.interrupt();
	
	// Allocate 1s for the thread to die
	bool ret = (lthread.timed_join(boost::posix_time::milliseconds(1000)));

        // clocke the socket
        close(sd);

        // Thread did not die
        return ret;
}

uint16_t AT86RF233::addr_id()
{
	if (DEBUG) std::cout << "AT86RF233: addr_id()" << std::endl;
	return addr;
}

uint16_t AT86RF233::addr_bc()
{
	if (DEBUG) std::cout << "AT86RF233: addr_bc()" << std::endl;
	return 0xFFFF;
}

// Transmit a response
bool AT86RF233::send(uint8_t type, uint8_t *data, uint32_t len)
{
	if (DEBUG) std::cout << "AT86RF233: send()" << std::endl;

	// Check packet length is less than max
	if (len > AT86RF233_MAX_PACKET_LEN)
		return false;

	// Append a type to the front of the frame
	txbuff[0] = type;
	memcpy(&(txbuff[1]),data,len);

	// Send the packet
	return (write(sd, txbuff, len+1) == len+1);
}
