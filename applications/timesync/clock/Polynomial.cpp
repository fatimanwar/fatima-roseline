#include "Polynomial.hpp"

#include <iostream>

using namespace timesync;

// PRIVATE METHODS ////////////////////////////////////////////////////////////////

// Linearized measurement matrix
void Polynomial::LinearizeH()
{
	H[_diff][_a0] = 1.0;
	H[_diff][_a1] = (double) dt;
	H[_diff][_a2] = (double) dt * (double) dt;
}


void Polynomial::MeasurementEq()
{
	Y[_diff] = X[_a0] + X[_a1] * (double) dt + X[_a2] * (double) dt * (double) dt;
}

// Perform the state update
void Polynomial::SerialUpdate()
{
	double HP[NUM_STA], HPHR, Error, K[NUM_STA][NUM_OBS];

	// Iterate over relevant measurements
	for (uint16_t m = 0; m < NUM_OBS; m++)
	{
		for (int j = 0; j < NUM_STA; j++)
		{	
			// Find Hp = H*P
			HP[j] = 0;
			for (int k = 0; k < NUM_STA; k++)
				HP[j] += H[m][k] * P[k][j];
		}
		HPHR = R[m];	

		// Find  HPHR = H*P*H' + R
		for (int k = 0; k < NUM_STA; k++)
			HPHR += HP[k] * H[m][k];

		// find K = HP/HPHR
		for (int k = 0; k < NUM_STA; k++)
			K[k][m] = HP[k] / HPHR;

		for (int i = 0; i < NUM_STA; i++)
		{	
			// Find P(m)= P(m-1) + K*HP
			for (int j = i; j < NUM_STA; j++)
				P[i][j] = P[j][i] = P[i][j] - K[i][m] * HP[j];
		}

		// Find X(m)= X(m-1) + K*Error
		Error = Z[m] - Y[m];
		for (int i = 0; i < NUM_STA; i++)
			X[i] = X[i] + K[i][m] * Error;	
	}
}


Polynomial::Polynomial()
{
	// Set all initial values
	reset();
}

uint32_t Polynomial::entries()
{
	return n;
}

void Polynomial::reset()
{
	// Reset number of entries
	n = 0;				
	
	// Reset the state
	for (int i = 0; i < NUM_STA; i++)
	{
		X[i] = 0.0;
		for (int j = 0; j < NUM_STA; j++)
			P[i][j] = 0.0;
	}

	// There are 24 clock ticks in 1us. Since the spread of the
	// AT86RF233 is 1us, it follows that the difference between
	// two interrupts (RX and TX) is the addition of two GRV
	// with 24 clock ticks spread each = 24^2 + 24^2
	R[_diff] = 1152.0;

	// These are the most important parameters to ensure that
	// the filter converges quickly.
	P[_a0][_a0] = 1152.0;		// See above
	P[_a1][_a1] = 10e-4;		// Accuracy
	P[_a2][_a2] = 10e-6;		// Ageing

	// Reset reference
	t0 = 0;
	lt = 0;
}

void Polynomial::add(uint64_t localTime, uint64_t globalTime)
{
	// Set ourselves a t0 if needed, but don't use record!
	if (t0 == 0)
	{
		// Set the reference epoch
		t0 = localTime;
		
		// Boostrap our estimate
		X[_a0] = (double)(globalTime) - (double)(localTime); 

		// Do not filter the first measurement
		return;
	}

	// Increment the number of entries
	n++;

	// Work out the time elapsed since epoch
	dt = localTime - t0;

	// Increase state covariance to represent tracking uncertainty
	/*
	if (lt > 0)
	{
		P[_a0][_a0] += (double) (localTime - lt) / 2400000000;		// See R[_diff] documentation
		P[_a1][_a1] += (double) (localTime - lt) / 24000000000;		// Accuracy
		P[_a2][_a2] += (double) (localTime - lt) / 240000000000;	// Ageing
	}
	lt = localTime;
	*/

	// Measurment equation
	this->MeasurementEq();

	// Linearized measurement
	this->LinearizeH();

	// Measurement is difference between global and local time
	Z[_diff] = (double)(globalTime) - (double)(localTime);

	// Serial update with velocity update
	this->SerialUpdate();
}

uint32_t Polynomial::getsync(roseline_sync &sync)
{
	// Reference epoch
	sync.t0 = (double) t0;

	// Offset
	sync.pars[0] = X[_a0];
	sync.vars[0] = P[_a0][_a0];

	// Drift
	sync.pars[1] = X[_a1];
	sync.vars[1] = P[_a1][_a1];

	// Drift rate
	sync.pars[2] = X[_a2];
	sync.vars[2] = P[_a2][_a2];

	// success
	return entries();
}