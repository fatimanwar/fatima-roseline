#include "Regression.hpp"

using namespace timesync;


#include <stdio.h>                          /* standard i/o                  */
#include <string.h>                         /* string functions              */
#include <math.h>                           /* math functions                */


void Regression::reset()
{
	n = 0;
	next = 0;
}	

void Regression::add(uint64_t localTime, uint64_t globalTime)
{
	// Map from a local time to an error
	data[next][0] = (double) localTime;
	data[next][1] = (double) globalTime - (double) localTime;

	// update table size
	if (n < REGRESSION_TABLE_SIZE) 
		n++;

	// Temp variables
	double   sumx = 0.0;                        /* sum of x                      */
	double   sumx2 = 0.0;                       /* sum of x**2                   */
	double   sumxy = 0.0;                       /* sum of x * y                  */
	double   sumy = 0.0;                        /* sum of y                      */
	double   sumy2 = 0.0;                       /* sum of y**2                   */
	double   x;                                 /* input x data                  */
	double   y;                                 /* input y data                  */

	// k = l_0
	double k = data[0][0];

	//
	double c = 0;
	for (int i = 0; i < n; i++)
	{
		c     += 1.0;                      		   /* increment num of data points  */
		sumx  += (data[i][0] - k);                         /* compute sum of x              */
		sumx2 += (data[i][0] - k) * (data[i][0] - k);      /* compute sum of x**2           */
		sumxy += (data[i][0] - k) * data[i][1];            /* compute sum of x * y          */
		sumy  += data[i][1];                           	   /* compute sum of y              */
		sumy2 += data[i][1] * data[i][1];                       /* compute sum of y**2           */
	}                                        	/* loop again for more data      */

	m = (c * sumxy  -  sumx * sumy) /        	/* compute slope                 */
		(c * sumx2 - sumx*sumx);

	b = (sumy * sumx2  -  sumx * sumxy) /    	/* compute y-intercept           */
		(c * sumx2  -  sumx*sumx);

	r = k;						/* local offset */

	// move pointer to the next free entry
	next = (next + 1) % REGRESSION_TABLE_SIZE;
}

uint32_t Regression::entries()
{
	return n;
}

uint32_t Regression::getsync(roseline_sync &sync)
{
	// Reference epoch
	sync.t0 = r;

	// Offset
	sync.pars[0] = b;
	sync.vars[0] = 0.0;

	// Drift
	sync.pars[1] = m;
	sync.vars[1] = 0.0;

	// Drift rate
	sync.pars[2] = 0.0;
	sync.vars[2] = 0.0;

	// success
	return entries();
}
