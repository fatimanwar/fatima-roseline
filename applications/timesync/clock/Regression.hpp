#ifndef REGRESSION_CLOCK_H
#define REGRESSION_CLOCK_H

#define REGRESSION_TABLE_SIZE 6

#include "LogicalClock.hpp"

namespace timesync
{
    	// Structure of an item in the regression table
	typedef struct TableItem
	{
	  uint64_t localTime;
	  int64_t  offset;
	} TableItem;

	// A logical clock that uses regression
        class Regression : public LogicalClock
        {
	        // Inherited methods
	        public: uint32_t entries();					// Entry counter
	        public: void reset();						// Reset the clock
		public: void add(uint64_t localTime, uint64_t globalTime);	// Add a local -> global map
		public: uint32_t getsync(roseline_sync &sync);			// Sync parameters

		// Member variables
		private: double data[REGRESSION_TABLE_SIZE][2];
		private: uint8_t next, n;
		private: double m, b, r;
        };
}

#endif
