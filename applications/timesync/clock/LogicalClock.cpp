// Base type
#include "LogicalClock.hpp"

// Subtypes
#include "Regression.hpp"
#include "Polynomial.hpp"

using namespace timesync;

boost::shared_ptr<LogicalClock> LogicalClock::Factory(LogicalClockType type)
{
	switch(type)
	{
		case CLOCK_REGRESSION: return boost::shared_ptr<LogicalClock>((LogicalClock*) new Regression());
		case CLOCK_POLYNOMIAL: return boost::shared_ptr<LogicalClock>((LogicalClock*) new Polynomial());
	}
	return boost::shared_ptr<LogicalClock>((LogicalClock*) new Regression());
}