#ifndef POLYNOMIAL_CLOCK_H
#define POLYNOMIAL_CLOCK_H

#include "LogicalClock.hpp"

namespace timesync
{
	// State
	enum State
	{
		_a0   	= 0,	// a0
		_a1   	= 1,	// a1 * (t - t0)
		_a2   	= 2,	// a2 * (t - t0)^2
		NUM_STA = 3
	};

	// Observation
    	enum Noise
	{
		_ng      = 0,
		NUM_NOI  = 1
	};

	// Observation
	enum Observation
	{
		_diff     = 0,	// Observation of (global - local)
		NUM_OBS  = 1
	};

	class Polynomial : public LogicalClock
	{
		// Inherited methods
		public: uint32_t entries();									// Entry counter
		public: void reset();										// Reset the clock
		public: void add(uint64_t localTime, uint64_t globalTime);	// Add a local -> global map
		public: uint32_t getsync(roseline_sync &sync);				// Sync parameters

		// Internal methods
		private: void LinearizeH();
		private: void MeasurementEq();
		private: void SerialUpdate();

		// Implementation-specific methods
		public:  Polynomial();					// Constructor
		private: uint64_t n, t0, dt, lt;		// Last number of entries
		private: double X[NUM_STA];				// State
		private: double Y[NUM_OBS];				// Prediction
		private: double Z[NUM_OBS];				// Observation
		private: double F[NUM_STA][NUM_STA];	// Process model
		private: double G[NUM_STA][NUM_NOI];	// Noise model
		private: double P[NUM_STA][NUM_STA];	// Covariance
		private: double H[NUM_OBS][NUM_STA];	// Measurement model
		private: double Q[NUM_STA];				// Process covariance
		private: double R[NUM_OBS];				// Measurement covariance
	};
}

#endif
