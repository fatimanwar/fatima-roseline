#ifndef LOGICAL_CLOCK_H
#define LOGICAL_CLOCK_H

// Standard integer types
#include <cstdint>

// Required for logging and shared pointers
#include <boost/shared_ptr.hpp>

// PRovides some timer information
#include "../../../core/modules/roseline_ioctl.h"

namespace timesync
{
        // Filter type
        enum LogicalClockType
        {
		CLOCK_REGRESSION,
                CLOCK_POLYNOMIAL
        };

        // Base functionality provided by filters
        class LogicalClock
        {
		public: virtual uint32_t entries() = 0;						// Number of entries in the table
		public: virtual void reset() = 0;						// Reset the clock
		public: virtual void add(uint64_t localTime, uint64_t globalTime) = 0;		// Add a local -> global map
		public: virtual uint32_t getsync(roseline_sync &sync) = 0;			// Get the sync parameters
		public: static boost::shared_ptr<LogicalClock> Factory(LogicalClockType type);	// Factory method
        };
}

#endif