// Base type
#include "LogicalTruth.hpp"

// Subtypes
#include "GNSS.hpp"

using namespace timesync;

boost::shared_ptr<LogicalTruth> LogicalTruth::Factory(LogicalTruthType type)
{
	switch(type)
	{
		case TRUTH_GNSS: return boost::shared_ptr<LogicalTruth>((LogicalTruth*) new GNSS());
	}
	return boost::shared_ptr<LogicalTruth>((LogicalTruth*) new GNSS());
}