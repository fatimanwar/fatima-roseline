/****************************************************************************
 *
 *   Copyright (c) 2012-2015 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include "UBX.h"

#define UBX_CONFIG_TIMEOUT		200			// ms, timeout for waiting ACK
#define UBX_PACKET_TIMEOUT		2			// ms, if now data during this delay assume that full update received
#define UBX_WAIT_BEFORE_READ	20			// ms, wait before reading to save read() calls
#define DISABLE_MSG_INTERVAL	1000000		// us, try to disable message with this interval

#define MIN(X,Y)	((X) < (Y) ? (X) : (Y))
#define SWAP16(X)	((((X) >>  8) & 0x00ff) | (((X) << 8) & 0xff00))
#define UBX_TRACE_PARSER(s, ...)	{/*printf(s, ## __VA_ARGS__);*/}	/* decoding progress in parse_char() */
#define UBX_TRACE_RXMSG(s, ...)		{/*printf(s, ## __VA_ARGS__);*/}	/* Rx msgs in payload_rx_done() */
#define UBX_TRACE_SVINFO(s, ...)	{/*printf(s, ## __VA_ARGS__);*/}	/* NAV-SVINFO processing (debug use only, will cause rx buffer overflows) */
#define UBX_WARN(s, ...)		{ /*printf(s, ## __VA_ARGS__);*/ }

// Constructor
UBX::UBX() :
	_configured(false),
	_ack_state(UBX_ACK_IDLE),
	_got_posllh(false),
	_got_velned(false),
	_disable_cmd_last(0),
	_ack_waiting_msg(0),
	_ubx_version(0),
	_use_nav_pvt(false)
{
	// Initialise the decoder
	decode_init();
}

// Initialise
bool UBX::init(const std::string &port, const unsigned short &baud)
{
   // Open serial port @ 9600
    try
    {
    	// Open the serial port
        serial.open(port.c_str(), baud);
        if (serial.isOpen())
        {
		// Configure to use the UBLOX protocol
		memset(&_buf.payload_tx_cfg_prt, 0, sizeof(_buf.payload_tx_cfg_prt));
		_buf.payload_tx_cfg_prt.portID		 = UBX_TX_CFG_PRT_PORTID;
		_buf.payload_tx_cfg_prt.mode		 = UBX_TX_CFG_PRT_MODE;
		_buf.payload_tx_cfg_prt.baudRate	 = 9600;
		_buf.payload_tx_cfg_prt.inProtoMask	 = UBX_TX_CFG_PRT_INPROTOMASK;
		_buf.payload_tx_cfg_prt.outProtoMask = UBX_TX_CFG_PRT_OUTPROTOMASK;
		send_message(UBX_MSG_CFG_PRT, _buf.raw, sizeof(_buf.payload_tx_cfg_prt));

		/* Send a CFG-RATE message to define update rate */
		memset(&_buf.payload_tx_cfg_rate, 0, sizeof(_buf.payload_tx_cfg_rate));
		_buf.payload_tx_cfg_rate.measRate	= UBX_TX_CFG_RATE_MEASINTERVAL;
		_buf.payload_tx_cfg_rate.navRate	= UBX_TX_CFG_RATE_NAVRATE;
		_buf.payload_tx_cfg_rate.timeRef	= UBX_TX_CFG_RATE_TIMEREF;
		send_message(UBX_MSG_CFG_RATE, _buf.raw, sizeof(_buf.payload_tx_cfg_rate));

		/* Set 5Hz callback for receive data */
		configure_message_rate(UBX_MSG_TIM_TM2, 1);

		// Yes please
		_configured = true;

		/* Set the callback for receive data */
		serial.setCallback(boost::bind(&UBX::parse,this,_1,_2));

		// Success
		return true;
	}
    }
    catch (boost::system::system_error e)
    {
        std::cerr << "Could not open the serial port" << std::endl;
    }

    // Failure
    return false;
}

/* Get the most recent time stamp */
bool UBX::read(uint64_t &cnt)
{
 	// Get the ms/ns time of falling edge
	uint64_t ms = tim.towMsF;
	uint64_t ns = tim.towSubMsF;

	// Combine into nanoseconds
	cnt = ms * 1e6 + ns;

	// Reset to zero
	ms = 0;
	ns = 0;

 	// Success!
 	return (cnt > 0);
}


// Destructor
UBX::~UBX() {}

// Parse the binary UBX packet
void UBX::parse(const char *byte, size_t cnt)
{
    // 0 = decoding, 1 = message handled, 2 = sat info message handled
    for (int i = 0; i < cnt; i++)
    	parse_char(byte[i]);
}

// Parse a single character
int	UBX::parse_char(const uint8_t b)
{
	int ret = 0;

	switch (_decode_state)
	{

	/* Expecting Sync1 */
	case UBX_DECODE_SYNC1:
		if (b == UBX_SYNC1) {	// Sync1 found --> expecting Sync2
			UBX_TRACE_PARSER("\nA");
			_decode_state = UBX_DECODE_SYNC2;
		}
		break;

	/* Expecting Sync2 */
	case UBX_DECODE_SYNC2:
		if (b == UBX_SYNC2) {	// Sync2 found --> expecting Class
			UBX_TRACE_PARSER("B");
			_decode_state = UBX_DECODE_CLASS;

		} else {		// Sync1 not followed by Sync2: reset parser
			decode_init();
		}
		break;

	/* Expecting Class */
	case UBX_DECODE_CLASS:
		UBX_TRACE_PARSER("C");
		add_byte_to_checksum(b);  // checksum is calculated for everything except Sync and Checksum bytes
		_rx_msg = b;
		_decode_state = UBX_DECODE_ID;
		break;

	/* Expecting ID */
	case UBX_DECODE_ID:
		UBX_TRACE_PARSER("D");
		add_byte_to_checksum(b);
		_rx_msg |= b << 8;
		_decode_state = UBX_DECODE_LENGTH1;
		break;

	/* Expecting first length byte */
	case UBX_DECODE_LENGTH1:
		UBX_TRACE_PARSER("E");
		add_byte_to_checksum(b);
		_rx_payload_length = b;
		_decode_state = UBX_DECODE_LENGTH2;
		break;

	/* Expecting second length byte */
	case UBX_DECODE_LENGTH2:
		UBX_TRACE_PARSER("F");
		add_byte_to_checksum(b);
		_rx_payload_length |= b << 8;	// calculate payload size
		if (payload_rx_init() != 0) {	// start payload reception
			// payload will not be handled, discard message
			decode_init();
		} else {
			_decode_state = (_rx_payload_length > 0) ? UBX_DECODE_PAYLOAD : UBX_DECODE_CHKSUM1;
		}
		break;

	/* Expecting payload */
	case UBX_DECODE_PAYLOAD:
		UBX_TRACE_PARSER(".");
		add_byte_to_checksum(b);
		switch (_rx_msg)
		{
		default:
			ret = payload_rx_add(b);		// add a payload byte
			break;
		}
		if (ret < 0) {
			// payload not handled, discard message
			decode_init();
		} else if (ret > 0) {
			// payload complete, expecting checksum
			_decode_state = UBX_DECODE_CHKSUM1;
		} else {
			// expecting more payload, stay in state UBX_DECODE_PAYLOAD
		}
		ret = 0;
		break;

	/* Expecting first checksum byte */
	case UBX_DECODE_CHKSUM1:
		if (_rx_ck_a != b) {
			UBX_WARN("ubx checksum err");
			decode_init();
		} else {
			_decode_state = UBX_DECODE_CHKSUM2;
		}
		break;

	/* Expecting second checksum byte */
	case UBX_DECODE_CHKSUM2:
		if (_rx_ck_b != b) {
			UBX_WARN("ubx checksum err");
		} else {
			ret = payload_rx_done();	// finish payload processing
		}
		decode_init();
		break;

	default:
		break;
	}

	return ret;
}

int	UBX::payload_rx_init()
{
	int ret = 0;
	_rx_state = UBX_RXMSG_HANDLE;	// handle by default
	switch (_rx_msg)
	{
	case UBX_MSG_TIM_TM2:
		if (_rx_payload_length != sizeof(ubx_payload_rx_tim_tm2_t))
			_rx_state = UBX_RXMSG_ERROR_LENGTH;
		else if (!_configured)
			_rx_state = UBX_RXMSG_IGNORE;	// ignore if not _configured
		break;
	default:
		_rx_state = UBX_RXMSG_DISABLE;	// disable all other messages
		break;
	}

	switch (_rx_state)
	{
	case UBX_RXMSG_HANDLE:	// handle message
	case UBX_RXMSG_IGNORE:	// ignore message but don't report error
		ret = 0;
		break;

	case UBX_RXMSG_DISABLE:	// disable unexpected messages
		UBX_WARN("ubx msg 0x%04x len %u unexpected", SWAP16((unsigned)_rx_msg), (unsigned)_rx_payload_length);
		ret = -1;	// return error, abort handling this message
		break;

	case UBX_RXMSG_ERROR_LENGTH:	// error: invalid length
		UBX_WARN("ubx msg 0x%04x invalid len %u", SWAP16((unsigned)_rx_msg), (unsigned)_rx_payload_length);
		ret = -1;	// return error, abort handling this message
		break;

	default:	// invalid message state
		UBX_WARN("ubx internal err1");
		ret = -1;	// return error, abort handling this message
		break;
	}

	return ret;
}

// -1 = error, 0 = ok, 1 = payload completedint
int UBX::payload_rx_add(const uint8_t b)
{
	int ret = 0;
	_buf.raw[_rx_payload_index] = b;
	if (++_rx_payload_index >= _rx_payload_length)
		ret = 1;	// payload received completely
	return ret;
}

// 0 = no message handled, 1 = message handled
int UBX::payload_rx_done(void)
{
	int ret = 0;

	// Return if no message handled
	if (_rx_state != UBX_RXMSG_HANDLE)
	{
		return ret;
	}

	// Handle message
	if (_rx_msg == UBX_MSG_TIM_TM2)
	{
		UBX_TRACE_RXMSG("Rx TIM-TM2\n");
		memcpy(&tim,_buf.raw,sizeof(tim));
		//boost::lock_guard<boost::mutex> lk(mx);
        //cv.notify_all();
		ret = 1;
	}

	return ret;
}

void UBX::decode_init(void)
{
	_decode_state = UBX_DECODE_SYNC1;
	_rx_ck_a = 0;
	_rx_ck_b = 0;
	_rx_payload_length = 0;
	_rx_payload_index = 0;
}

void UBX::add_byte_to_checksum(const uint8_t b)
{
	_rx_ck_a = _rx_ck_a + b;
	_rx_ck_b = _rx_ck_b + _rx_ck_a;
}

void UBX::calc_checksum(const uint8_t *buffer, const uint16_t length, ubx_checksum_t *checksum)
{
	for (uint16_t i = 0; i < length; i++) {
		checksum->ck_a = checksum->ck_a + buffer[i];
		checksum->ck_b = checksum->ck_b + checksum->ck_a;
	}
}

void UBX::configure_message_rate(const uint16_t msg, const uint8_t rate)
{
	ubx_payload_tx_cfg_msg_t cfg_msg;	// don't use _buf (allow interleaved operation)
	cfg_msg.msg	 = msg;
	cfg_msg.rate = rate;
	send_message(UBX_MSG_CFG_MSG, (uint8_t *)&cfg_msg, sizeof(cfg_msg));
}

void UBX::send_message(const uint16_t msg, const uint8_t *payload, const uint16_t length)
{
	// Populate header
	ubx_header_t header = {UBX_SYNC1, UBX_SYNC2};
	ubx_checksum_t checksum = {0, 0};
	header.msg = msg;
	header.length = length;

	// Calculate checksum
	calc_checksum(((uint8_t*)&header) + 2, sizeof(header) - 2, &checksum);  // skip 2 sync bytes
	if (payload != NULL)
		calc_checksum(payload, length, &checksum);

    // Send message on the wire
    serial.write((const char *)&header,(size_t)sizeof(header));
    if (payload != NULL)
        serial.write((const char *)payload,length);
    serial.write((const char *)&checksum,(size_t)sizeof(checksum));
}