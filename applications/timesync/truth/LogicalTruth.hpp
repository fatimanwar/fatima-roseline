#ifndef LOGICAL_TRUTH_H
#define LOGICAL_TRUTH_H

// Standard integer types
#include <cstdint>

// Required for logging and shared pointers
#include <boost/shared_ptr.hpp>

namespace timesync
{
        enum LogicalTruthType
        {
		TRUTH_GNSS
        };

        // Base functionality provided by filters
        class LogicalTruth
        {
      		public: static boost::shared_ptr<LogicalTruth> Factory(LogicalTruthType type);
        };
}

#endif