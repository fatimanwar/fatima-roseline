#ifndef LOGICAL_STAMP_H
#define LOGICAL_STAMP_H

// Standard integer types
#include <cstdint>

// PRovides some timer information
extern "C"
{
    #include "../../../core/modules/roseline_ioctl.h"
}

// Required for logging and shared pointers
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

// Callback signature for received 64bit time capture
typedef boost::function<void(bool success,uint64_t ts)> StampCallback;	// Used by Logical Class

namespace timesync
{
    // The logical stamp time class provides an abstract way of obtaining a timestamp
    // for incoming and outgoing messages. The calling class specifies the type of
    // time stamp (SOFTWARE or INPUT_CAPTURE), the timer of interest (TIMER4, TIMER5,
    // TIMER6, TIMER7) and the callback for the time mark. When a new time mark is
    // requested LogicalTimeStamp::poll() is called, and a timer is started. If the
    // time is not passed back from the implementation by STAMP_TIMEOUT_MS ms then a
    // StampCallback(false,XXX) is called. Otherwise, StampCallback(true,XXX) ...

    enum LogicalStampType
    {
        STAMP_SOFTWARE,
        STAMP_INPUTCAPTURE
    };

    // Various configuration options
    enum 
    {
        STAMP_TIMEOUT_MS = 1
    };

    // A logical timestamping required access to a disciplined clock, and a truth
    class LogicalStamp
    {
        // Required constructor
        public: LogicalStamp(boost::asio::io_service *io);

           	// Must be implemented by inheriter
        public: virtual uint64_t rxlatency() = 0;		   // Fixed latency of this stamp type
        public: virtual uint64_t txlatency() = 0;	  	   // Fixed latency of this stamp type
        public: virtual void trigger(uint64_t start) = 0;  // Trigger an event to occur with given params
        protected: virtual void request() = 0;			   // What to do when polled()
        protected: void response(bool status, uint64_t mark);	// Callback for receiving 

        // Offered by the parent class
        private: void timeout();				// Called on timeout
        public: void callback(StampCallback cb);		// Callback
        public: bool poll();					// Calling app -> Iface
        public: virtual TimerName timerid() = 0;		// Get the timer name

        // Callback for stamp events
        private: StampCallback cb_stamp;			// Call back the main function
        private: boost::asio::deadline_timer timer_timeout;	// Timeout timer
        private: bool waiting;					// Waiting state

        // Factory method to generate logical stamp instances
        public: static boost::shared_ptr<LogicalStamp> Factory(
            LogicalStampType type, 				// Stamp type
            boost::asio::io_service *io,			// IO service
            TimerName intimer 				// Timer ID			
        );
    };
}

#endif