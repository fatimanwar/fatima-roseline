// Base type
#include "LogicalStamp.hpp"

// Subtypes
#include "Software.hpp"
#include "InputCapture.hpp"

using namespace timesync;

LogicalStamp::LogicalStamp(boost::asio::io_service *io) 
	:  timer_timeout(*io, boost::posix_time::milliseconds(STAMP_TIMEOUT_MS))	
{}

void LogicalStamp::callback(StampCallback cb)
{
	cb_stamp = cb;

	request();
}

void LogicalStamp::timeout()
{
	// There was a timeout on waiting for the callback
	//if (cb_stamp)
	//	cb_stamp(false, 0);
}

bool LogicalStamp::poll()
{
	// Call the mark function
	request();

	// Success
	return true;
}

void LogicalStamp::response(bool status, uint64_t mark)
{
	// Cancel the timer, rpeventing timeout
	//timer_timeout.cancel();

	// Return the time mark
	cb_stamp(status, mark);
}

boost::shared_ptr<LogicalStamp> LogicalStamp::Factory(
	LogicalStampType type, 				// Stamp type
	boost::asio::io_service *io,			// IO service
	TimerName intimer	 			// Timer ID			
) {
	switch(type)
	{
		case STAMP_SOFTWARE:		
			return boost::shared_ptr<LogicalStamp>((LogicalStamp*) new Software(io, intimer));
		case STAMP_INPUTCAPTURE:	
			return boost::shared_ptr<LogicalStamp>((LogicalStamp*) new InputCapture(io, intimer));
	}
	return boost::shared_ptr<LogicalStamp>((LogicalStamp*) new InputCapture(io, intimer));
}
