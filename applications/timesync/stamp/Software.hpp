#ifndef SOFTWARE_H
#define SOFTWARE_H

#include "LogicalStamp.hpp"

namespace timesync
{
        class Software : public LogicalStamp
        {
        	// Constructor and destructor
               	public: Software(
               		boost::asio::io_service *io,	// IO service
			TimerName intimer
		);
        	public: ~Software();

        	// Timer register
		private: volatile uint32_t *reg;	// Register with current time
		private: int md, fd;			// MMAP and IOCTL descriptors
		private: TimerName timer;		// Timer of interest

               	// The request and response must be implemented by child clases
        	protected: void request();
        public: void trigger(uint64_t start);

        	public: uint64_t rxlatency();	
        	public: uint64_t txlatency();				
		public: TimerName timerid();
                public: uint64_t query();
	};
}

#endif
