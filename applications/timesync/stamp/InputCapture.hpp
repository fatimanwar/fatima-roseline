#ifndef INPUTCAPTURE_H
#define INPUTCAPTURE_H

#include "LogicalStamp.hpp"

#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdint.h>
#include <signal.h>

namespace timesync
{
        enum
        {
                CONF_QUERY_MS = 10
        };

	/* The SFD stamping mechanism relies on the fact that you */
        class InputCapture : public LogicalStamp
        {
        	// Constructor and destructor
               	public: InputCapture(			
               		boost::asio::io_service *io,	// IO service
			TimerName intimer
		);
        	public: ~InputCapture();

        	// Implementation-specific callback to get timer info
        	public: uint64_t rxlatency();	
        	public: uint64_t txlatency();	
            public: void trigger(uint64_t start);

        	// Get the timer name               				
		public: TimerName timerid();
	
		// The request and response must be implemented by child clases
        	protected: void request();

        	// Receive 	
        	private: boost::asio::deadline_timer query;             // Qeery timer
        	private: int fd;				// 0: init, 1:triggered, 2: valid 
        	private: roseline_capt cap;				// Cached capture info
        	private: TimerName timer;				// Timer of interest
        };
}

#endif
