#include "Software.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>

using namespace timesync;

// Request a time mark
void Software::request()
{
	// Get the count at request
	uint32_t c = (uint32_t) reg[0x3c/4];

	// Get the overflow at request, and merge with the count
	roseline_capt tmp;
	tmp.id = timer;
	if (ioctl(fd, ROSELINE_GET_CAPTURE, &tmp) != -1)
	{
		// Get the time mark
		uint64_t t = (((uint64_t)tmp.overflow) << 32) | (uint64_t) c;
		
		// We now have valid info
		response(true, t);
	}

	// Callback with the status and value
	response(false, 0);
}

Software::Software(	boost::asio::io_service *io,	// IO service
			TimerName intimer) 		// Timer
	: LogicalStamp(io)
{
	// MMAP based on the register
	int md = open("/dev/mem", O_RDWR | O_SYNC);
	switch(timer)
	{
		case IOTIMER_A : reg = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, md, TIMER4_REG); break;
		case IOTIMER_B : reg = (uint32_t *)mmap(NULL, 0x1000, PROT_READ|PROT_WRITE, MAP_SHARED, md, TIMER6_REG); break;
		default: std::cerr << "Bad timer for MMAP" << std::endl;
	}	
}

Software::~Software()
{
	close(md);	// Cleanup: MMAP
	close(fd);	// Cleanum: ioctl
}

uint64_t Software::rxlatency()
{
	return 24;	// 1 clock cycle @ 24MHz
}

uint64_t Software::txlatency()
{
	return 24;	// 1 clock cycle @ 24MHz
}

void Software::trigger(uint64_t start)
{
	// Do nothing
}

// Get the timer name               				
TimerName Software::timerid()
{
	return timer;
}