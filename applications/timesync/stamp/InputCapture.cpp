#include "InputCapture.hpp"

#include <boost/function.hpp>
#include <boost/bind.hpp>

using namespace timesync;

// Keep polling until we have somethign
void InputCapture::request()
{
	// Keep probing
 	query.expires_at(query.expires_at() + boost::posix_time::milliseconds(CONF_QUERY_MS));
	query.async_wait(boost::bind(&InputCapture::request, this));

	// Perform the query
	roseline_capt tmp;
	tmp.id = timer;
	if (ioctl(fd, ROSELINE_GET_CAPTURE, &tmp) != -1)
	{
		// If this is new data
		if (tmp.capture > cap.capture)
		{
			// Cacel the query timer
			query.cancel();

			// Cache the timer information
			memcpy(&cap,&tmp,sizeof(cap));

			// Get the time mark
			uint64_t t = (((uint64_t) cap.overflow) << 32) |  (uint64_t) cap.count_at_capture;

			// Return the time mark
			response(true, t);
		}
	}
}

// Constructor
InputCapture::InputCapture(	boost::asio::io_service *io,	// IO service
				TimerName intimer)		// Timer
	: LogicalStamp(io), query(*io, boost::posix_time::milliseconds(CONF_QUERY_MS)), timer(intimer)
{
	// Open an IOCTL handle to request capture events for this PID
	const char *file_name = "/dev/roseline";
	fd = open(file_name, O_RDWR);
	if (fd == -1)
		std::cerr << "error:  InputCapture open()";
}

InputCapture::~InputCapture()
{
	// Close ioctl
	close(fd);
}

uint64_t InputCapture::rxlatency()
{
	return 216;	// 9us @ 24MHz
}

uint64_t InputCapture::txlatency()
{
	return 0;	// 0us @ 24Mhz
}

void InputCapture::trigger(uint64_t start)
{
    // Start 1s timer
    roseline_comp data;
    data.id          = IOTIMER_A;       // Timer to use
    data.enable      = 1;               // Enable this timer
    data.next        = start;           // Time of first event
    data.cycles_high = 24000;           // High duration
    data.cycles_low  = 24000;           // Low duration
    data.limit       = 1;               // Iterations
    if (ioctl(fd, ROSELINE_SET_COMPARE, &data) == -1)
        perror("Problem sending compare instruction");
}

// Get the timer name               				
TimerName InputCapture::timerid()
{
	return timer;
}