// Basic printing
#include <iostream>
#include <fstream>
#include <string>

// Boost includes
#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>

// Clock includes
#include "clock/LogicalClock.hpp"
#include "radio/LogicalRadio.hpp"
#include "truth/LogicalTruth.hpp"
#include "stamp/LogicalStamp.hpp"
#include "sync/LogicalSync.hpp"

// Convenience
using namespace timesync;

// A requirement for program options
namespace timesync
{
	// Allows conversion directly from a string to a logical sync type
	std::istream& operator>>(std::istream& in, LogicalRadioType& unit)
	{
	    std::string token;
	    in >> token;
	         if (token == "at86rf233")   	unit = RADIO_AT86RF233;
	    else throw boost::program_options::validation_error(
	      boost::program_options::validation_error::invalid_option_value, "Invalid option");
	    return in;
	}
	std::ostream& operator<<(std::ostream& os, LogicalRadioType const& unit)
	{
		switch (unit)
		{
			case RADIO_AT86RF233: 	os << "at86rf233"; 	break;
			default: os.setstate(std::ios_base::failbit);	break;
		}
		return os;
	}

	// Allows conversion directly from a string to a logical sync type
	std::istream& operator>>(std::istream& in, LogicalClockType& unit)
	{
	    std::string token;
	    in >> token;
	         if (token == "regression")   	unit = CLOCK_REGRESSION;
	    else if (token == "polynomial")   	unit = CLOCK_POLYNOMIAL;
	    else throw boost::program_options::validation_error(
	      boost::program_options::validation_error::invalid_option_value, "Invalid option");
	    return in;
	}
	std::ostream& operator<<(std::ostream& os, LogicalClockType const& unit)
	{
		switch (unit)
		{
			case CLOCK_REGRESSION: 	os << "regression"; 	break;
			case CLOCK_POLYNOMIAL: os << "polynomial"; break;
			default: os.setstate(std::ios_base::failbit);	break;
		}
		return os;
	}

	// Allows conversion directly from a string to a clock type
	std::istream& operator>>(std::istream& in, LogicalSyncType& unit)
	{
	    std::string token;
	    in >> token;
	         if (token == "ftsp")   	unit = SYNC_FTSP;
	    else throw boost::program_options::validation_error(
	      boost::program_options::validation_error::invalid_option_value, "Invalid option");
	    return in;
	}
	std::ostream& operator<<(std::ostream& os, LogicalSyncType const& unit)
	{
		switch (unit)
		{
			case SYNC_FTSP: 	os << "ftsp"; 	break;
			default: os.setstate(std::ios_base::failbit);	break;
		}
		return os;
	}

	// Allows conversion directly from a string to a clock type
	std::istream& operator>>(std::istream& in, LogicalTruthType& unit)
	{
	    std::string token;
	    in >> token;
	         if (token == "gnss")   	unit = TRUTH_GNSS;
	    else throw boost::program_options::validation_error(
	      boost::program_options::validation_error::invalid_option_value, "Invalid option");
	    return in;
	}
	std::ostream& operator<<(std::ostream& os, LogicalTruthType const& unit)
	{
		switch (unit)
		{
			case TRUTH_GNSS: 	os << "gnss"; 	break;
			default: os.setstate(std::ios_base::failbit);	break;
		}
		return os;
	}

	// Allows conversion directly from a string to a clock type
	std::istream& operator>>(std::istream& in, LogicalStampType& unit)
	{
	    std::string token;
	    in >> token;
	         if (token == "inputcapture")   unit = STAMP_INPUTCAPTURE;
            else if (token == "software")   	unit = STAMP_SOFTWARE;
	    else throw boost::program_options::validation_error(
	      boost::program_options::validation_error::invalid_option_value, "Invalid option");
	    return in;
	}
	std::ostream& operator<<(std::ostream& os, LogicalStampType const& unit)
	{
		switch (unit)
		{
			case STAMP_INPUTCAPTURE: 	os << "inputcapture"; 	break;
			case STAMP_SOFTWARE: 		os << "software"; 	break;
			default: os.setstate(std::ios_base::failbit);	break;
		}
		return os;
	}
}

// Main entry point of application
int main(int argc, char **argv)
{
	// Parse command line options
	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
		("help,h",  	"produce help message")
		("pan,p",  		boost::program_options::value<uint16_t>()->default_value(0xFEED), "Radio PAN")
		("id,i",  		boost::program_options::value<uint16_t>()->default_value(0x0001), "Radio ID")
		("event,e",   	boost::program_options::value<float>()->default_value(5.0), "Initial time between sync events")
		("ground,g", 	boost::program_options::value<LogicalTruthType>()->default_value(TRUTH_GNSS), "ground truth type = gnss")
		("radio,r", 	boost::program_options::value<LogicalRadioType>()->default_value(RADIO_AT86RF233), "radio type = at86rf233")
		("clock,c", 	boost::program_options::value<LogicalClockType>()->default_value(CLOCK_POLYNOMIAL), "clock type = regression|polynomial")
		("stamp,s", 	boost::program_options::value<LogicalStampType>()->default_value(STAMP_INPUTCAPTURE), "stamp type = software|inputcapture")
		("alg,a", 		boost::program_options::value<LogicalSyncType>()->default_value(SYNC_FTSP), "sync algorithm = ftsp")
		("timer,t", 	boost::program_options::value<std::string>()->default_value("IOTIMER_A"), "roseline IO timer = IOTIMER_A|IOTIMER_B") 
		("master,m",  	"set this node to the master (root) timing node")
		("strobe,o", 	boost::program_options::value<uint16_t>()->default_value(100), "input capture stamping strobe") 
	;
	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);    

	// Print some help with arguments
	if (vm.count("help") > 0)
	{
		std::cout << desc << "\n";
		return 0;
	}

	// Check the timer name
    TimerName unit;
         if (vm["timer"].as<std::string>() == "IOTIMER_A") unit = IOTIMER_A;
    else if (vm["timer"].as<std::string>() == "IOTIMER_B") unit = IOTIMER_B;
    else 
    {
		std::cerr << "Specified IO timer must be IOTIMER_A or IOTIMER_B" << "\n";
		return 1;
	}	

	// CORE FUNCTIONALITY /////////////////////////////////////////////////////////////

	// Create an IO service
	boost::asio::io_service io;

	// STEP 1 : CREATE A CLOCK
	boost::shared_ptr<LogicalClock> clk = LogicalClock::Factory(
		vm["clock"].as<LogicalClockType>()	// Clock type
	);

	// STEP 2 : CREATE A RADIO INTERFACE
	boost::shared_ptr<LogicalRadio> radio = LogicalRadio::Factory(
		vm["radio"].as<LogicalRadioType>(), 	// Radio type
		&io,					// IO service (async signals, timers)
		vm["pan"].as<uint16_t>(), 		// PAN
		vm["id"].as<uint16_t>()			// Unique ID
	);

	// STEP 3 : CREATE A STAMP INTERFACE on TIMER x
	boost::shared_ptr<LogicalStamp> stamp = LogicalStamp::Factory(
		vm["stamp"].as<LogicalStampType>(), 	// Stamp type
		&io,					// IO service (async signals, timers)
		unit					// Timer
	);

	// STEP 4 : CREATE A SYNC ALGORITHM
	boost::shared_ptr<LogicalSync> sync = LogicalSync::Factory(
		vm["alg"].as<LogicalSyncType>(),	// Syncrhonization algorithm
		&io,					// IO service (async signals, timers)
		clk, 					// Clock
		radio, 					// Radio
		stamp,					// Stamp mechanism
		(vm.count("master") > 0)
	);

	// STEP 5 : START THE SYNCHRONIZATION ALGORITHM WITH A GIVEN EVENT SEPARATION
	sync->start(vm["event"].as<float>());

	// Wait until all services have no more work
	io.run();

	// Everything OK
	return 0;
}
