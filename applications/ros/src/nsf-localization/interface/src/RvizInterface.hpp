#ifndef RVIZINTERFACE_H
#define RVIZINTERFACE_H

// General Qt stuff
#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QGridLayout>
#include <QVBoxLayout>

// ROS + messaging
#include <ros/ros.h>

namespace rviz
{
	class Display;
	class RenderPanel;
	class VisualizationManager;
}

class RvizInterface: public QWidget
{
Q_OBJECT
	public:	RvizInterface(const std::string &cfg, ros::NodeHandle* nh, QWidget* parent = 0);
	public: virtual ~RvizInterface();

private Q_SLOTS:
	void ranging(int error);

	private: rviz::VisualizationManager* manager;
	private: rviz::RenderPanel* render_panel;
	private: QLabel* ranging_value;
	private: ros::Publisher pub_cfg;
};


#endif 