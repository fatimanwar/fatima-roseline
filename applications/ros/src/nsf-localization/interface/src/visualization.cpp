// QT4 Library
#include <QApplication>

// ROS + messaging
#include <ros/ros.h>
#include <tdoa/Anchor.h>
#include <tdoa/Sensor.h>
#include <visualization_msgs/MarkerArray.h>

// Visualiszation
#include "RvizInterface.hpp"


// Map of (string) -> (markers) of sensors 
std::map<std::string,visualization_msgs::Marker> sen_mptr;	// Sensor markers
std::map<std::string,visualization_msgs::Marker> sen_lptr;	// Sensor labels
std::map<std::string,visualization_msgs::Marker> anc_mptr;	// Anchor markers
std::map<std::string,visualization_msgs::Marker> anc_lptr;	// Anchor labels

// Publisher of the marker array
ros::Publisher sen_pub;
ros::Publisher anc_pub;

// Keep an integer list
int cnt = 0;

// Callback when new pose arrives
void cb_anchor(const tdoa::Anchor::ConstPtr& msg)
{
	//ROS_INFO("ANCHOR: %s", msg->name.c_str());

	if (anc_mptr.find(msg->name) == anc_mptr.end())
	{
		visualization_msgs::Marker marker;
		marker.header.frame_id = "/my_frame";
		marker.header.stamp = ros::Time::now();
		marker.ns = "basic_shapes";
		marker.id = cnt++;
		marker.type = visualization_msgs::Marker::CUBE;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose = msg->pose; 
		marker.scale.x = 0.05;
		marker.scale.y = 0.05;
		marker.scale.z = 0.05;
		marker.color.r = 1.0f;
		marker.color.g = 1.0f;
		marker.color.b = 1.0f;
		marker.color.a = 1.0;
		marker.lifetime = ros::Duration();
		anc_mptr[msg->name] = marker;
	}
	else
	{
		anc_mptr[msg->name].pose = msg->pose; 
	}

 	// TEXT LABEL

	if (anc_lptr.find(msg->name) == anc_lptr.end())
	{
		std::ostringstream oss;
		oss << msg->name << " (" << msg->serr << ")";
		visualization_msgs::Marker mlabel;
		mlabel.header.frame_id = "/my_frame";
		mlabel.header.stamp = ros::Time::now();
		mlabel.ns = "basic_shapes";
		mlabel.id = cnt++;
		mlabel.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
		mlabel.action = visualization_msgs::Marker::ADD;
		mlabel.pose = msg->pose;
		mlabel.pose.position.z += 0.1;
		mlabel.scale.x = 0.025;
		mlabel.scale.y = 0.025;
		mlabel.scale.z = 0.025;
		mlabel.color.r = 1.0f;
		mlabel.color.g = 1.0f;
		mlabel.color.b = 1.0f;
		mlabel.color.a = 1.0;
		mlabel.lifetime = ros::Duration();
		mlabel.text = oss.str();
		anc_lptr[msg->name] = mlabel;
	}
	else
	{
		std::ostringstream oss;
		oss << msg->name << " (" << msg->serr << ")";
		anc_lptr[msg->name].pose = msg->pose; 
		anc_lptr[msg->name].pose.position.z += 0.1;
		anc_lptr[msg->name].text = oss.str();
	}

	// UPDATE MARKER ARRAY
	visualization_msgs::MarkerArray ma;
	for (std::map<std::string,visualization_msgs::Marker>::iterator it = anc_lptr.begin(); it != anc_lptr.end(); ++it)
		ma.markers.push_back(it->second);
	for (std::map<std::string,visualization_msgs::Marker>::iterator it = anc_mptr.begin(); it != anc_mptr.end(); ++it)
		ma.markers.push_back(it->second);
	anc_pub.publish(ma);
}

// Callback when new pose arrives
void cb_sensor(const tdoa::Sensor::ConstPtr& msg)
{
	//ROS_INFO("SENSOR: %s", msg->name.c_str());

	// BLOCK

	if (sen_mptr.find(msg->name) == sen_mptr.end())
	{
		visualization_msgs::Marker marker;
		marker.header.frame_id = "/my_frame";
		marker.header.stamp = ros::Time::now();
		marker.ns = "basic_shapes";
		marker.id = cnt++;
		marker.type = visualization_msgs::Marker::SPHERE;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose = msg->pc.pose; 
		marker.scale.x = 0.05;
		marker.scale.y = 0.05;
		marker.scale.z = 0.05;
		marker.color.r = 1.0f;
		marker.color.g = 1.0f;
		marker.color.b = 1.0f;
		marker.color.a = 1.0;
		marker.lifetime = ros::Duration();
		sen_mptr[msg->name] = marker;
	}
	else
	{
		sen_mptr[msg->name].pose = msg->pc.pose; 
	}


 	// TEXT LABEL

	if (sen_lptr.find(msg->name) == sen_lptr.end())
	{
		visualization_msgs::Marker mlabel;
		mlabel.header.frame_id = "/my_frame";
		mlabel.header.stamp = ros::Time::now();
		mlabel.ns = "basic_shapes";
		mlabel.id = cnt++;
		mlabel.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
		mlabel.action = visualization_msgs::Marker::ADD;
		mlabel.pose = msg->pc.pose;
		mlabel.pose.position.z += 0.1;
		mlabel.scale.x = 0.05;
		mlabel.scale.y = 0.05;
		mlabel.scale.z = 0.05;
		mlabel.color.r = 1.0f;
		mlabel.color.g = 1.0f;
		mlabel.color.b = 1.0f;
		mlabel.color.a = 1.0;
		mlabel.lifetime = ros::Duration();
		mlabel.text = msg->name;
		sen_lptr[msg->name] = mlabel;
	}
	else
	{
		sen_lptr[msg->name].pose = msg->pc.pose; 
		sen_lptr[msg->name].pose.position.z += 0.1;
	}

	// UPDATE MARKER ARRAY

	visualization_msgs::MarkerArray ma;
	for (std::map<std::string,visualization_msgs::Marker>::iterator it = sen_lptr.begin(); it != sen_lptr.end(); ++it)
		ma.markers.push_back(it->second);
	for (std::map<std::string,visualization_msgs::Marker>::iterator it = sen_mptr.begin(); it != sen_mptr.end(); ++it)
		ma.markers.push_back(it->second);
	sen_pub.publish(ma);
}

int main(int argc, char **argv)
{
	// Initialize ROS, if necessary
	if(!ros::isInitialized())
		ros::init(argc, argv, "visualization", ros::init_options::AnonymousName);
	ros::NodeHandle n("~");

	// Get the configuration parameter
	std::string cfg;
	n.getParam("config", cfg);

	// Register for messages
	sen_pub = n.advertise<visualization_msgs::MarkerArray>("/anchor_markers", 1000);
	anc_pub = n.advertise<visualization_msgs::MarkerArray>("/sensor_markers", 1000);
	ros::Subscriber anc_sub = n.subscribe<tdoa::Anchor>("/anchor", 1000, cb_anchor);
	ros::Subscriber sen_sub = n.subscribe<tdoa::Sensor>("/sensor", 1000, cb_sensor);

	// Create the QT4 app
	QApplication app(argc, argv);
	RvizInterface* rvi = new RvizInterface(cfg, &n);
	rvi->show();
	rvi->setWindowState(rvi->windowState()|Qt::WindowMaximized);
	app.exec();

	// Clean up
	delete rvi;
}