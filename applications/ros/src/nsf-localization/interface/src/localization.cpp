// Non-linear least-squares solver + logging
#include <ceres/ceres.h>
#include <glog/logging.h>
#include <iostream>

// ros stuff
#include <ros/ros.h>
#include <tdoa/Anchor.h>
#include <tdoa/Sensor.h>
#include <visualization_msgs/MarkerArray.h>

// Namespac declarations
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

// Publisher of the marker array
ros::Publisher  pub_sen;
ros::Subscriber sub_anc;

// Localization parameters
int id = -1;

// A vector of measurements
std::vector<tdoa::Anchor> measurements;

// A templated cost functor that implements the residual r = 10 -
// x. The method operator() is templated so that we can then use an
// automatic differentiation wrapper around it to generate its
// derivatives.
class CostFunctor
{
	public: CostFunctor(double _x, double _y, double _z, double _obs)
		: x(_x), y(_y), z(_z), obs(_obs) {}
  	
  	template <typename T> bool operator()(const T* const p, T* e) const
	{
		// Get the range to the sensor
		T range	= sqrt(
			  (p[0]-T(x)) * (p[0]-T(x))
			+ (p[1]-T(y)) * (p[1]-T(y))
			+ (p[2]-T(z)) * (p[2]-T(z))
		);


		// Error = predicted - observed
    		e[0] = (p[3] + range / 340.29) - T(obs);

    		// Success
    		return true;
  	}

	private: double x, y, z, obs;
};

// Obtain a localization solution from the received measurements
void solution()
{
	// Build a cerse-solver problem
	Problem problem;

	// Initial guess of sensor position [X,Y,Z,T]
	double x[4] = {0.0, 0.0, 0.0, -0.1};

	double mv = -1.0;
	for (std::vector<tdoa::Anchor>::iterator it = measurements.begin();
	     it != measurements.end(); it++)
	     if (mv < 0 || mv > it->event)
	     	mv = it->event;

	//for (std::vector<tdoa::Anchor>::iterator it = measurements.begin();
	//    it != measurements.end(); it++)
	//   std::cout << it->name << " : " << (it->event - mv) << std::endl;

	// Iterate over all measurements
	for (std::vector<tdoa::Anchor>::iterator it = measurements.begin();
	     it != measurements.end(); it++)
	{
		// Add a residual block representing a constraint on the measured arrival time of the
		// ultrasonic signal. We will solve for the sensor position and time of transmission.
		problem.AddResidualBlock(new AutoDiffCostFunction<CostFunctor,1,4>(new CostFunctor(
                 	it->pose.position.x,it->pose.position.y,it->pose.position.z,(it->event - mv) / 24e6)), NULL, (double*)&x);
	}

	// Run the solver!
	Solver::Options options;
	options.minimizer_progress_to_stdout = true;
	options.max_num_iterations = 200;
	options.gradient_tolerance = 1e-6;
	options.function_tolerance = 1e-6;
	options.parameter_tolerance = 1e-6;
	options.num_threads = 4;
	options.num_linear_solver_threads = 4;
	options.max_solver_time_in_seconds = 5.0;
	Solver::Summary summary;
	Solve(options, &problem, &summary);

	// Create the sensor position
	tdoa::Sensor msg;
	msg.name       	    	= "Transmitter";
	msg.event     	    	= x[3];
	msg.pc.pose.position.x  = x[0];
	msg.pc.pose.position.y  = x[1];
	msg.pc.pose.position.z  = x[2];

	// Publish the position
	pub_sen.publish(msg);

}

// Callback when new pose arrives
void cb_anchor(const tdoa::Anchor::ConstPtr& msg)
{
	// If we receive a message greater than marker + threshold, then this refers to the
	// next chirp round. We therefore need to localize and reset the system.
	if (msg->count != id && id >= 0)
	{
		if (measurements.size() > 4) 
		{
			ROS_INFO("%d measurements received. Performing localization.",(int)measurements.size());
			solution();
		}
		else
		{
			ROS_INFO("%d measurements received. Not sufficient to localize.",(int)measurements.size());
		}

		// Clear the measurement buffer
		measurements.clear();
	}

	measurements.push_back(*msg);
	id = msg->count;
}

// Main entry point of ros node
int main(int argc, char **argv)
{
	// An unfortunate requirement of ceres-solver
	google::InitGoogleLogging(argv[0]);

	// Initialise ROS node
	ros::init(argc, argv, "localization");
	ros::NodeHandle n("~");
	sub_anc = n.subscribe<tdoa::Anchor>("/anchor", 1000, cb_anchor);
	pub_sen = n.advertise<tdoa::Sensor>("/sensor", 1000);

	// Keep going until ctrl+c
	ros::spin();

	// Succcess
	return 0;
}



/*

int main(int argc, char** argv)
{
	// An unfortunate requirement of ceres-solver
	google::InitGoogleLogging(argv[0]);

	// Initial guess of sensor position [TRUTH: 8.5159, 3.7566, 0.3528]
	double x[4] = {1.0, 1.0, 1.0, 1.0};

	// Observations [X,Y,Z,DIST,TDOA]
	double m[15][5] = {
		{8.2621,    1.7862,         0.0,    2.0178,    0.0009},
		{0.3369,    4.9442,         0.0,    8.2723,    0.0191},
		{0.1794,    2.3245,         0.0,    8.4660,    0.0196},
		{7.1489,    4.7341,         0.0,    1.7172,         0},
		{9.2436,    0.5012,         0.0,    3.3544,    0.0048},
		{5.5488,    1.5978,         0.0,    3.6863,    0.0057},
		{8.2884,    1.9716,         0.0,    1.8337,    0.0003},
		{3.5681,    4.1928,         0.0,    4.9796,    0.0095},
		{3.0678,    0.8013,         0.0,    6.2081,    0.0131},
		{6.7383,    1.1659,         0.0,    3.1617,    0.0042},
		{0.5617,    0.8192,         0.0,    8.4867,    0.0197},
		{3.2681,    2.8429,         0.0,    5.3385,    0.0105},
		{8.8328,    0.7643,         0.0,    3.0297,    0.0038},
		{4.1109,    3.3531,         0.0,    4.4376,    0.0079},
		{5.0217,    4.4832,         0.0,    3.5864,    0.0054}
	};

	// Build the problem.
	Problem problem;

	// Set up the only cost function (also known as residual). This uses
	// auto-differentiation to obtain the derivative (jacobian).
	for (int i = 0; i < 15; i++)
		problem.AddResidualBlock(new AutoDiffCostFunction<CostFunctor,1,4>(
			new CostFunctor(m[i][0],m[i][1],m[i][2],m[i][4])), NULL, (double*)&x);

	// Run the solver!
	Solver::Options options;
	options.minimizer_progress_to_stdout = true;
	Solver::Summary summary;
	Solve(options, &problem, &summary);
	std::cout << summary.BriefReport() << "\n";
	std::cout << "x : " << x[0] << std::endl;
	std::cout << "y : " << x[1] << std::endl;
	std::cout << "z : " << x[2] << std::endl;

	// Success
	return 0;
}
*/