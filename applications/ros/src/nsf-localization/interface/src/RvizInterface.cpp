// Needed for rtviz interaction
#include "rviz/visualization_manager.h"
#include "rviz/yaml_config_reader.h"
#include "rviz/render_panel.h"
#include "rviz/display.h"

// This code header
#include "RvizInterface.hpp"

// TDOA configuration message
#include <ros/ros.h>
#include <tdoa/Configuration.h>

// BEGIN_TUTORIAL
// Constructor for RvizInterface.  This does most of the work of the class.
RvizInterface::RvizInterface(const std::string &cfg, ros::NodeHandle *nh, QWidget* parent)
  : QWidget(parent)
{
	// Set the configuration publisher
	pub_cfg = nh->advertise<tdoa::Configuration>("/configuration", 1000);

	// Construct and lay out labels and slider controls.
	QLabel* ranging_label = new QLabel("Sync update");
	ranging_value = new QLabel("10 ms");
	QSlider* ranging_slider = new QSlider( Qt::Horizontal );
	ranging_slider->setMinimum(1000);
	ranging_slider->setMaximum(30000);
	QGridLayout* controls_layout = new QGridLayout();
	controls_layout->addWidget(ranging_label,  1, 0);
	controls_layout->addWidget(ranging_slider, 1, 1);
	controls_layout->addWidget(ranging_value,  1, 2);

	// Construct and lay out render panel.
	render_panel = new rviz::RenderPanel();
	QVBoxLayout* main_layout = new QVBoxLayout();
	main_layout->addLayout(controls_layout);
	main_layout->addWidget(render_panel);

	// Set the top-level layout for this RvizInterface widget.
	setLayout(main_layout);

	// Make signal/slot connections.
	connect(ranging_slider, SIGNAL(valueChanged(int)), this, SLOT(ranging(int)));

	// Initialize the main RViz classes.
	manager = new rviz::VisualizationManager(render_panel);
	render_panel->initialize(manager->getSceneManager(), manager);
	manager->initialize();
	manager->startUpdate();

	// Read the config file
	rviz::YamlConfigReader reader;
	rviz::Config config;
	reader.readFile(config, QString::fromStdString(cfg));
	if(!reader.error())
		manager->load(config.mapGetChild("Visualization Manager"));

	// Initialize the slider values.
	ranging_slider->setValue(1000);
}

// Destructor.
RvizInterface::~RvizInterface()
{
  	delete manager;
}

void RvizInterface::ranging(int error)
{
	ROS_INFO("Received new sync parameter: %d", error);

	// Use a string stream to print text label
    	std::ostringstream ss("");
    	ss << error << " ms";

  	// Do something
  	ranging_value->setText(QString::fromStdString(ss.str()));

  	// Package and send the message
  	tdoa::Configuration msg;
  	msg.error = (double) error;
  	pub_cfg.publish(msg);
}