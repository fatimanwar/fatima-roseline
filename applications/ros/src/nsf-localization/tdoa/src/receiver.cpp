// Standard includes
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdint.h>
#include <signal.h>
#include <stdlib.h>

// ROS includes
#include <ros/ros.h>
#include <tdoa/Trigger.h>
#include <tdoa/Anchor.h>
#include <tdoa/Configuration.h>

// C++ includes
#include <iostream>

// For thread fucntionality
#include <boost/thread.hpp>

// Include the BeagleBone Black IO library
#include "bbbio/BBBiolib.h"

extern "C"
{
	#include "audio/audio_proc.h"
	#include "cmu/demodulate_roseline.h"
}

// Include the ROSELINE ioctl library
#include "../../../../timesync/module/roseline_ioctl.h"

// Fixed size for CMU code
#define AUDIO_BUFFER_SIZE 	48000
#define FILTER_CMU  		0
#define FILTER_UCLA 		1

// Flags to signal threads
bool flag    = true;
bool debug   = false;
bool newdata = false;
bool trigdata = false;

// For ROS messaging
ros::Publisher  pub_anc;	// Ougoing anchor messages
ros::Publisher  pub_sig;	// For debuggin the audio
ros::Subscriber sub_cfg;	// Incoming configuration messsages
ros::Subscriber sub_tri;	// Incoming configuration messsages
tdoa::Anchor    msg_anc;	// Anchor message
roseline_sync   syn;		// Synchronization values
roseline_pars   par;		// Synchronization parameters

// Double-buffer the audio signals
unsigned int buffer[3][AUDIO_BUFFER_SIZE];
uint32_t tbuffer = 0;
uint32_t obuffer = 0;
uint32_t msgid   = 0;

// ioctl file descriptor
int fd;

// Callback for new configuration parameters
void cb_tri(const tdoa::Trigger::ConstPtr& msg)
{
	ROS_INFO("Received new trigger");
	if (newdata)
		ROS_WARN("Trigger received, but still processing");
	else
	{
		msgid    = msg->count;
		trigdata = true;
	}
}

// Callback for new configuration parameters
void cb_cfg(const tdoa::Configuration::ConstPtr& msg)
{
	ROS_INFO("Received new time synchronization configuration request for %f", msg->error);

	// Sanity check
	if (!fd)
		ROS_WARN("ioctl() not yet initialized");

	// Pass the desired time sync accuracy down to the time sync algorithm
	par.id    = syn.id;
	par.error = msg->error;
	
	// Perform an IOCTL to find the capture
	if (ioctl(fd, ROSELINE_SET_PARS, &par) == -1)
		ROS_WARN("Problem updating synchronization parameters");
}

// ADC: thread work
void worker_adc(ros::NodeHandle *n)
{
	// These can be modfied
	ROS_INFO("Starting ADC worker thread");

	// Initialise the BBBIO library
	iolib_init();

	// Additional parameters
	int clk_div;
	int open_dly;
	int sample_dly;
	int timer;
	n->getParam("clk_div", clk_div);
	n->getParam("open_dly", open_dly);
	n->getParam("sample_dly", sample_dly);
	n->getParam("timer", timer);
	switch(timer)
	{
		case 4 :  timer = BBBIO_DMTIMER_TIMER4; break;
		case 5 :  timer = BBBIO_DMTIMER_TIMER5; break;
		case 6 :  timer = BBBIO_DMTIMER_TIMER6; break;
		case 7 :  timer = BBBIO_DMTIMER_TIMER7; break;
	}

 	// Set the ADC to sample at 48kHz with 30Hz tolerance
	BBBIO_ADCTSC_module_ctrl(clk_div);
	BBBIO_ADCTSC_channel_ctrl(BBBIO_ADC_AIN2, BBBIO_ADC_STEP_MODE_SW_CONTINUOUS, 
		open_dly, sample_dly, BBBIO_ADC_STEP_AVG_1, buffer[0], AUDIO_BUFFER_SIZE);

	// The heavy lifting
	while (flag)
	{
		if (trigdata)
		{
			// Introduce a check for rac eocnditions
			if (newdata)
			{
				ROS_WARN("Trying to process an ADC sample without the previous completing");
			}
			else
			{
				// Enable AIN2 and capture into the buffer
				BBBIO_ADCTSC_channel_enable(BBBIO_ADC_AIN2);
				
				// Perform a fast time read and deal with overflows
				tbuffer = (uint32_t) BBBIO_DMTIMER_Work(timer);
				obuffer = (uint32_t) syn.overflow;

				// Capture the audio
				BBBIO_ADCTSC_work(AUDIO_BUFFER_SIZE);

				memcpy(buffer[1],buffer[0],sizeof(unsigned int)*AUDIO_BUFFER_SIZE);
				newdata  = true;
			}

			trigdata = false;
		}
		else
			boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	}

	// Clear up BBIOlib
	iolib_free();
}

// This just finds the first value greater than some threshold amount
void worker_fil(ros::NodeHandle *n)
{
	// These can be modfied
	ROS_INFO("Starting filter worker thread");

	// Capture data gets dumped into this structure
	roseline_sync tmp;

	// Should be optimized out of the loop, but just to be safe...
	float tof, rss;
	float nasty[AUDIO_BUFFER_SIZE],  supernasty[AUDIO_BUFFER_SIZE];
	unsigned int max;

	double divisor;
	double mult;
	double threshold;
	bool cmu;

	// Some code to monitor the 
	while (flag)
	{
		// Additional parameters
		n->getParam("divisor", divisor);
		n->getParam("multiplier", mult);
		n->getParam("threshold", threshold);
		n->getParam("cmu", cmu);

		// Will be activated by adc thread
		if (newdata)
		{
			// Convert signal to -1 to +1
			max = 0;
			for (int i = 0; i < AUDIO_BUFFER_SIZE; i++)
			{
				if (max < 0 || max < buffer[1][i]) 
					max = buffer[1][i];
				nasty[i] = (double) buffer[1][i];
			}

			// Bandpass filter the buffer for infrasonic measurements only
			tof = -1.0;
			rss = -1.0;
			if (cmu)
				demodulate_roseline(nasty, (float) divisor, (float) mult, &tof, &rss);
			else
			{
				zp_bandpass_filter(nasty, supernasty, AUDIO_BUFFER_SIZE);
				for (int i = 0; i < AUDIO_BUFFER_SIZE; i++)
				{
					if (max < 0 || max < buffer[1][i]) 
						max = buffer[1][i];
				}
				if (max >  threshold)
					tof = max;
				else
					tof = -1.0;
			}
			ROS_INFO("Maximum value for this sample %u",max);
	
			// Get index relative to start point
			double idx = ((double) tof / (double) AUDIO_BUFFER_SIZE) * (double) syn.frequency;

			// Calculate the time of the first sample in the buffer
			double t = (double)(((uint64_t)obuffer) << 32 | (uint64_t)(tbuffer)) + idx;
		
			// If a positive IDX was found there is a chirp in here
			if (tof > 0)
			{
				// Debug information
				ROS_INFO("Event triggered at time %f",t);

				// Transmite event detection
				msg_anc.event = syn.est_skew * (((double) t) - syn.est_local) + syn.est_offset;
				msg_anc.count = msgid;
				msg_anc.tdoa  = tof;
				msg_anc.rssi  = rss;
				msg_anc.serr  = syn.var_local;
				pub_anc.publish(msg_anc);	
			}

			double g = syn.est_skew * (((double) t) - syn.est_local) + syn.est_offset;

			// Create the name
			std::ostringstream oss;
			oss.precision(15);
			oss << std::fixed;
			oss << "new" << g << "-" << tof << ".csv";

		 	// Open a binary file for writing
		   	FILE *fh = fopen(oss.str().c_str(), "w");
		    	if (fh != NULL)
			{		    	
				// Write the data
				for (int i = 0; i < AUDIO_BUFFER_SIZE; i++)
			        	fprintf(fh,"%u\n",buffer[1][i]);

				// Close file
				fclose (fh);
			}

			// Finished processing
			newdata = false;
		}

		// Sleep for a little bit between checks
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	}
}

// INPUT CAPTURE: thread work
void worker_cap(ros::NodeHandle *n)
{
	ROS_INFO("Starting INPUT CAPTURE worker thread");

	// Capture data gets dumped into this structure
	roseline_capt tmp;
	roseline_capt cap;
	int capture;

	// Some code to monitor the 
	while (flag)
	{
		// Get the timer configuration
		n->getParam("capture", capture);
		switch(capture)
		{
			case 4 :  tmp.id = TIMER4; break;
			case 5 :  tmp.id = TIMER5; break;
			case 6 :  tmp.id = TIMER6; break;
			case 7 :  tmp.id = TIMER7; break;
		}
		
		// Perform an IOCTL to find the capture
		if (ioctl(fd, ROSELINE_GET_CAPTURE, &tmp) != -1)
		{
			// If this is new data
			if (tmp.capture > cap.capture)
			{
				// Cache the timer information
				memcpy(&cap,&tmp,sizeof(roseline_capt));

				// Calculate the time of the event
				double t = ((uint64_t)tmp.overflow) << 32 | (uint64_t) tmp.count_at_capture;

				// Transmite event detection
				msg_anc.event = syn.est_skew * (((double) t) - syn.est_local) + syn.est_offset;
				msg_anc.count = msg_anc.count + 1;
				pub_anc.publish(msg_anc);
			}
		}

		// Sleep for a little bit between checks
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	}

	// Clean up
	close(fd);
}

// This is a thread that periodically checks the kernel module for new time synchronization parameters
// when new values are available it updates 
void worker_syn(ros::NodeHandle *n)
{
	ROS_INFO("Starting TIMESYNC worker thread");

	// Capture data gets dumped into this structure
	roseline_sync tmp;
	int timer;

	// Some code to monitor the 
	while (flag)
	{
		// Get the timer configuration
		n->getParam("timer", timer);
		switch(timer)
		{
			case 4 :  tmp.id = TIMER4; break;
			case 5 :  tmp.id = TIMER5; break;
			case 6 :  tmp.id = TIMER6; break;
			case 7 :  tmp.id = TIMER7; break;
		}

		// Perform ioctl to find new parameters, and cache them if found
		if (ioctl(fd, ROSELINE_GET_SYNC, &tmp) != -1)
			if (tmp.seq > syn.seq)
			{
				ROS_INFO("Received time sync update");
				memcpy(&syn,&tmp,sizeof(roseline_sync));
			}

		// Sleep for a little bit between checks
		boost::this_thread::sleep(boost::posix_time::milliseconds(10));
	}
}

// Main entry point of application
int main( int argc, char** argv)
{
	// Initialise ROS
	ros::init(argc, argv, "anchor");
	ros::NodeHandle n("~");
	pub_anc = n.advertise<tdoa::Anchor>("/anchor", 1000);
	sub_cfg = n.subscribe("/configuration", 1000, cb_cfg);
	sub_tri = n.subscribe("/trigger", 1000, cb_tri);

	// Basic parameters for the system
	bool simple = true;

	// Ummutable parameters
	n.getParam("x", msg_anc.pose.position.x);	// Anchor position X
	n.getParam("y", msg_anc.pose.position.y);	// Anchor position Y
	n.getParam("z", msg_anc.pose.position.z);	// Anchor position Z
	n.getParam("n", msg_anc.name);			// Name of the anchor
	n.getParam("simple", simple);			// Is this a simple (interrupt-triggered ADC)

	// Open an IOCTL handle to register for both capture events
	const char *file_name = "/dev/roseline";
	fd = open(file_name, O_RDWR);
	if (fd == -1)
		ROS_ERROR("Cannot open file descriptor to ROSELINE ioctl()");
	
	// Protocol to listen for time sync updates
	boost::thread tsyncp_thread(boost::bind(worker_syn,&n));

	// This thread will be responsible for doing all of the neat stuff
	boost::thread usonic_thread;
	boost::thread filter_thread;
	if (simple)
	    	usonic_thread = boost::thread(boost::bind(worker_cap,&n));
	else
	{
	    	usonic_thread = boost::thread(boost::bind(worker_adc,&n));
	    	filter_thread = boost::thread(boost::bind(worker_fil,&n));
	}

	// Keep spinning until ROS is shut down (the ADC sampling will run int the background)
	ros::spin();

	// Signal threads to end
	flag = false;
	tsyncp_thread.join();	// Timsync parameter listener
	usonic_thread.join();	// Ultrasonic measurement listener
	filter_thread.join();	// Ultrasonic filter

	// Close acess to ioctl
	close(fd);

	// Make for great success
	return 0;
}
