/*
 * matched_filter_conj.c
 *
 * Code generation for function 'matched_filter_conj'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "matched_filter_conj.h"
#include "ifft.h"
#include "bsxfun.h"
#include "fft.h"

/* Function Definitions */
void matched_filter_conj(const float signal[48000], const creal32_T
  template_conj[65536], float filtered[65536])
{
  static creal32_T fft_signal[65536];
  static creal32_T fcv2[65536];
  int i;

  /* MATCHED_FILTER_CONJ Summary of this function goes here */
  /*    Detailed explanation goes here */
  fft(signal, fft_signal);
  bsxfun(fft_signal, template_conj, fcv2);
  ifft(fcv2, fft_signal);
  for (i = 0; i < 65536; i++) {
    filtered[i] = fft_signal[i].re;
  }
}

/* End of code generation (matched_filter_conj.c) */
