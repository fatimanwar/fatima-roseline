/*
 * _coder_demodulate_roseline_api.h
 *
 * Code generation for function 'demodulate_roseline'
 *
 */

#ifndef ___CODER_DEMODULATE_ROSELINE_API_H__
#define ___CODER_DEMODULATE_ROSELINE_API_H__
/* Include files */
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"

/* Function Declarations */
extern void demodulate_roseline_initialize(emlrtContext *aContext);
extern void demodulate_roseline_terminate(void);
extern void demodulate_roseline_atexit(void);
extern void demodulate_roseline_api(const mxArray *prhs[6], const mxArray *plhs[2]);
extern void demodulate_roseline(real32_T rec[48000], real32_T THRESHOLD_DIVISOR, real32_T THRESHOLD_MULTIPLIER, real32_T *TOF_s, real32_T *RSSI);
extern void demodulate_roseline_xil_terminate(void);

#endif
/* End of code generation (_coder_demodulate_roseline_api.h) */
