/*
 * _coder_demodulate_roseline_api.c
 *
 * Code generation for function 'demodulate_roseline'
 *
 */

/* Include files */
#include "_coder_demodulate_roseline_api.h"

/* Function Declarations */
static real32_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *rec,
  const char_T *identifier))[48000];
static real32_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[48000];
static real32_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *THRESHOLD_DIVISOR, const char_T *identifier);
static real32_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *emlrt_marshallOut(const real32_T u);
static real32_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[48000];
static real32_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);

/* Function Definitions */
void demodulate_roseline_initialize(emlrtContext *aContext)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtCreateRootTLS(&emlrtRootTLSGlobal, aContext, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

void demodulate_roseline_terminate(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

void demodulate_roseline_atexit(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  demodulate_roseline_xil_terminate();
}

void demodulate_roseline_api(const mxArray *prhs[6], const mxArray *plhs[2])
{
  static const uint32_T upchirp_conj[4] = { 3856757314U, 1384013669U,
    3754607714U, 320548482U };

  static const uint32_T Fs[4] = { 1125703075U, 2423509671U, 2935162258U,
    4059327423U };

  static const uint32_T NFFT[4] = { 2889934616U, 2501429503U, 2492452377U,
    2712359907U };

  real32_T (*rec)[48000];
  real32_T THRESHOLD_DIVISOR;
  real32_T THRESHOLD_MULTIPLIER;
  real32_T RSSI;
  real32_T TOF_s;
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;
  prhs[0] = emlrtProtectR2012b(prhs[0], 0, false, -1);

  /* Check constant function inputs */
  emlrtCheckArrayChecksumR2014a(&st, "upchirp_conj", upchirp_conj, prhs[1],
    false);
  emlrtCheckArrayChecksumR2014a(&st, "Fs", Fs, prhs[4], false);
  emlrtCheckArrayChecksumR2014a(&st, "NFFT", NFFT, prhs[5], false);

  /* Marshall function inputs */
  rec = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "rec");
  THRESHOLD_DIVISOR = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]),
    "THRESHOLD_DIVISOR");
  THRESHOLD_MULTIPLIER = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]),
    "THRESHOLD_MULTIPLIER");

  /* Invoke the target function */
  demodulate_roseline(*rec, THRESHOLD_DIVISOR, THRESHOLD_MULTIPLIER, &TOF_s,
                      &RSSI);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(TOF_s);
  plhs[1] = emlrt_marshallOut(RSSI);
}

static real32_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *rec,
  const char_T *identifier))[48000]
{
  real32_T (*y)[48000];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(sp, emlrtAlias(rec), &thisId);
  emlrtDestroyArray(&rec);
  return y;
}
  static real32_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[48000]
{
  real32_T (*y)[48000];
  y = e_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static real32_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *THRESHOLD_DIVISOR, const char_T *identifier)
{
  real32_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(sp, emlrtAlias(THRESHOLD_DIVISOR), &thisId);
  emlrtDestroyArray(&THRESHOLD_DIVISOR);
  return y;
}

static real32_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real32_T y;
  y = f_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *emlrt_marshallOut(const real32_T u)
{
  const mxArray *y;
  const mxArray *m0;
  y = NULL;
  m0 = emlrtCreateNumericMatrix(1, 1, mxSINGLE_CLASS, mxREAL);
  *(real32_T *)mxGetData(m0) = u;
  emlrtAssign(&y, m0);
  return y;
}

static real32_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[48000]
{
  real32_T (*ret)[48000];
  int32_T iv0[1];
  iv0[0] = 48000;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 1U, iv0);
  ret = (real32_T (*)[48000])mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real32_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real32_T ret;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "single", false, 0U, 0);
  ret = *(real32_T *)mxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/* End of code generation (_coder_demodulate_roseline_api.c) */
