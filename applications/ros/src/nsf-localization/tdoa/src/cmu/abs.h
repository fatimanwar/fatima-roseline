/*
 * abs.h
 *
 * Code generation for function 'abs'
 *
 */

#ifndef __ABS_H__
#define __ABS_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void b_abs(const emxArray_real32_T *x, emxArray_real32_T *y);
extern void c_abs(const creal32_T x[16384], float y[16384]);

#endif

/* End of code generation (abs.h) */
