/*
 * sign.c
 *
 * Code generation for function 'sign'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "sign.h"

/* Function Definitions */
void b_sign(float x[2])
{
  int k;
  for (k = 0; k < 2; k++) {
    if (x[k] < 0.0F) {
      x[k] = -1.0F;
    } else if (x[k] > 0.0F) {
      x[k] = 1.0F;
    } else {
      if (x[k] == 0.0F) {
        x[k] = 0.0F;
      }
    }
  }
}

/* End of code generation (sign.c) */
