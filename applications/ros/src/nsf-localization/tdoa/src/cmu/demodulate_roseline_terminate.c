/*
 * demodulate_roseline_terminate.c
 *
 * Code generation for function 'demodulate_roseline_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "demodulate_roseline_terminate.h"

/* Function Definitions */
void demodulate_roseline_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (demodulate_roseline_terminate.c) */
