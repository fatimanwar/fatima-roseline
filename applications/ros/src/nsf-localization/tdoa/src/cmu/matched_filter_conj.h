/*
 * matched_filter_conj.h
 *
 * Code generation for function 'matched_filter_conj'
 *
 */

#ifndef __MATCHED_FILTER_CONJ_H__
#define __MATCHED_FILTER_CONJ_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void matched_filter_conj(const float signal[48000], const creal32_T
  template_conj[65536], float filtered[65536]);

#endif

/* End of code generation (matched_filter_conj.h) */
