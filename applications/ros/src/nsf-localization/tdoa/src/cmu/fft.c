/*
 * fft.c
 *
 * Code generation for function 'fft'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "fft.h"

/* Function Definitions */
void b_fft(const float x[9601], creal32_T y[16384])
{
  int i;
  float costab1q[4097];
  int k;
  float costab[8193];
  float sintab[8193];
  int ix;
  int ju;
  int iy;
  boolean_T tst;
  float temp_re;
  int iDelta;
  int iDelta2;
  int iheight;
  float temp_im;
  float twid_re;
  float twid_im;
  for (i = 0; i < 16384; i++) {
    y[i].re = 0.0F;
    y[i].im = 0.0F;
  }

  costab1q[0] = 1.0F;
  for (k = 0; k < 2048; k++) {
    costab1q[k + 1] = (real32_T)cos(0.000383495208F * ((float)k + 1.0F));
  }

  for (k = 0; k < 2047; k++) {
    costab1q[k + 2049] = (real32_T)sin(0.000383495208F * (4096.0F - ((float)k +
      2049.0F)));
  }

  costab1q[4096] = 0.0F;
  costab[0] = 1.0F;
  sintab[0] = 0.0F;
  for (k = 0; k < 4096; k++) {
    costab[k + 1] = costab1q[k + 1];
    sintab[k + 1] = -costab1q[4095 - k];
  }

  for (k = 0; k < 4096; k++) {
    costab[k + 4097] = -costab1q[4095 - k];
    sintab[k + 4097] = -costab1q[k + 1];
  }

  ix = 0;
  ju = 0;
  iy = 0;
  for (i = 0; i < 9600; i++) {
    y[iy].re = x[ix];
    y[iy].im = 0.0F;
    iy = 16384;
    tst = true;
    while (tst) {
      iy >>= 1;
      ju ^= iy;
      tst = ((ju & iy) == 0);
    }

    iy = ju;
    ix++;
  }

  y[iy].re = x[ix];
  y[iy].im = 0.0F;
  for (i = 0; i < 16384; i += 2) {
    temp_re = y[i + 1].re;
    y[i + 1].re = y[i].re - y[i + 1].re;
    y[i + 1].im = 0.0F;
    y[i].re += temp_re;
    y[i].im = 0.0F;
  }

  iDelta = 2;
  iDelta2 = 4;
  k = 4096;
  iheight = 16381;
  while (k > 0) {
    for (i = 0; i < iheight; i += iDelta2) {
      iy = i + iDelta;
      temp_re = y[iy].re;
      temp_im = y[iy].im;
      y[i + iDelta].re = y[i].re - y[iy].re;
      y[i + iDelta].im = y[i].im - y[iy].im;
      y[i].re += temp_re;
      y[i].im += temp_im;
    }

    iy = 1;
    for (ix = k; ix < 8192; ix += k) {
      twid_re = costab[ix];
      twid_im = sintab[ix];
      i = iy;
      ju = iy + iheight;
      while (i < ju) {
        temp_re = twid_re * y[i + iDelta].re - twid_im * y[i + iDelta].im;
        temp_im = twid_re * y[i + iDelta].im + twid_im * y[i + iDelta].re;
        y[i + iDelta].re = y[i].re - temp_re;
        y[i + iDelta].im = y[i].im - temp_im;
        y[i].re += temp_re;
        y[i].im += temp_im;
        i += iDelta2;
      }

      iy++;
    }

    k /= 2;
    iDelta = iDelta2;
    iDelta2 <<= 1;
    iheight -= iDelta;
  }
}

void fft(const float x[48000], creal32_T y[65536])
{
  int i;
  float costab1q[16385];
  int k;
  static float costab[32769];
  static float sintab[32769];
  int ix;
  int ju;
  int iy;
  boolean_T tst;
  float temp_re;
  int iDelta;
  int iDelta2;
  int iheight;
  float temp_im;
  float twid_re;
  float twid_im;
  for (i = 0; i < 65536; i++) {
    y[i].re = 0.0F;
    y[i].im = 0.0F;
  }

  costab1q[0] = 1.0F;
  for (k = 0; k < 8192; k++) {
    costab1q[k + 1] = (real32_T)cos(9.58738E-5F * ((float)k + 1.0F));
  }

  for (k = 0; k < 8191; k++) {
    costab1q[k + 8193] = (real32_T)sin(9.58738E-5F * (16384.0F - ((float)k +
      8193.0F)));
  }

  costab1q[16384] = 0.0F;
  costab[0] = 1.0F;
  sintab[0] = 0.0F;
  for (k = 0; k < 16384; k++) {
    costab[k + 1] = costab1q[k + 1];
    sintab[k + 1] = -costab1q[16383 - k];
  }

  for (k = 0; k < 16384; k++) {
    costab[k + 16385] = -costab1q[16383 - k];
    sintab[k + 16385] = -costab1q[k + 1];
  }

  ix = 0;
  ju = 0;
  iy = 0;
  for (i = 0; i < 47999; i++) {
    y[iy].re = x[ix];
    y[iy].im = 0.0F;
    iy = 65536;
    tst = true;
    while (tst) {
      iy >>= 1;
      ju ^= iy;
      tst = ((ju & iy) == 0);
    }

    iy = ju;
    ix++;
  }

  y[iy].re = x[ix];
  y[iy].im = 0.0F;
  for (i = 0; i < 65536; i += 2) {
    temp_re = y[i + 1].re;
    y[i + 1].re = y[i].re - y[i + 1].re;
    y[i + 1].im = 0.0F;
    y[i].re += temp_re;
    y[i].im = 0.0F;
  }

  iDelta = 2;
  iDelta2 = 4;
  k = 16384;
  iheight = 65533;
  while (k > 0) {
    for (i = 0; i < iheight; i += iDelta2) {
      iy = i + iDelta;
      temp_re = y[iy].re;
      temp_im = y[iy].im;
      y[i + iDelta].re = y[i].re - y[iy].re;
      y[i + iDelta].im = y[i].im - y[iy].im;
      y[i].re += temp_re;
      y[i].im += temp_im;
    }

    iy = 1;
    for (ix = k; ix < 32768; ix += k) {
      twid_re = costab[ix];
      twid_im = sintab[ix];
      i = iy;
      ju = iy + iheight;
      while (i < ju) {
        temp_re = twid_re * y[i + iDelta].re - twid_im * y[i + iDelta].im;
        temp_im = twid_re * y[i + iDelta].im + twid_im * y[i + iDelta].re;
        y[i + iDelta].re = y[i].re - temp_re;
        y[i + iDelta].im = y[i].im - temp_im;
        y[i].re += temp_re;
        y[i].im += temp_im;
        i += iDelta2;
      }

      iy++;
    }

    k /= 2;
    iDelta = iDelta2;
    iDelta2 <<= 1;
    iheight -= iDelta;
  }
}

/* End of code generation (fft.c) */
