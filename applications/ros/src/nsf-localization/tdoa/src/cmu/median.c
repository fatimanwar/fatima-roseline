/*
 * median.c
 *
 * Code generation for function 'median'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "median.h"
#include "demodulate_roseline_emxutil.h"

/* Function Declarations */
static void eml_sort_idx(const emxArray_real32_T *x, emxArray_int32_T *idx);
static float vectormedian(const emxArray_real32_T *v);

/* Function Definitions */
static void eml_sort_idx(const emxArray_real32_T *x, emxArray_int32_T *idx)
{
  int n;
  int unnamed_idx_0;
  int i;
  boolean_T p;
  emxArray_int32_T *idx0;
  int i2;
  int j;
  int pEnd;
  int b_p;
  int q;
  int qEnd;
  int kEnd;
  n = x->size[0];
  unnamed_idx_0 = x->size[0];
  i = idx->size[0];
  idx->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)idx, i, (int)sizeof(int));
  for (unnamed_idx_0 = 1; unnamed_idx_0 <= n; unnamed_idx_0++) {
    idx->data[unnamed_idx_0 - 1] = unnamed_idx_0;
  }

  for (unnamed_idx_0 = 1; unnamed_idx_0 <= n - 1; unnamed_idx_0 += 2) {
    if ((x->data[unnamed_idx_0 - 1] <= x->data[unnamed_idx_0]) || rtIsNaNF
        (x->data[unnamed_idx_0])) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
    } else {
      idx->data[unnamed_idx_0 - 1] = unnamed_idx_0 + 1;
      idx->data[unnamed_idx_0] = unnamed_idx_0;
    }
  }

  emxInit_int32_T(&idx0, 1);
  i = idx0->size[0];
  idx0->size[0] = x->size[0];
  emxEnsureCapacity((emxArray__common *)idx0, i, (int)sizeof(int));
  unnamed_idx_0 = x->size[0];
  for (i = 0; i < unnamed_idx_0; i++) {
    idx0->data[i] = 1;
  }

  i = 2;
  while (i < n) {
    i2 = i << 1;
    j = 1;
    for (pEnd = 1 + i; pEnd < n + 1; pEnd = qEnd + i) {
      b_p = j;
      q = pEnd - 1;
      qEnd = j + i2;
      if (qEnd > n + 1) {
        qEnd = n + 1;
      }

      unnamed_idx_0 = 0;
      kEnd = qEnd - j;
      while (unnamed_idx_0 + 1 <= kEnd) {
        if ((x->data[idx->data[b_p - 1] - 1] <= x->data[idx->data[q] - 1]) ||
            rtIsNaNF(x->data[idx->data[q] - 1])) {
          p = true;
        } else {
          p = false;
        }

        if (p) {
          idx0->data[unnamed_idx_0] = idx->data[b_p - 1];
          b_p++;
          if (b_p == pEnd) {
            while (q + 1 < qEnd) {
              unnamed_idx_0++;
              idx0->data[unnamed_idx_0] = idx->data[q];
              q++;
            }
          }
        } else {
          idx0->data[unnamed_idx_0] = idx->data[q];
          q++;
          if (q + 1 == qEnd) {
            while (b_p < pEnd) {
              unnamed_idx_0++;
              idx0->data[unnamed_idx_0] = idx->data[b_p - 1];
              b_p++;
            }
          }
        }

        unnamed_idx_0++;
      }

      for (unnamed_idx_0 = 0; unnamed_idx_0 + 1 <= kEnd; unnamed_idx_0++) {
        idx->data[(j + unnamed_idx_0) - 1] = idx0->data[unnamed_idx_0];
      }

      j = qEnd;
    }

    i = i2;
  }

  emxFree_int32_T(&idx0);
}

static float vectormedian(const emxArray_real32_T *v)
{
  float m;
  emxArray_int32_T *idx;
  int midm1;
  emxInit_int32_T(&idx, 1);
  midm1 = v->size[0];
  midm1 /= 2;
  eml_sort_idx(v, idx);
  if (rtIsNaNF(v->data[idx->data[idx->size[0] - 1] - 1])) {
    m = v->data[idx->data[idx->size[0] - 1] - 1];
  } else if (midm1 << 1 == v->size[0]) {
    if (rtIsInfF(v->data[idx->data[midm1 - 1] - 1]) || rtIsInfF(v->data
         [idx->data[midm1] - 1])) {
      m = (v->data[idx->data[midm1 - 1] - 1] + v->data[idx->data[midm1] - 1]) /
        2.0F;
    } else {
      m = v->data[idx->data[midm1 - 1] - 1] + (v->data[idx->data[midm1] - 1] -
        v->data[idx->data[midm1 - 1] - 1]) / 2.0F;
    }
  } else {
    m = v->data[idx->data[midm1] - 1];
  }

  emxFree_int32_T(&idx);
  return m;
}

float median(const emxArray_real32_T *x)
{
  float y;
  if (x->size[0] == 0) {
    y = ((real32_T)rtNaN);
  } else {
    y = vectormedian(x);
  }

  return y;
}

/* End of code generation (median.c) */
