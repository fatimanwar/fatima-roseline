/*
 * demodulate_roseline.h
 *
 * Code generation for function 'demodulate_roseline'
 *
 */

#ifndef __DEMODULATE_ROSELINE_H__
#define __DEMODULATE_ROSELINE_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void demodulate_roseline(const float rec[48000], float THRESHOLD_DIVISOR,
  float THRESHOLD_MULTIPLIER, float *TOF_s, float *RSSI);

#endif

/* End of code generation (demodulate_roseline.h) */
