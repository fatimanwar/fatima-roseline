/*
 * demodulate_roseline_initialize.c
 *
 * Code generation for function 'demodulate_roseline_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "demodulate_roseline_initialize.h"

/* Function Definitions */
void demodulate_roseline_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (demodulate_roseline_initialize.c) */
