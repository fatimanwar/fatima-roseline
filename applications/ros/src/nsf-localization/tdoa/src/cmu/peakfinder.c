/*
 * peakfinder.c
 *
 * Code generation for function 'peakfinder'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "peakfinder.h"
#include "demodulate_roseline_emxutil.h"
#include "sign.h"
#include "diff.h"

/* Function Declarations */
static int b_compute_nones(const boolean_T x_data[], int n);
static void b_eml_li_find(const boolean_T x_data[], const int x_size[2], int
  y_data[], int y_size[1]);
static void b_eml_null_assignment(emxArray_real32_T *x);
static void c_eml_null_assignment(emxArray_real_T *x);
static int compute_nones(const boolean_T x[16383]);
static void d_eml_null_assignment(emxArray_real_T *x);
static void eml_li_find(const boolean_T x[16383], int y_data[], int y_size[1]);
static void eml_null_assignment(emxArray_real32_T *x);

/* Function Definitions */
static int b_compute_nones(const boolean_T x_data[], int n)
{
  int k;
  int i;
  k = 0;
  for (i = 1; i <= n; i++) {
    if (x_data[i - 1]) {
      k++;
    }
  }

  return k;
}

static void b_eml_li_find(const boolean_T x_data[], const int x_size[2], int
  y_data[], int y_size[1])
{
  int n;
  int k;
  int i;
  n = x_size[0] * x_size[1];
  k = b_compute_nones(x_data, n);
  y_size[0] = k;
  k = 0;
  for (i = 1; i <= n; i++) {
    if (x_data[i - 1]) {
      y_data[k] = i;
      k++;
    }
  }
}

static void b_eml_null_assignment(emxArray_real32_T *x)
{
  emxArray_real32_T *b_x;
  int k;
  int loop_ub;
  emxArray_real32_T *c_x;
  emxInit_real32_T(&b_x, 1);
  k = b_x->size[0];
  b_x->size[0] = x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(float));
  loop_ub = x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = x->data[k];
  }

  for (k = 1; k < x->size[0]; k++) {
    b_x->data[k - 1] = b_x->data[k];
  }

  emxInit_real32_T(&c_x, 1);
  loop_ub = x->size[0] - 2;
  k = c_x->size[0];
  c_x->size[0] = loop_ub + 1;
  emxEnsureCapacity((emxArray__common *)c_x, k, (int)sizeof(float));
  for (k = 0; k <= loop_ub; k++) {
    c_x->data[k] = b_x->data[k];
  }

  k = b_x->size[0];
  b_x->size[0] = c_x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(float));
  loop_ub = c_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = c_x->data[k];
  }

  emxFree_real32_T(&c_x);
  k = x->size[0];
  x->size[0] = b_x->size[0];
  emxEnsureCapacity((emxArray__common *)x, k, (int)sizeof(float));
  loop_ub = b_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    x->data[k] = b_x->data[k];
  }

  emxFree_real32_T(&b_x);
}

static void c_eml_null_assignment(emxArray_real_T *x)
{
  emxArray_real_T *b_x;
  int k;
  int loop_ub;
  emxArray_real_T *c_x;
  b_emxInit_real_T(&b_x, 1);
  k = b_x->size[0];
  b_x->size[0] = x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(double));
  loop_ub = x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = x->data[k];
  }

  for (k = 2; k < x->size[0]; k++) {
    b_x->data[k - 1] = b_x->data[k];
  }

  b_emxInit_real_T(&c_x, 1);
  loop_ub = x->size[0] - 2;
  k = c_x->size[0];
  c_x->size[0] = loop_ub + 1;
  emxEnsureCapacity((emxArray__common *)c_x, k, (int)sizeof(double));
  for (k = 0; k <= loop_ub; k++) {
    c_x->data[k] = b_x->data[k];
  }

  k = b_x->size[0];
  b_x->size[0] = c_x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(double));
  loop_ub = c_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = c_x->data[k];
  }

  emxFree_real_T(&c_x);
  k = x->size[0];
  x->size[0] = b_x->size[0];
  emxEnsureCapacity((emxArray__common *)x, k, (int)sizeof(double));
  loop_ub = b_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    x->data[k] = b_x->data[k];
  }

  emxFree_real_T(&b_x);
}

static int compute_nones(const boolean_T x[16383])
{
  int k;
  int i;
  k = 0;
  for (i = 0; i < 16383; i++) {
    if (x[i]) {
      k++;
    }
  }

  return k;
}

static void d_eml_null_assignment(emxArray_real_T *x)
{
  emxArray_real_T *b_x;
  int k;
  int loop_ub;
  emxArray_real_T *c_x;
  b_emxInit_real_T(&b_x, 1);
  k = b_x->size[0];
  b_x->size[0] = x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(double));
  loop_ub = x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = x->data[k];
  }

  for (k = 1; k < x->size[0]; k++) {
    b_x->data[k - 1] = b_x->data[k];
  }

  b_emxInit_real_T(&c_x, 1);
  loop_ub = x->size[0] - 2;
  k = c_x->size[0];
  c_x->size[0] = loop_ub + 1;
  emxEnsureCapacity((emxArray__common *)c_x, k, (int)sizeof(double));
  for (k = 0; k <= loop_ub; k++) {
    c_x->data[k] = b_x->data[k];
  }

  k = b_x->size[0];
  b_x->size[0] = c_x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(double));
  loop_ub = c_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = c_x->data[k];
  }

  emxFree_real_T(&c_x);
  k = x->size[0];
  x->size[0] = b_x->size[0];
  emxEnsureCapacity((emxArray__common *)x, k, (int)sizeof(double));
  loop_ub = b_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    x->data[k] = b_x->data[k];
  }

  emxFree_real_T(&b_x);
}

static void eml_li_find(const boolean_T x[16383], int y_data[], int y_size[1])
{
  int j;
  int i;
  y_size[0] = compute_nones(x);
  j = 0;
  for (i = 0; i < 16383; i++) {
    if (x[i]) {
      y_data[j] = i + 1;
      j++;
    }
  }
}

static void eml_null_assignment(emxArray_real32_T *x)
{
  emxArray_real32_T *b_x;
  int k;
  int loop_ub;
  emxArray_real32_T *c_x;
  emxInit_real32_T(&b_x, 1);
  k = b_x->size[0];
  b_x->size[0] = x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(float));
  loop_ub = x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = x->data[k];
  }

  for (k = 2; k < x->size[0]; k++) {
    b_x->data[k - 1] = b_x->data[k];
  }

  emxInit_real32_T(&c_x, 1);
  loop_ub = x->size[0] - 2;
  k = c_x->size[0];
  c_x->size[0] = loop_ub + 1;
  emxEnsureCapacity((emxArray__common *)c_x, k, (int)sizeof(float));
  for (k = 0; k <= loop_ub; k++) {
    c_x->data[k] = b_x->data[k];
  }

  k = b_x->size[0];
  b_x->size[0] = c_x->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, k, (int)sizeof(float));
  loop_ub = c_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    b_x->data[k] = c_x->data[k];
  }

  emxFree_real32_T(&c_x);
  k = x->size[0];
  x->size[0] = b_x->size[0];
  emxEnsureCapacity((emxArray__common *)x, k, (int)sizeof(float));
  loop_ub = b_x->size[0];
  for (k = 0; k < loop_ub; k++) {
    x->data[k] = b_x->data[k];
  }

  emxFree_real32_T(&b_x);
}

void peakfinder(const float x0[16384], float sel, float thresh, emxArray_real_T *
                peakInds, emxArray_real_T *peakMags)
{
  int tempLoc;
  static float dx0[16383];
  boolean_T b_dx0[16383];
  int i;
  int tmp_size[1];
  static int tmp_data[16383];
  int i2;
  boolean_T x[16382];
  short ii_data[16382];
  int ii;
  boolean_T exitg4;
  boolean_T guard1 = false;
  emxArray_real_T *ind;
  emxArray_real32_T *b_x;
  emxArray_real_T *r1;
  int len;
  int ixstart;
  float mtmp;
  boolean_T exitg3;
  float tempMag;
  boolean_T foundPeak;
  float leftMin;
  float c_x[3];
  float signDx[2];
  emxArray_real_T *peakLoc;
  emxArray_real_T *peakMag;
  boolean_T exitg2;
  emxArray_real_T *r2;
  int b_tmp_data[8192];
  boolean_T exitg1;
  int m_size[2];
  boolean_T m_data[8192];
  emxArray_real_T *b_peakInds;
  emxArray_real_T *b_peakMags;

  /* PEAKFINDER Noise tolerant fast peak finding algorithm */
  /*    INPUTS: */
  /*        x0 - A real vector from the maxima will be found (required) */
  /*        sel - The amount above surrounding data for a peak to be */
  /*            identified (default = (max(x0)-min(x0))/4). Larger values mean */
  /*            the algorithm is more selective in finding peaks. */
  /*        thresh - A threshold value which peaks must be larger than to be */
  /*            maxima or smaller than to be minima. */
  /*        extrema - 1 if maxima are desired, -1 if minima are desired */
  /*            (default = maxima, 1) */
  /*    OUTPUTS: */
  /*        peakLoc - The indicies of the identified peaks in x0 */
  /*        peakMag - The magnitude of the identified peaks */
  /*  */
  /*    [peakLoc] = peakfinder(x0) returns the indicies of local maxima that */
  /*        are at least 1/4 the range of the data above surrounding data. */
  /*  */
  /*    [peakLoc] = peakfinder(x0,sel) returns the indicies of local maxima */
  /*        that are at least sel above surrounding data. */
  /*  */
  /*    [peakLoc] = peakfinder(x0,sel,thresh) returns the indicies of local  */
  /*        maxima that are at least sel above surrounding data and larger */
  /*        (smaller) than thresh if you are finding maxima (minima). */
  /*  */
  /*    [peakLoc] = peakfinder(x0,sel,thresh,extrema) returns the maxima of the */
  /*        data if extrema > 0 and the minima of the data if extrema < 0 */
  /*  */
  /*    [peakLoc, peakMag] = peakfinder(x0,...) returns the indicies of the */
  /*        local maxima as well as the magnitudes of those maxima */
  /*  */
  /*    If called with no output the identified maxima will be plotted along */
  /*        with the input data. */
  /*  */
  /*    Note: If repeated values are found the first is identified as the peak */
  /*  */
  /*  Ex: */
  /*  t = 0:.0001:10; */
  /*  x = 12*sin(10*2*pi*t)-3*sin(.1*2*pi*t)+randn(1,numel(t)); */
  /*  x(1250:1255) = max(x); */
  /*  peakfinder(x) */
  /*  */
  /*  Copyright Nathanael C. Yoder 2011 (nyoder@gmail.com) */
  /*  Perform error checking and set defaults if not passed in */
  /* error(nargchk(1,4,nargin,'struct')); */
  /* error(nargoutchk(0,2,nargout,'struct')); */
  tempLoc = 0;

  /*  Make it so we are finding maxima regardless */
  /*  Adjust threshold according to extrema. */
  diff(x0, dx0);

  /*  Find derivative */
  for (i = 0; i < 16383; i++) {
    b_dx0[i] = (dx0[i] == 0.0F);
  }

  eml_li_find(b_dx0, tmp_data, tmp_size);
  i = tmp_size[0];
  for (i2 = 0; i2 < i; i2++) {
    dx0[tmp_data[i2] - 1] = -2.22044605E-16F;
  }

  /*  This is so we find the first of repeated values */
  for (i = 0; i < 16382; i++) {
    x[i] = (dx0[i] * dx0[i + 1] < 0.0F);
  }

  i = 0;
  ii = 1;
  exitg4 = false;
  while ((!exitg4) && (ii < 16383)) {
    guard1 = false;
    if (x[ii - 1]) {
      i++;
      ii_data[i - 1] = (short)ii;
      if (i >= 16382) {
        exitg4 = true;
      } else {
        guard1 = true;
      }
    } else {
      guard1 = true;
    }

    if (guard1) {
      ii++;
    }
  }

  if (1 > i) {
    i = 0;
  }

  b_emxInit_real_T(&ind, 1);
  i2 = ind->size[0];
  ind->size[0] = i;
  emxEnsureCapacity((emxArray__common *)ind, i2, (int)sizeof(double));
  for (i2 = 0; i2 < i; i2++) {
    ind->data[i2] = (double)ii_data[i2] + 1.0;
  }

  emxInit_real32_T(&b_x, 1);

  /*  Find where the derivative changes sign */
  /*  Include endpoints in potential peaks and valleys */
  i2 = b_x->size[0];
  b_x->size[0] = 2 + ind->size[0];
  emxEnsureCapacity((emxArray__common *)b_x, i2, (int)sizeof(float));
  b_x->data[0] = x0[0];
  i = ind->size[0];
  for (i2 = 0; i2 < i; i2++) {
    b_x->data[i2 + 1] = x0[(int)ind->data[i2] - 1];
  }

  b_emxInit_real_T(&r1, 1);
  b_x->data[1 + ind->size[0]] = x0[16383];
  i2 = r1->size[0];
  r1->size[0] = 2 + ind->size[0];
  emxEnsureCapacity((emxArray__common *)r1, i2, (int)sizeof(double));
  r1->data[0] = 1.0;
  i = ind->size[0];
  for (i2 = 0; i2 < i; i2++) {
    r1->data[i2 + 1] = ind->data[i2];
  }

  r1->data[1 + ind->size[0]] = 16384.0;
  i2 = ind->size[0];
  ind->size[0] = r1->size[0];
  emxEnsureCapacity((emxArray__common *)ind, i2, (int)sizeof(double));
  i = r1->size[0];
  for (i2 = 0; i2 < i; i2++) {
    ind->data[i2] = r1->data[i2];
  }

  emxFree_real_T(&r1);

  /*  x only has the peaks, valleys, and endpoints */
  len = b_x->size[0];
  ixstart = 1;
  i = b_x->size[0];
  mtmp = b_x->data[0];
  if (rtIsNaNF(b_x->data[0])) {
    ii = 2;
    exitg3 = false;
    while ((!exitg3) && (ii <= i)) {
      ixstart = ii;
      if (!rtIsNaNF(b_x->data[ii - 1])) {
        mtmp = b_x->data[ii - 1];
        exitg3 = true;
      } else {
        ii++;
      }
    }
  }

  if (ixstart < b_x->size[0]) {
    while (ixstart + 1 <= i) {
      if (b_x->data[ixstart] < mtmp) {
        mtmp = b_x->data[ixstart];
      }

      ixstart++;
    }
  }

  if (b_x->size[0] > 2) {
    /*  Function with peaks and valleys */
    /*  Set initial parameters for loop */
    tempMag = mtmp;
    foundPeak = false;
    leftMin = mtmp;

    /*  Deal with first point a little differently since tacked it on */
    /*  Calculate the sign of the derivative since we taked the first point */
    /*   on it does not neccessarily alternate like the rest. */
    for (i2 = 0; i2 < 3; i2++) {
      c_x[i2] = b_x->data[i2];
    }

    b_diff(c_x, signDx);
    b_sign(signDx);
    if (signDx[0] <= 0.0F) {
      /*  The first point is larger or equal to the second */
      ii = -1;
      if (signDx[0] == signDx[1]) {
        /*  Want alternating signs */
        eml_null_assignment(b_x);
        c_eml_null_assignment(ind);
        len--;
      }
    } else {
      /*  First point is smaller than the second */
      ii = 0;
      if (signDx[0] == signDx[1]) {
        /*  Want alternating signs */
        b_eml_null_assignment(b_x);
        d_eml_null_assignment(ind);
        len--;
      }
    }

    b_emxInit_real_T(&peakLoc, 1);

    /*  Preallocate max number of maxima */
    i = (int)ceil((double)len / 2.0);
    i2 = peakLoc->size[0];
    peakLoc->size[0] = i;
    emxEnsureCapacity((emxArray__common *)peakLoc, i2, (int)sizeof(double));
    for (i2 = 0; i2 < i; i2++) {
      peakLoc->data[i2] = 0.0;
    }

    b_emxInit_real_T(&peakMag, 1);
    i2 = peakMag->size[0];
    peakMag->size[0] = i;
    emxEnsureCapacity((emxArray__common *)peakMag, i2, (int)sizeof(double));
    for (i2 = 0; i2 < i; i2++) {
      peakMag->data[i2] = 0.0;
    }

    ixstart = 0;

    /*  Loop through extrema which should be peaks and then valleys */
    exitg2 = false;
    while ((!exitg2) && (ii + 1 < len)) {
      ii++;

      /*  This is a peak */
      /*  Reset peak finding if we had a peak and the next peak is bigger */
      /*    than the last or the left min was small enough to reset. */
      if (foundPeak) {
        tempMag = mtmp;
        foundPeak = false;
      }

      /*  Make sure we don't iterate past the length of our vector */
      if (ii + 1 == len) {
        exitg2 = true;
      } else {
        /*  Found new peak that was lager than temp mag and selectivity larger */
        /*    than the minimum to its left. */
        if ((b_x->data[ii] > tempMag) && (b_x->data[ii] > leftMin + sel)) {
          tempLoc = ii + 1;
          tempMag = b_x->data[ii];
        }

        ii++;

        /*  Move onto the valley */
        /*  Come down at least sel from peak */
        if (tempMag > sel + b_x->data[ii]) {
          foundPeak = true;

          /*  We have found a peak */
          leftMin = b_x->data[ii];
          peakLoc->data[ixstart] = tempLoc;

          /*  Add peak to index */
          peakMag->data[ixstart] = tempMag;
          ixstart++;
        } else {
          if (b_x->data[ii] < leftMin) {
            /*  New left minima */
            leftMin = b_x->data[ii];
          }
        }
      }
    }

    /*  Check end point */
    if ((b_x->data[b_x->size[0] - 1] > tempMag) && (b_x->data[b_x->size[0] - 1] >
         leftMin + sel)) {
      peakLoc->data[ixstart] = len;
      peakMag->data[ixstart] = b_x->data[b_x->size[0] - 1];
      ixstart++;
    } else {
      if ((!foundPeak) && (tempMag > mtmp)) {
        /*  Check if we still need to add the last point */
        peakLoc->data[ixstart] = tempLoc;
        peakMag->data[ixstart] = tempMag;
        ixstart++;
      }
    }

    /*  Create output */
    if (1 > ixstart) {
      i = 0;
    } else {
      i = ixstart;
    }

    b_emxInit_real_T(&r2, 1);
    i2 = r2->size[0];
    r2->size[0] = i;
    emxEnsureCapacity((emxArray__common *)r2, i2, (int)sizeof(double));
    for (i2 = 0; i2 < i; i2++) {
      r2->data[i2] = ind->data[(int)peakLoc->data[i2] - 1];
    }

    emxFree_real_T(&peakLoc);
    i2 = peakInds->size[0] * peakInds->size[1];
    peakInds->size[0] = i;
    peakInds->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)peakInds, i2, (int)sizeof(double));
    for (i2 = 0; i2 < i; i2++) {
      peakInds->data[i2] = r2->data[i2];
    }

    emxFree_real_T(&r2);
    if (1 > ixstart) {
      i = 0;
    } else {
      i = ixstart;
    }

    i2 = peakMags->size[0] * peakMags->size[1];
    peakMags->size[0] = i;
    peakMags->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)peakMags, i2, (int)sizeof(double));
    for (i2 = 0; i2 < i; i2++) {
      b_tmp_data[i2] = 1 + i2;
    }

    for (i2 = 0; i2 < i; i2++) {
      peakMags->data[i2] = peakMag->data[b_tmp_data[i2] - 1];
    }

    emxFree_real_T(&peakMag);
  } else {
    /*  This is a monotone function where an endpoint is the only peak */
    ixstart = 1;
    tempMag = b_x->data[0];
    i = 0;
    if (rtIsNaNF(b_x->data[0])) {
      ii = 2;
      exitg1 = false;
      while ((!exitg1) && (ii <= 2)) {
        ixstart = 2;
        if (!rtIsNaNF(b_x->data[1])) {
          tempMag = b_x->data[1];
          i = 1;
          exitg1 = true;
        } else {
          ii = 3;
        }
      }
    }

    if ((ixstart < 2) && (b_x->data[1] > tempMag)) {
      tempMag = b_x->data[1];
      i = 1;
    }

    i2 = peakMags->size[0] * peakMags->size[1];
    peakMags->size[0] = 1;
    peakMags->size[1] = 1;
    emxEnsureCapacity((emxArray__common *)peakMags, i2, (int)sizeof(double));
    peakMags->data[0] = tempMag;
    if (tempMag > mtmp + sel) {
      i2 = peakInds->size[0] * peakInds->size[1];
      peakInds->size[0] = 1;
      peakInds->size[1] = 1;
      emxEnsureCapacity((emxArray__common *)peakInds, i2, (int)sizeof(double));
      peakInds->data[0] = ind->data[i];
    } else {
      i2 = peakMags->size[0] * peakMags->size[1];
      peakMags->size[0] = 0;
      peakMags->size[1] = 0;
      emxEnsureCapacity((emxArray__common *)peakMags, i2, (int)sizeof(double));
      i2 = peakInds->size[0] * peakInds->size[1];
      peakInds->size[0] = 0;
      peakInds->size[1] = 0;
      emxEnsureCapacity((emxArray__common *)peakInds, i2, (int)sizeof(double));
    }
  }

  emxFree_real32_T(&b_x);
  emxFree_real_T(&ind);

  /*  Apply threshold value.  Since always finding maxima it will always be */
  /*    larger than the thresh. */
  m_size[0] = peakMags->size[0];
  m_size[1] = peakMags->size[1];
  i = peakMags->size[0] * peakMags->size[1];
  for (i2 = 0; i2 < i; i2++) {
    m_data[i2] = (peakMags->data[i2] > thresh);
  }

  b_emxInit_real_T(&b_peakInds, 1);
  b_eml_li_find(m_data, m_size, b_tmp_data, tmp_size);
  i2 = b_peakInds->size[0];
  b_peakInds->size[0] = tmp_size[0];
  emxEnsureCapacity((emxArray__common *)b_peakInds, i2, (int)sizeof(double));
  i = tmp_size[0];
  for (i2 = 0; i2 < i; i2++) {
    b_peakInds->data[i2] = peakInds->data[b_tmp_data[i2] - 1];
  }

  i = tmp_size[0];
  i2 = peakInds->size[0] * peakInds->size[1];
  peakInds->size[0] = tmp_size[0];
  peakInds->size[1] = 1;
  emxEnsureCapacity((emxArray__common *)peakInds, i2, (int)sizeof(double));
  i2 = 0;
  while (i2 <= 0) {
    for (i2 = 0; i2 < i; i2++) {
      peakInds->data[i2] = b_peakInds->data[i2];
    }

    i2 = 1;
  }

  emxFree_real_T(&b_peakInds);
  b_emxInit_real_T(&b_peakMags, 1);
  b_eml_li_find(m_data, m_size, b_tmp_data, tmp_size);
  i2 = b_peakMags->size[0];
  b_peakMags->size[0] = tmp_size[0];
  emxEnsureCapacity((emxArray__common *)b_peakMags, i2, (int)sizeof(double));
  i = tmp_size[0];
  for (i2 = 0; i2 < i; i2++) {
    b_peakMags->data[i2] = peakMags->data[b_tmp_data[i2] - 1];
  }

  i = tmp_size[0];
  i2 = peakMags->size[0] * peakMags->size[1];
  peakMags->size[0] = tmp_size[0];
  peakMags->size[1] = 1;
  emxEnsureCapacity((emxArray__common *)peakMags, i2, (int)sizeof(double));
  i2 = 0;
  while (i2 <= 0) {
    for (i2 = 0; i2 < i; i2++) {
      peakMags->data[i2] = b_peakMags->data[i2];
    }

    i2 = 1;
  }

  emxFree_real_T(&b_peakMags);

  /*  Rotate data if needed */
  /*  Change sign of data if was finding minima */
  /*  Plot if no output desired */
  /*  if nargout == 0 */
  /*      if isempty(peakInds) */
  /*          disp('No significant peaks found') */
  /*      else */
  /*          figure; */
  /*          plot(1:len0,x0,'.-',peakInds,peakMags,'ro','linewidth',2); */
  /*      end */
  /*  else */
  /* varargout = {peakInds,peakMags}; */
  /*  end */
}

/* End of code generation (peakfinder.c) */
