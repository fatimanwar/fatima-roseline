/*
 * extract_envelope.c
 *
 * Code generation for function 'extract_envelope'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "extract_envelope.h"
#include "ifft.h"
#include "fft.h"

/* Function Definitions */
void extract_envelope(const float xr[9601], creal32_T x[16384])
{
  signed char h[16384];
  int i;
  static creal32_T b_x[16384];

  /* GET_ANALYTICAL_SIGNAL Summary of this function goes here */
  /*    Detailed explanation goes here */
  b_fft(xr, x);

  /*  n-point FFT over columns. */
  memset(&h[0], 0, sizeof(signed char) << 14);

  /*  nx1 for nonempty. 0x0 for empty. */
  /*  even and nonempty */
  for (i = 0; i < 2; i++) {
    h[i << 13] = 1;
  }

  memset(&h[1], 2, 8191U * sizeof(signed char));
  for (i = 0; i < 16384; i++) {
    b_x[i].re = (float)h[i] * x[i].re;
    b_x[i].im = (float)h[i] * x[i].im;
  }

  b_ifft(b_x, x);

  /*  Convert back to the original shape. */
}

/* End of code generation (extract_envelope.c) */
