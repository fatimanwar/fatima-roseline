/*
 * rms.h
 *
 * Code generation for function 'rms'
 *
 */

#ifndef __RMS_H__
#define __RMS_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern float rms(const float x[4801]);

#endif

/* End of code generation (rms.h) */
