/*
 * peakfinder.h
 *
 * Code generation for function 'peakfinder'
 *
 */

#ifndef __PEAKFINDER_H__
#define __PEAKFINDER_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void peakfinder(const float x0[16384], float sel, float thresh,
  emxArray_real_T *peakInds, emxArray_real_T *peakMags);

#endif

/* End of code generation (peakfinder.h) */
