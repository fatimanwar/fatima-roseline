/*
 * sign.h
 *
 * Code generation for function 'sign'
 *
 */

#ifndef __SIGN_H__
#define __SIGN_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void b_sign(float x[2]);

#endif

/* End of code generation (sign.h) */
