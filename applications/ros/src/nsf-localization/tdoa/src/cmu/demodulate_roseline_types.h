/*
 * demodulate_roseline_types.h
 *
 * Code generation for function 'demodulate_roseline'
 *
 */

#ifndef __DEMODULATE_ROSELINE_TYPES_H__
#define __DEMODULATE_ROSELINE_TYPES_H__

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef struct_emxArray__common
#define struct_emxArray__common
struct emxArray__common
{
    void *data;
    int *size;
    int allocatedSize;
    int numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray__common*/
#ifndef typedef_emxArray__common
#define typedef_emxArray__common
typedef struct emxArray__common emxArray__common;
#endif /*typedef_emxArray__common*/
#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T
struct emxArray_int32_T
{
    int *data;
    int *size;
    int allocatedSize;
    int numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray_int32_T*/
#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T
typedef struct emxArray_int32_T emxArray_int32_T;
#endif /*typedef_emxArray_int32_T*/
#ifndef struct_emxArray_int32_T_16382
#define struct_emxArray_int32_T_16382
struct emxArray_int32_T_16382
{
    int data[16382];
    int size[1];
};
#endif /*struct_emxArray_int32_T_16382*/
#ifndef typedef_emxArray_int32_T_16382
#define typedef_emxArray_int32_T_16382
typedef struct emxArray_int32_T_16382 emxArray_int32_T_16382;
#endif /*typedef_emxArray_int32_T_16382*/
#ifndef struct_emxArray_int32_T_16383
#define struct_emxArray_int32_T_16383
struct emxArray_int32_T_16383
{
    int data[16383];
    int size[1];
};
#endif /*struct_emxArray_int32_T_16383*/
#ifndef typedef_emxArray_int32_T_16383
#define typedef_emxArray_int32_T_16383
typedef struct emxArray_int32_T_16383 emxArray_int32_T_16383;
#endif /*typedef_emxArray_int32_T_16383*/
#ifndef struct_emxArray_int32_T_8192
#define struct_emxArray_int32_T_8192
struct emxArray_int32_T_8192
{
    int data[8192];
    int size[1];
};
#endif /*struct_emxArray_int32_T_8192*/
#ifndef typedef_emxArray_int32_T_8192
#define typedef_emxArray_int32_T_8192
typedef struct emxArray_int32_T_8192 emxArray_int32_T_8192;
#endif /*typedef_emxArray_int32_T_8192*/
#ifndef struct_emxArray_real32_T
#define struct_emxArray_real32_T
struct emxArray_real32_T
{
    float *data;
    int *size;
    int allocatedSize;
    int numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray_real32_T*/
#ifndef typedef_emxArray_real32_T
#define typedef_emxArray_real32_T
typedef struct emxArray_real32_T emxArray_real32_T;
#endif /*typedef_emxArray_real32_T*/
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T
struct emxArray_real_T
{
    double *data;
    int *size;
    int allocatedSize;
    int numDimensions;
    boolean_T canFreeData;
};
#endif /*struct_emxArray_real_T*/
#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T
typedef struct emxArray_real_T emxArray_real_T;
#endif /*typedef_emxArray_real_T*/

#endif
/* End of code generation (demodulate_roseline_types.h) */
