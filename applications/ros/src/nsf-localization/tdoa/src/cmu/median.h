/*
 * median.h
 *
 * Code generation for function 'median'
 *
 */

#ifndef __MEDIAN_H__
#define __MEDIAN_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern float median(const emxArray_real32_T *x);

#endif

/* End of code generation (median.h) */
