/*
 * extract_envelope.h
 *
 * Code generation for function 'extract_envelope'
 *
 */

#ifndef __EXTRACT_ENVELOPE_H__
#define __EXTRACT_ENVELOPE_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void extract_envelope(const float xr[9601], creal32_T x[16384]);

#endif

/* End of code generation (extract_envelope.h) */
