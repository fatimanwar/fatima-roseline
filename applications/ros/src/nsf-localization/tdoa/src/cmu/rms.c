/*
 * rms.c
 *
 * Code generation for function 'rms'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "rms.h"

/* Function Definitions */
float rms(const float x[4801])
{
  float y;
  float b_x[4801];
  int i;
  for (i = 0; i < 4801; i++) {
    b_x[i] = x[i] * x[i];
  }

  y = b_x[0];
  for (i = 0; i < 4800; i++) {
    y += b_x[i + 1];
  }

  return (real32_T)sqrt(y / 4801.0F);
}

/* End of code generation (rms.c) */
