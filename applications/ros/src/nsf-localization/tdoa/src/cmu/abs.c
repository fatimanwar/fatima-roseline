/*
 * abs.c
 *
 * Code generation for function 'abs'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "abs.h"
#include "demodulate_roseline_emxutil.h"

/* Function Declarations */
static float rt_hypotf_snf(float u0, float u1);

/* Function Definitions */
static float rt_hypotf_snf(float u0, float u1)
{
  float y;
  float a;
  float b;
  a = (real32_T)fabs(u0);
  b = (real32_T)fabs(u1);
  if (a < b) {
    a /= b;
    y = b * (real32_T)sqrt(a * a + 1.0F);
  } else if (a > b) {
    b /= a;
    y = a * (real32_T)sqrt(b * b + 1.0F);
  } else if (rtIsNaNF(b)) {
    y = b;
  } else {
    y = a * 1.41421354F;
  }

  return y;
}

void b_abs(const emxArray_real32_T *x, emxArray_real32_T *y)
{
  int unnamed_idx_0;
  int i1;
  unnamed_idx_0 = x->size[0];
  i1 = y->size[0];
  y->size[0] = unnamed_idx_0;
  emxEnsureCapacity((emxArray__common *)y, i1, (int)sizeof(float));
  for (unnamed_idx_0 = 0; unnamed_idx_0 < x->size[0]; unnamed_idx_0++) {
    y->data[unnamed_idx_0] = (real32_T)fabs(x->data[unnamed_idx_0]);
  }
}

void c_abs(const creal32_T x[16384], float y[16384])
{
  int k;
  for (k = 0; k < 16384; k++) {
    y[k] = rt_hypotf_snf(x[k].re, x[k].im);
  }
}

/* End of code generation (abs.c) */
