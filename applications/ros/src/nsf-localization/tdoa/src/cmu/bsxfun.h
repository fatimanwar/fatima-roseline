/*
 * bsxfun.h
 *
 * Code generation for function 'bsxfun'
 *
 */

#ifndef __BSXFUN_H__
#define __BSXFUN_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void bsxfun(const creal32_T a[65536], const creal32_T b[65536], creal32_T
                   c[65536]);

#endif

/* End of code generation (bsxfun.h) */
