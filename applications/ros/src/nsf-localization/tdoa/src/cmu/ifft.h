/*
 * ifft.h
 *
 * Code generation for function 'ifft'
 *
 */

#ifndef __IFFT_H__
#define __IFFT_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void b_ifft(const creal32_T x[16384], creal32_T y[16384]);
extern void ifft(const creal32_T x[65536], creal32_T y[65536]);

#endif

/* End of code generation (ifft.h) */
