/*
 * demodulate_roseline_terminate.h
 *
 * Code generation for function 'demodulate_roseline_terminate'
 *
 */

#ifndef __DEMODULATE_ROSELINE_TERMINATE_H__
#define __DEMODULATE_ROSELINE_TERMINATE_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void demodulate_roseline_terminate(void);

#endif

/* End of code generation (demodulate_roseline_terminate.h) */
