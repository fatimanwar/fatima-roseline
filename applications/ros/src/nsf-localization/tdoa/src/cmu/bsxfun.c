/*
 * bsxfun.c
 *
 * Code generation for function 'bsxfun'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "demodulate_roseline.h"
#include "bsxfun.h"

/* Function Definitions */
void bsxfun(const creal32_T a[65536], const creal32_T b[65536], creal32_T c
            [65536])
{
  static creal32_T av[65536];
  static creal32_T bv[65536];
  int k;
  float av_re;
  float av_im;
  for (k = 0; k < 65536; k++) {
    av[k] = a[k];
    bv[k] = b[k];
    av_re = av[k].re;
    av_im = av[k].im;
    av[k].re = av[k].re * bv[k].re - av[k].im * bv[k].im;
    av[k].im = av_re * bv[k].im + av_im * bv[k].re;
    c[k] = av[k];
  }
}

/* End of code generation (bsxfun.c) */
