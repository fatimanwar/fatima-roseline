/*
 * diff.h
 *
 * Code generation for function 'diff'
 *
 */

#ifndef __DIFF_H__
#define __DIFF_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "demodulate_roseline_types.h"

/* Function Declarations */
extern void b_diff(const float x[3], float y[2]);
extern void diff(const float x[16384], float y[16383]);

#endif

/* End of code generation (diff.h) */
