#ifndef AUDIO_PROC_H
#define AUDIO_PROC_H

// Includes
#include <math.h>
#include <stdio.h>

// Definitions
#define NZEROS 4
#define NPOLES 4
#define GAIN   6.941574299e+01

typedef enum FILTER_DIR{
	FDIR_FORWARD,
	FDIR_REVERSE
} filterDir_t;

// Function Definitions
void zp_bandpass_filter(float *input, float *output, int len);
void bp_butterworth(float *input, float *output, int len, filterDir_t dir);

#endif
