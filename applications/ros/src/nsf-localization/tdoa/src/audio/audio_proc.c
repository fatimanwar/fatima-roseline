#include "audio_proc.h"

// filtering poles and zeros
static float xv[NZEROS+1], yv[NPOLES+1];

// Zero phase band pass filter
void zp_bandpass_filter(float *input, float *output, int len)
{
	// temp array
	float temp[len];

	// BP Forward
	bp_butterworth(input, temp, len, FDIR_FORWARD);

	// BP Reverse
	bp_butterworth(temp, output, len, FDIR_REVERSE);
}

void bp_butterworth(float *input, float *output, int len, filterDir_t dir)
{
	int i;
	if( dir == FDIR_FORWARD )
	{

	  	for (i=0; i<len; i++)
	     	{ 
		  	xv[0] = xv[1]; xv[1] = xv[2]; xv[2] = xv[3]; xv[3] = xv[4]; 
			xv[4] = (float) input[i] / GAIN;
			yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4]; 
			yv[4] =   (xv[0] + xv[4]) - 2 * xv[2]
			         + ( -0.6905989233 * yv[0]) + ( -2.8087788724 * yv[1])
			         + ( -4.5190260481 * yv[2]) + ( -3.3854106817 * yv[3]);
			output[i] = (float) yv[4];
		}
	}
	else
	{
		for (i=(len-1); i>0; i--)
	     	{
		  	xv[0] = xv[1]; xv[1] = xv[2]; xv[2] = xv[3]; xv[3] = xv[4]; 
			xv[4] = (float) input[i] / GAIN;
			yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4]; 
			yv[4] =   (xv[0] + xv[4]) - 2 * xv[2]
			         + ( -0.6905989233 * yv[0]) + ( -2.8087788724 * yv[1])
			         + ( -4.5190260481 * yv[2]) + ( -3.3854106817 * yv[3]);
			output[i] = (float) yv[4]; 
		}
	}
}