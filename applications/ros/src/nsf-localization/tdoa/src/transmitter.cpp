// ROS includes
#include <ros/ros.h>
#include <tdoa/Trigger.h>

// Include the BeagleBone Black IO library
#include "bbbio/BBBiolib.h"

tdoa::Trigger  msg_tri;
ros::Publisher pub_tri;

void tx_start(const ros::TimerEvent&)
{
	ROS_INFO("Broadcasting ultrasonic chirp on P9.23...");

	// Send the trigger
	msg_tri.count++;
	pub_tri.publish(msg_tri);

   	// Sleep for approximately 100ms
   	ros::Duration(0.15).sleep();

	// Turn on
   	BBBIO_GPIO_low(BBBIO_GPIO1, BBBIO_GPIO_PIN_17);
   	
   	// Sleep for approximately 100ms
   	ros::Duration(0.01).sleep();

   	// Turn off
	BBBIO_GPIO_high(BBBIO_GPIO1, BBBIO_GPIO_PIN_17);
}

// Main entry point of application
int main( int argc, char** argv)
{
	// Initialise ROS
	ros::init(argc, argv, "transmitter");
	ros::NodeHandle n("~");
	pub_tri = n.advertise<tdoa::Trigger>("/trigger", 1000);

	// Enable fast IO
	iolib_init();
	BBBIO_sys_Enable_GPIO(BBBIO_GPIO1);
	BBBIO_GPIO_set_dir(
		BBBIO_GPIO1, 		// bank
		0x0, 			// input
		BBBIO_GPIO_PIN_17	// output
	);

	// Turn off transmitter by default
	BBBIO_GPIO_high(BBBIO_GPIO1, BBBIO_GPIO_PIN_17);

	// Creat a timer to call abck every second
	double trigger = 5.0;
	n.getParam("trigger", trigger);
 	ros::Timer timer = n.createTimer(ros::Duration(trigger), tx_start);

 	// Keep going until shutdown
	ros::spin();

	// Clean up
	iolib_free();

	// Make for great success
	return 0;
}
