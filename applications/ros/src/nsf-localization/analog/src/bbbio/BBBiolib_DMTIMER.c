#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sched.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include "BBBiolib.h"
#include "BBBiolib_DMTIMER.h"
#include <unistd.h>
#include <signal.h>

/* Device register mamory map */
#define DMTIMER_MMAP_ADDR 0x48044000
#define DMTIMER_MMAP_LEN  0x1000

// The various timers
#define DMTIMER_OFFSET_TIMER4 0x4000
#define DMTIMER_OFFSET_TIMER5 0x6000
#define DMTIMER_OFFSET_TIMER6 0x8000
#define DMTIMER_OFFSET_TIMER7 0xA000

// Memory address
extern int memh;
volatile unsigned int *dmtimer_ptr = NULL;

// INitialize the DMTimer subsystem
int BBBIO_DMTIMER_Init()
{
	// Check that we have a file descriptor to memory
	if (memh == 0)
	{
#ifdef BBBIO_LIB_DBG
		fprintf(stderr, "BBBIO_DMTIMER_Init : memory not mapped?\n");
#endif
		return 0;
	}

	// Try and start the memory map
	dmtimer_ptr = mmap(0, DMTIMER_MMAP_LEN, PROT_READ | PROT_WRITE, MAP_SHARED, memh, DMTIMER_MMAP_ADDR);
	if(dmtimer_ptr == MAP_FAILED)
	{
#ifdef BBBIO_LIB_DBG
		fprintf(stderr, "BBBIO_DMTIMER_Init: DMTIMER mmap failure!\n");
#endif
		return 0;
	}
}

// Fetch the current local clock count
uint32_t BBBIO_DMTIMER_Work(int timer)
{
	switch(timer)
	{
		case BBBIO_DMTIMER_TIMER4: return (uint32_t)dmtimer_ptr[0x3c/4];
		case BBBIO_DMTIMER_TIMER5: return (uint32_t)dmtimer_ptr[0x3c/4];
		case BBBIO_DMTIMER_TIMER6: return (uint32_t)dmtimer_ptr[0x3c/4];
		case BBBIO_DMTIMER_TIMER7: return (uint32_t)dmtimer_ptr[0x3c/4];
	}
}