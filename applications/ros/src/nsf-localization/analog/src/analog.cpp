// Standard includes
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdint.h>
#include <signal.h>
#include <stdlib.h>

// ROS includes
#include <ros/ros.h>
#include <analog/Measurement.h>

// C++ includes
#include <iostream>

// For thread fucntionality
#include <boost/thread.hpp>

// Include the BeagleBone Black IO library
#include "bbbio/BBBiolib.h"

// Include the ROSELINE ioctl library
#include "../../../../timesync/module/roseline_ioctl.h"

// Get the buffer size for the audio
#define AUDIO_BUFFER_SIZE  2880
#define AUDIO_BUFFER_DEBUG true

// Flags to signal threads
bool flag = true;

// Signal to thread that new data is available
bool newdata[3] = {false,false,false};

// For ROS messaging
ros::Publisher  	pub_mea;	// For debuggin the audio
analog::Measurement 	msg_mea;	// Signal message

// Double-buffer the audio signals
unsigned int *buffer[3][2];
uint32_t tbuffer = 0;
uint32_t obuffer = 0;

// ADC: thread work
void worker_adc(int chan, int open_dly, int sample_dly, unsigned int** buffer, int buff_sz, bool* sig)
{
	ROS_INFO("Starting ADC worker thread");

	// Setup the buffer
	BBBIO_ADCTSC_channel_ctrl(chan, BBBIO_ADC_STEP_MODE_SW_CONTINUOUS, 
		open_dly, sample_dly, BBBIO_ADC_STEP_AVG_1, buffer[0], buff_sz);

	// Keep going until we are signalled to stop
	while (flag)
	{
		// Enable AIN2 and capture into the buffer
		BBBIO_ADCTSC_channel_enable(chan);
		
		// Capture the audio (blocking function for sample time -- 1s)
		BBBIO_ADCTSC_work(buff_sz);

		// Buffer copy and signal filter
		memcpy(buffer[1],buffer[0],sizeof(unsigned int)*buff_sz);
		
		// New data is available for this channel!
		*sig = true;
	}
}

// This just finds the first value greater than some threshold amount
void worker_fil(int buff_sz, unsigned int*** buffer)
{
	ROS_INFO("Starting MEA worker thread");

	// Some code to monitor the 
	while (flag)
	{
		// Thread syncronization
		if (newdata[0] && newdata[1] && newdata[2])
		{
			// Publish measurement 1-by-1
			for (int i = 0; i < buff_sz; i++)
			{
				// Copy the data
				switch(i)
				{
					case 0: msg_mea.phase1 = std::vector<unsigned int>(&buffer[i][1],&buffer[i][1][buff_sz-1]); break;
					case 1: msg_mea.phase2 = std::vector<unsigned int>(&buffer[i][1],&buffer[i][1][buff_sz-1]); break;
					case 2: msg_mea.phase3 = std::vector<unsigned int>(&buffer[i][1],&buffer[i][1][buff_sz-1]); break;
				}
			
				// Reset flag
				newdata[i] = false;	
			}

			// Publish the message
			pub_mea.publish(msg_mea);	
			
		}

		// Sleep for a little bit between newdata checks
		boost::this_thread::sleep(boost::posix_time::milliseconds(100));
	}
}

// Channel mappign from int -> BBBIO enum
int chanmap(int chan)
{
	switch(chan)
	{
		case 0 :  chan = BBBIO_ADC_AIN0; break;
		case 1 :  chan = BBBIO_ADC_AIN1; break;
		case 2 :  chan = BBBIO_ADC_AIN2; break;
		case 3 :  chan = BBBIO_ADC_AIN3; break;
		case 4 :  chan = BBBIO_ADC_AIN4; break;
		case 5 :  chan = BBBIO_ADC_AIN5; break;
		case 6 :  chan = BBBIO_ADC_AIN6; break;
		default: 
			ROS_ERROR("Unknown ADC channel");
			break;
	}
	return chan;
}

// Main entry point of application
int main( int argc, char** argv)
{
	// Initialise ROS
	ros::init(argc, argv, "analog");
	ros::NodeHandle n("~");
	pub_mea = n.advertise<analog::Measurement>("analog", 1000);

	// Basic parameters for the system
	int clk_div    = 34;
	int open_dly   = 1;
	int sample_dly = 1;
	int chan1      = 4;
	int chan2      = 5;
	int chan3      = 6;
	int buff_sz    = 2882;

	// Extract configuration parameters for the ADC
	n.getParam("buffer_sz", buff_sz);		// ADC only : clock divider
	n.getParam("clk_div", clk_div);			// ADC only : clock divider
	n.getParam("open_dly", open_dly);		// ADC only : open delay
	n.getParam("sample_dly", sample_dly);		// ADC only : sample delay
	
	// Channel mappings
	n.getParam("phase1_channel", chan1);		// Anchor position X
	n.getParam("phase2_channel", chan2);		// Anchor position X
	n.getParam("phase3_channel", chan3);		// Anchor position X

	// Initialise the BBBIO library
	iolib_init();

 	// Set the ADC to sample at 40kHz with 30Hz tolerance
	BBBIO_ADCTSC_module_ctrl(clk_div);

	// Allocate buffers
	for (int i = 0; i < 3; i++)
	{
		buffer[i][0] = new unsigned int[buff_sz];
		buffer[i][1] = new unsigned int[buff_sz];
	}

	// Start the worker threads for collecting ADC values
    	boost::thread phase1_thread(boost::bind(worker_adc,chan1,open_dly,sample_dly,buff_sz,buffer[0]));
    	boost::thread phase2_thread(boost::bind(worker_adc,chan2,open_dly,sample_dly,buff_sz,buffer[1]));
    	boost::thread phase3_thread(boost::bind(worker_adc,chan3,open_dly,sample_dly,buff_sz,buffer[2]));

    	// Collect and transmit these values to host
    	boost::thread filter_thread(boost::bind(worker_fil,buff_sz,buffer));
	
	// Keep spinning until ROS is shut down (the ADC sampling will run int the background)
	ros::spin();

	// Signal threads to end
	flag = false;
	phase1_thread.join();	// Phase 1
	phase2_thread.join();	// Phase 2
	phase3_thread.join();	// Phase 3
	filter_thread.join();	// Filtering

	// Clear up BBIOlib
	iolib_free();

	// Clear buffers
	for (int i = 0; i < 3; i++)
	{
		delete[] buffer[i][0];
		delete[] buffer[i][1];
	}

	// Make for great success
	return 0;
}
