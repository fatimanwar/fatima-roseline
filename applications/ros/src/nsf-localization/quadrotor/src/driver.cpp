#include <ros/ros.h>

//////////////////////////////////////////////////////////////////////////////////////////

#include <tdoa/Sensor.h>	// TDOA localization solution (target waypoint)
#include <quadrotor/State.h>	// Quadrotor state
#include <quadrotor/Control.h>	// Quadrotor control
#include <quadrotor/Goal.h>	// Quadrotor goal
#include <quadrotor/Config.h>	// Quadrotor goal

// Configuration parameters

double x_min, x_max, y_min, y_max, z_min, z_max;			// General configuration
double _rate, _maxtilt, _maxv, _th_hover, _Kpz, _Kiz, _Kdz, _Kxy, _Kv;	// PID controller parameters
std::string name;							// ID of device

//////////////////////////////////////////////////////////////////////////////////////////

// Internal PID controller variables
quadrotor::Control control;	// Control to send to the FCS
quadrotor::State   state;	// Current state of the quadrotor
bool   enabled = false;		// Is control enabled?
bool   reach = false;		// Has the goal been reached?
double thresh = 0.01;		// Threshold distance considered to be at waypoint
double sp_x, sp_y, sp_z;	// Set point (target waypoint in TDO frame)
double ez, iz;			// Throttle I and D

// Rotate a vector from the navigation to body frame using RPY convention
void n2b(double rot[3], double vec[3])
{
	double tmp[3];
	for (int i = 0; i < 3; i++)
		tmp[i] = vec[i];
	double sph = sin(rot[0]); double cph = cos(rot[0]);
	double sth = sin(rot[1]); double cth = cos(rot[1]); 
	double sps = sin(rot[2]); double cps = cos(rot[2]);
	double dcm[3][3];
	dcm[0][0] = cth*cps;
	dcm[0][1] = cth*sps;
	dcm[0][2] = -sth;
	dcm[1][0] = sph*sth*cps - cph*sps;
	dcm[1][1] = sph*sth*sps + cph*cps;
	dcm[1][2] = sph*cth;
	dcm[2][0] = cph*sth*cps + sph*sps;
	dcm[2][1] = cph*sth*sps - sph*cps;
	dcm[2][2] = cph*cth;
	for (int i = 0; i < 3; i++)
	{   
		vec[i] = 0;
		for (int j = 0; j < 3; j++)
			vec[i] += dcm[i][j] * tmp[j];
	}
}

// Limit some value val to lie in the range [minval,maxval]
double limit(const double& val, const double& minval, const double& maxval)
{
	if (val < minval) return minval;
	if (val > maxval) return maxval;
	return val;
}

//////////////////////////////////////////////////////////////////////////////////////////

// Callback when new pose arrives
void cb_sens(const tdoa::Sensor::ConstPtr& msg)
{
	// Do some sanity checks on the incloming control
	if (msg->name.data == name)
	{
		state.x = msg->pose.position.x;
		state.y = msg->pose.position.y;
		state.z = msg->pose.position.z;
		ROS_INFO("Accepted new truthful sensor position :: x: %f, y: %f, z: %f ",sp_x,sp_y,sp_z);
	}
}

// Callback when new pose arrives
bool cb_conf(quadrotor::Config::Request &req, quadrotor::Config::Response &res)
{
	// Do some sanity checks on the incloming control
	enabled = req.enable;
	res.success = true;
	if (enabled)
	{
		res.status  = "Controller enabled";
		ROS_INFO("Enabled controller");
	}
	else
	{
		res.status  = "Controller disabled";
		ROS_INFO("Disabled controller");
	}
	return true;
}

// Callback when new pose arrives
bool cb_goal(quadrotor::Goal::Request &req, quadrotor::Goal::Response &res)
{
	// Do some sanity checks on the incloming control
	if (		(req.x > x_min && req.x < x_max)
		&&	(req.y > y_min && req.y < y_max)
		&& 	(req.z > z_min && req.z < z_max) )
	{
		reach = false;
		sp_x = req.x;
		sp_y = req.y;
		sp_z = req.z;
		res.success = true;
		res.status  = "Accepted new goal";
		ROS_INFO("Accepted new PID goal :: x: %f, y: %f, z: %f ",sp_x, sp_y,sp_z);
	}
	else
	{
		res.success = true;
		res.status  = "Operation not allowed: out of bounds";
		ROS_INFO("Rejected new PID goal :: x: %f, y: %f, z: %f ",sp_x, sp_y,sp_z);
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////

// This is the main body of the PID controller to maintain
void pid(const ros::TimerEvent& event)
{
	//////////////////////////// SAFETY CHECK ///////////////////////////

	if (!enabled) return;

	//////////////////////// CHECK IF GOAL REACHED //////////////////////

	// Distance to desired waypoint
	double dst = sqrt(
		    (sp_x-state.x)*(sp_x-state.x) 
		+   (sp_y-state.y)*(sp_y-state.y)
		+   (sp_z-state.z)*(sp_z-state.z)
	);

	// 
	if (!reach && dst < thresh)
		reach = true;

	//////////////////////////////////////////////////////////////////////

	double dt = 1.0 / _rate;

	// P PITCH AND ROLL CONTROLLERS //////////////////////////////////////

	// Obtain body-frame velocities
	double vel[3] = {sp_x-state.x, sp_y-state.y, sp_z-state.z};
	double rot[3] = {state.roll, state.pitch, state.yaw};
	n2b(rot,vel);
	double bx = vel[0];
	double by = vel[1];

	// Roll controller
	double desv = limit(_Kxy*by,-_maxv,_maxv);
	double desR = limit(-_Kv*(desv - state.v), -_maxtilt, _maxtilt);

	// Pitch controller
	double desu = limit(_Kxy*bx,-_maxv,_maxv);
	double desP = limit( _Kv*(desu - state.u), -_maxtilt, _maxtilt);

	/////////////////// PID THROTTLE CONTROLLER //////////////////////

	// Get the (P)roportional component (Changed by Andrew)
	double ez_ = sp_z - state.z;

	// Get the (I)ntegral component
	iz += ez_ * dt;

	// Get the (D)erivative component
	double de_ = (dt > 0 ? (ez_ - ez) / dt : 0.0);
	double desth = _th_hover + _Kpz * ez_ + _Kiz * iz + de_ * _Kdz;
	double th = limit(desth,0,1);

	// Save (P) contribution for next derivative calculation
	ez = ez_;

	// Save (I) contribution for next derivative calculation
	iz = iz - (desth - th) * 2.0;

	//////////////////////// CONTROL PACKAGING /////////////////////////

	// This will be returned
	control.roll     = desR;
	control.pitch    = desP;
	control.throttle = th;
	ROS_INFO("Set control vector :: r: %f, p: %f, th: %f ",desR, desP, th);
}

// Main entry point of ros node
int main(int argc, char **argv)
{
	// ROS initialization
	ros::init(argc, argv, "driver");
	ros::NodeHandle n("~");

	// Quadorotor name (used to get position)
	n.getParam("name",  name);

	// Safe bounds
	n.getParam("x_min", 	x_min);
	n.getParam("x_max", 	x_max);
	n.getParam("y_min", 	y_min);
	n.getParam("y_max", 	y_max);
	n.getParam("z_min", 	z_min);
	n.getParam("z_max", 	z_max);
	
	// PID parameters
	n.getParam("rate", 	_rate);
	n.getParam("maxtilt", 	_maxtilt);
	n.getParam("maxv", 	_maxv);
	n.getParam("th_hover",  _th_hover);
	n.getParam("Kpz", 	_Kpz);
	n.getParam("Kiz", 	_Kiz);
	n.getParam("Kdz",	_Kdz);
	n.getParam("Kxy",	_Kxy);
	n.getParam("Kv", 	_Kv);

	// Get sensor poses arriving from the external localization system
	ros::Subscriber    sub_s = n.subscribe<tdoa::Sensor>("/sensor_pose", 1000, cb_sens);	// TDOA position
	
	// Allow a user to configure the quadrotor and set the goal point
	ros::ServiceServer srv_c = n.advertiseService("configure", cb_conf);			// Configuration
	ros::ServiceServer srv_g = n.advertiseService("setgoal", cb_goal);			// Goal

	// Immediately start control loop
	ros::Timer ctrl = n.createTimer(ros::Duration(1.0/_rate),pid);

	// Idle loop
	ros::spin();

	// Success
	return 0;
}
