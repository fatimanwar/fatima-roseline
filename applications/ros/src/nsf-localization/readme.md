# Installation instructions #

1. Install Ubuntu 14.04
2. Install ROS Indigo
3. Install ros-indigo-rviz
4. Checkout roseline code
5. Change to roseline/applications/nsf-localization/src
6. catkin_init_workspace
7. Change to roseline/applications/nsf-localization
8. catkin_make

# General information #

controller - a simple Velocity PID controller for holding a position
interface  - the user interface for monitoring system performance
tdoa       - performs time difference of arrival localization
timesync   - performs time synchronisation

The principle of operation is the following. There are 15 nodes 
connected via an ethernet backbone to the base station. All commands
originate from the backbone controller.

The nodes boot off the microsd card. The microsd card contains a U-boot
bootloader that looks for a kernel image and device tree over TFTP, and 
a lightweight debian rootfs. The following locations are NFS-mounted:

- NFS mount /var/lib/modules       
- NFS mount /home/roseline 

Included in the /home/roseline folder is a cross-compiled version of all
user-space applications (ROS code, timesync code). Likewise, cross-compiled
versions of the kernel modules are in /var/lib/modules. This architecture
allows us to update the kernel, device tree, modules and applications 
without needing to copy them one-by-one to the devices.

The experiment is launched from the base station in the following way:

	> roslaunch interface gui.launch

This has the effect of launching the following nodes
	
	> roscore    : the core distributed ros messaging system 
	> rviz       : a visualization tool to plot experiemnt information
	> tdoa       : a tool to collect TDOA measurements and localize
	> interface  : converts sensor and anchor positions to rviz markers
	> controller : a PID controller to stabilize a quadrotor

A Go routine running on each of the anchors will collect measurements --
precise arrival times of an ultrasonic waveform. These will be forwarded
over Ethernet using standard sockets to the tdoa ROS node running on a 
central controller. This ROS node will calculate a localization solution
and broadcast it as a PoseStamped event.


