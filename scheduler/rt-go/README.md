================

## RT GO ARM

### Introduction

In RT GO, you can set goroutines as RT G, which can only run on RT thread (M) and RT processor (P). 
Each RT G gets a unique runnable queue ID in P (qid), it can be directly switched up by calling Goswitch(qid).
Based on Goswitch(), priority based scheduling is implemented.

### API
Run src/.all.bash to build.
The additional functions of runtime package are:

Calling SetRT() before 'go func()' will create a new RT goroutine.
qid should be unique for each RT goroutine, mid is which OS thread it is running on. pid is to select a P, for now only pid=0 is supported.
isnewm=true will create a new OS thread.

	func SetRT(qid int32, mid int32, pid int32, isnewm bool)

Call system call sched_setscheduler/sched_getscheduer to make OS thread a RT one.

	func Setsched(tid uint32, policy uint32, priority int32) int32 / func Getsched(tid uint32) int32

Switch from current goroutine to goroutine 'qid'

	func Goswitch(qid uint32)


## Earilest Deadline First Scheduler

###API
Run src/build.sh to build.
To create and schedule a task, it needs to

Create an OS thread (M), waiting() is waiting for event to happen: 

	SetRT(int32(0), int32(0), 0, true)
	go waiting(0, 0)
	

Create a time specification, S: starting time, P: period (P=0 is one-shot) and D: deadline

    var timesp TimeSpec = TimeSpec{S:6*second, P:10*second, D:10*second}
	

Create a task, assign a qid and initilize the deadline

	qid = int32(1)
	task = Task{Timespec:timesp, Taskid:0, Qid:int(qid)}
	task.Ddl = nowTime.Add(time.Duration(task.Timespec.D))
	

Create RT goroutine and run it. taskfn() is the function body of the task

	SetRT(qid, 0, 0, false)
	go taskfn()
	
In taskfn() body, the programmer must be responsible to check timer according to timing requirement and check asynchronous events by:
	
	libtimer.Sync()
	mid = runtime.Getmid()
	if CompareAndSwapInt32(&libtask.SigAsync[mid], 1, 0) { libtask.RunnableCheck_async() }
	
and after the task ending, expicilty yield to other tasks:

	YieldTask()
