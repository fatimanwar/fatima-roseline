package main

import (
	. "fmt"
	"time"
	lt "libtimer"
	lc "libcgo"
	. "runtime"
	"unsafe"
	. "sync/atomic"	
)

var qidIdle1, qidIdle2, qid1, qid2, qid3, qid4 int32 = 0, 1, 2, 3, 4, 5
var second int = 1000000000
var mod int = 30000000

type TimeSpec struct {
	s, p, d int   // starting time/ period/ relative deadline
}

type Task struct {
	timespec TimeSpec
	taskid, mid, qid int
	ddl time.Time // absolute deadline, used for EDF
	next *Task

	// successors are not switched on by 'Timer'/'Scheduler'
	// A task switched on successors when it is done
	succHead *Task // a list of successor tasks
	succTail *Task

	isIdle bool
}

type Worker struct {
	ddl time.Time // absolute deadline, used for EDF
	mid int
	prev *Worker
	next *Worker
}

var runnableHead *Task = nil // head of runnable task list
var taskCurr [2] *Task
var workerHead *Worker  // a sorted (ddl) list of all threads

var worker0 = Worker{mid:0}
var worker1 = Worker{mid:1}

var workerAddr = [...] *Worker{&worker0, &worker1} // statically store worker pointers

// var parallel = 1
// signal = 1 : asynchronous task switch
var asyncSig [2] int32 // asynchronous signal between M
var sigAllSet = [...] int32{1, 1}
var lockRunq int32
var lockWorker int32

// tasks
var timespIdle TimeSpec = TimeSpec{s:0*second, p:0*second, d:1000*second}
var taskIdle = [...] Task{ Task{timespec:timespIdle, taskid:-1, qid:int(qidIdle1), isIdle:true}, 
	                   Task{timespec:timespIdle, taskid:-2, qid:int(qidIdle2), isIdle:true} }

var timesp1 TimeSpec = TimeSpec{s:3*second, p:10*second, d:6*second} 
var timesp2 TimeSpec = TimeSpec{s:6*second, p:16*second, d:20*second} 

var task1 Task = Task{timespec:timesp1, taskid:1, qid:int(qid1)}
var task2 Task = Task{timespec:timesp2, taskid:2, qid:int(qid2)}

var task3 Task = Task{timespec:timesp1, taskid:3, qid:int(qid3)}
var task4 Task = Task{timespec:timesp1, taskid:4, qid:int(qid4)}

func main() {
	// initialize ddl
	nowTime := time.Now()
	taskIdle[0].ddl = nowTime.Add(time.Duration(taskIdle[0].timespec.d))
	taskIdle[1].ddl = nowTime.Add(time.Duration(taskIdle[1].timespec.d))

	task1.ddl = nowTime.Add(time.Duration(task1.timespec.d))
	task2.ddl = nowTime.Add(time.Duration(task2.timespec.d))

	// DAG
	task1.succHead = &task3
	task1.succTail = &task4
	task3.next = &task4

	// worker
	workerHead = &worker0
	worker0.next = &worker1
	worker1.prev = &worker0

	// SetRT(qid, mid, pid, isnewm)
	SetRT(qidIdle1, 0, 0, true)
	go waiting(0, 0)

	SetRT(qidIdle2, 1, 0, true)
	go waiting(1, 1)

	SetRT(qid1, 0, 0, false)
	go task(1)

	SetRT(qid2, 0, 0, false)
	go task(2)

	SetRT(qid3, 0, 0, false)
	go task(3)

	SetRT(qid4, 0, 0, false)
	go task(4)

	// get tid
	tid := int( Gettid() )
        Printf("Main: tid = %d\n", tid)

	for i := 0; i < 10*mod; i++ {}
	//go setTimer()
	schedule(&task1)
	schedule(&task2)

	for {	}

/*
	for {}
        Println("exit main")
*/
}

func schedule(task *Task) {
	f := taskSwitch // timeout func handler
	//f := testPrint
	arg := unsafe.Pointer(task)
	period := time.Duration(task.timespec.p)

	TimerNode := lt.Create()

	lt.Set(TimerNode, time.Now().Add(time.Duration(task.timespec.s)), period, f, arg, true) // start timer	
}

// for timer
func taskSwitch(arg unsafe.Pointer) {
	waitTask := (* Task) (arg)
	mid := Getmid()

	//Printf("wait ID: %d ; curr ID: %d  time-out: %s", waitTask.qid, taskCurr[0].qid, time.Now().String())

	// do not switch to itself
	if waitTask.taskid == taskCurr[mid].taskid { return }

	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
	runnablePut(waitTask)
	StoreInt32(&lockRunq, 0)

	if waitTask.ddl.Before(workerHead.ddl) { // compare with the latest ddl
		asyncSig = sigAllSet  // inform all workers to check
	}

/*
	if waitTask.ddl.Before(taskCurr[mid].ddl) {
		//Println("\nSwitch:  " + time.Now().String())
		//Println("ddl curr: " + taskCurr[mid].ddl.String())
		//Println("ddl wait: " + waitTask.ddl.String())	
		runnablePut(taskCurr[mid])
		StoreInt32(&lockRunq, 0)

		Printf("switching mid %d qid %d\n", mid, taskCurr[mid].qid)
		taskCurr[mid] = waitTask
		setWorker (mid, taskCurr[mid].ddl)
		Goswitch(uint32(waitTask.qid))
	} else { // lower priority task
		runnablePut(waitTask)
		StoreInt32(&lockRunq, 0)
	}
*/
}

// need to consider synchronization of runnable queue

// check if there is runnable task
func runnableCheck() bool {
	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
	if runnableHead == nil { 
		//Printf("runnable queue is empty\n")
		StoreInt32(&lockRunq, 0)
		return false
	} else {
		task := runnableHead
		runnableHead = runnableHead.next // remove
		StoreInt32(&lockRunq, 0)

		mid := Getmid()
		taskCurr[mid] = task
		setWorker (mid, taskCurr[mid].ddl)
		Goswitch(uint32(task.qid))
		return true
	}
}

// check if there is runnable task
func runnableCheck_async() bool {
	mid := Getmid()
	if runnableHead == nil || !runnableHead.ddl.Before(taskCurr[mid].ddl) {
		return false
	} else {
		task := runnableHead
		for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
		runnableHead = runnableHead.next // remove
		runnablePut(taskCurr[mid])
		StoreInt32(&lockRunq, 0)

		taskCurr[mid] = task
		setWorker (mid, taskCurr[mid].ddl)
		Goswitch(uint32(task.qid))
		return true
	} 
}

// put runnable task into queue
func runnablePut(task *Task) {
	if task.isIdle == true { return } // do not put 'idle' (background) task into runnable queue

	task.next = nil	
	if runnableHead == nil {
		runnableHead = task
	} else if task.ddl.Before(runnableHead.ddl) {
		task.next = runnableHead
		runnableHead = task
	} else {
		for taskCur := runnableHead; ; taskCur = taskCur.next {
			if taskCur.next == nil {
				taskCur.next = task
				return
			}		

			if task.ddl.Before(taskCur.next.ddl) {
				task.next = taskCur.next
				taskCur.next = task
				return
			}	
		}
	}
}

// put successor tasks into runnable queue
// successors have the earilest ddl
func succPut(task *Task) {
	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
	head := runnableHead
	runnableHead = task.succHead
	task.succTail.next = head
	StoreInt32(&lockRunq, 0)
}

// reset ddl of workers
func setWorker (mid uint32, ddl time.Time) {
	worker := workerAddr[mid]
	worker.ddl = ddl
	for ; !CompareAndSwapInt32(&lockWorker, 0, 1) ; {}
	// deletion
	if worker.prev == nil {
		workerHead = worker.next
		if worker.next != nil { worker.next.prev = nil }
	} else {
		worker.prev.next = worker.next
		if worker.next != nil { worker.next.prev = worker.prev }
	}
	// insertion
	if worker.ddl.After(workerHead.ddl) {
		worker.next = workerHead
		workerHead.prev = worker
		worker.prev = nil
		workerHead = worker
		StoreInt32(&lockWorker, 0)
	} else {
		for workerCur := workerHead; ; workerCur = workerCur.next {
			if workerCur.next == nil {
				workerCur.next = worker
				worker.prev = workerCur
				worker.next = nil
				StoreInt32(&lockWorker, 0)
				return
			}
			if worker.ddl.After(workerCur.next.ddl) {
				worker.next = workerCur.next
				worker.prev = workerCur
				workerCur.next.prev = worker
				workerCur.next = worker
				StoreInt32(&lockWorker, 0)
				return
			}
		}
	}
}

func waiting(id int, core int) {
	lc.BindCPU(core)

	for j := 0; j < 10*mod; j++ {}

	mid := Getmid()
	taskCurr[mid] = &taskIdle[mid]
	setWorker (mid, taskCurr[mid].ddl)
        
	// get tid
	tid := int( Gettid() )
        Printf("Waiting: tid = %d\n", tid)

        for i := 0;  ; i++ {
		lt.Sync()
		if CompareAndSwapInt32(&asyncSig[mid], 1, 0) { runnableCheck_async() }

		//for k := 0; k < 10; k ++ {}

		if i% (mod) == 0 {
			mid = Getmid()
			Printf("waiting %d mid %d = %d\n", id, mid, int(i/mod))
			//lc.CheckCPU()
		}
	}
	Println("exit waiting")
}

func task(id int) {
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	//var starttime, endtime time.Time
	var mid uint32
	Printf("ID %d : first\n", id)
	//Println("first Time:   " + time.Now().String() + "\n")

        for i := 0; ; i++ {
		//for k := 0; k < 100; k ++ {}
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if CompareAndSwapInt32(&asyncSig[mid], 1, 0) { runnableCheck_async() } // response to async signal
		// ========= switch in ==========

		if(i % (1*mod) == 0){
		//if time.Now().After(starttime.Add(time.Duration(2*second))) {
			// ====== task body ======
			Printf("test %d = %d\n", id, int(i/mod))
			// ======== end =========

			Printf("ID %d : end\n", id)
			//Println("end Time:   " + time.Now().String() + "\n")

			mid = Getmid()
			// update deadline
			taskCurr[mid].ddl = taskCurr[mid].ddl.Add(time.Duration(taskCurr[mid].timespec.p))

			// turn on successors (successors have ddl earlier than runnable)
			// put successors into runnable list, inform other threads to check
			if taskCurr[mid].succHead != nil {
				succPut(taskCurr[mid])
				asyncSig = sigAllSet
			}

			lc.CheckCPU()

			// ======== switch out (yield) =======
			if runnableCheck() == false { // check runnable task. if jumped, start (not return to 1)
				// no switch, no need for Getmid() here
				taskCurr[mid] = &taskIdle[mid]
				setWorker (mid, taskCurr[mid].ddl)
				Goswitch(uint32(taskCurr[mid].qid))
			}

			// ============ switch in  ===========
			//starttime = time.Now()
			Printf("ID %d start\n ", id)
			//Println("Start Time:   " + starttime.String())
		}
	}

        Println("exit task")
}


func testPrint(arg unsafe.Pointer) {
	//task := (* Task) (arg)
	//mid := Getmid()
	//Printf("ID: %d  mid: %d   ", task.taskid, mid)
	Println("time-out: " + time.Now().String())
}
