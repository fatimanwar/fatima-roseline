package main

import (
	. "fmt"
	"time"
	lt "libtimer"
	. "runtime"
	"unsafe"
	. "sync/atomic"	
)

var qid1, qid2, qid3, qid4, qid5 int32
var second int = 1000000000
var mod int = 30000000

// var parallel = 1
// signal = 1 : asynchronous task switch
var asyncSig = [...] int{0} // asynchronous signal between M

type TimeSpec struct {
	s, p, d int   // starting time/ period/ relative deadline
}

type Task struct {
	timespec TimeSpec
	taskid, mid, qid int
	ddl time.Time // absolute deadline, used for EDF
	next *Task

	// successors are not switched on by 'Timer'/'Scheduler'
	// A task switched on successors when it is done
	succHead *Task // a list of successor tasks
	succTail *Task

	isIdle bool
}

var runnableHead *Task = nil // head of runnable task list

var lockRunq int32 = 0

// task DAG
var timesp1 TimeSpec = TimeSpec{s:0*second, p:0*second, d:1000*second}
var timesp2 TimeSpec = TimeSpec{s:3*second, p:10*second, d:6*second} 
var timesp3 TimeSpec = TimeSpec{s:4*second, p:12*second, d:20*second} 

var task1 Task = Task{timespec:timesp1, taskid:1, mid:0, qid:int(qid1), isIdle:true}

var taskIdle = [...] Task{task1}
var taskCurr = [...] *Task{nil}

var task2 Task = Task{timespec:timesp2, taskid:2, mid:0, qid:int(qid2)}
var task3 Task = Task{timespec:timesp3, taskid:3, mid:0, qid:int(qid3)}

// successor of task2, use timespec 2
var task4 Task = Task{timespec:timesp2, taskid:4, qid:int(qid4)}
var task5 Task = Task{timespec:timesp2, taskid:5, qid:int(qid5)}

func main() {
	qid1, qid2, qid3, qid4, qid5 = 1, 2, 3, 4, 5

	nowTime := time.Now()
	taskIdle[0].ddl = nowTime.Add(time.Duration(taskIdle[0].timespec.d))
	task2.ddl = nowTime.Add(time.Duration(task2.timespec.d))
	task3.ddl = nowTime.Add(time.Duration(task3.timespec.d))

	SetRT(qid1, 0, 0, true)
	go waiting()

	SetRT(qid2, 0, 0, false)
	go task(2)

	SetRT(qid3, 0, 0, false)
	go task(3)

	SetRT(qid4, 0, 0, false)
	go task(4)

	SetRT(qid5, 0, 0, false)
	go task(5)

	taskIdle[0].qid = int(qid1) // task1
	task2.qid = int(qid2)
	task3.qid = int(qid3)
	task4.qid = int(qid4)
	task5.qid = int(qid5)

	// DAG
	task2.succHead = &task4
	task2.succTail = &task5
	task4.next = &task5

	// get tid
	tid := int( Gettid() )
        Printf("Main: tid = %d\n", tid)

	//go setTimer()
	schedule(&task2)
	schedule(&task3)

	for {	}

/*
	for {}
        Println("exit main")
*/
}

func schedule(task *Task) {
	f := taskSwitch // timeout func handler
	//f := testPrint
	arg := unsafe.Pointer(task)
	period := time.Duration(task.timespec.p)

	TimerNode := lt.Create()

	lt.Set(TimerNode, time.Now().Add(time.Duration(task.timespec.s)), period, f, arg, true) // start timer	
}

// for timer
func taskSwitch(arg unsafe.Pointer) {
	waitTask := (* Task) (arg)
	mid := Getmid()

	//Printf("wait ID: %d ; curr ID: %d  time-out: %s", waitTask.qid, taskCurr[0].qid, time.Now().String())

	// do not switch to itself
	if waitTask.taskid == taskCurr[mid].taskid { return }

	if waitTask.ddl.Before(taskCurr[mid].ddl) {
		//Println("\nSwitch:  " + time.Now().String())
		//Println("ddl curr: " + taskCurr[mid].ddl.String())
		//Println("ddl wait: " + waitTask.ddl.String())	
		runnablePut(taskCurr[mid])
		taskCurr[mid] = waitTask
		Goswitch(uint32(waitTask.qid))
	} else { // lower priority task
		getLock(&lockRunq)
		runnablePut(waitTask)
		putLock(&lockRunq)
	}
}

// need to consider synchronization of runnable queue

// check if there is runnable task
func runnableCheck() bool {
	getLock(&lockRunq)
	if runnableHead == nil { 
		//Printf("runnable queue is empty\n")
		putLock(&lockRunq)
		return false
	} else {
		task := runnableHead
		runnableHead = runnableHead.next // remove
		putLock(&lockRunq)

		mid := Getmid()
		taskCurr[mid] = task
		Goswitch(uint32(task.qid))
		return true
	}
}

// check if there is runnable task
func runnableCheck_async() bool {
	mid := Getmid()
	getLock(&lockRunq)
	if runnableHead == nil || !runnableHead.ddl.Before(taskCurr[mid].ddl) {
		putLock(&lockRunq)
		return false
	} else {
		task := runnableHead
		runnableHead = runnableHead.next // remove
		runnablePut(taskCurr[mid])
		putLock(&lockRunq)

		taskCurr[mid] = task
		Goswitch(uint32(task.qid))
		return true
	} 
}

// put runnable task into queue
func runnablePut(task *Task) {
	if task.isIdle == true { return } // do not put 'idle' (background) task into runnable queue

	task.next = nil	
	if runnableHead == nil {
		runnableHead = task
	} else if task.ddl.Before(runnableHead.ddl) {
		task.next = runnableHead
		runnableHead = task
	} else {
		for taskCur := runnableHead; ; taskCur = taskCur.next {
			if taskCur.next == nil {
				taskCur.next = task
				return
			}		

			if task.ddl.Before(taskCur.next.ddl) {
				task.next = taskCur.next
				taskCur.next = task
				return
			}	
		}
	}
}

// put successor tasks into runnable queue
// successors have the earilest ddl
func succPut(task *Task) {
	getLock(&lockRunq)
	head := runnableHead
	runnableHead = task.succHead
	task.succTail.next = head
	putLock(&lockRunq)
}

func getLock(lock *int32) {
	for ; LoadInt32(lock) != 0 ; { } // spinning
	AddInt32(lock, 1) // get lock
}

func putLock(lock *int32) {
	AddInt32(lock, -1) // put lock
}

func waiting() {
	mid := Getmid()
	taskCurr[mid] = &taskIdle[mid]
//	ret := CheckRT()
//	Printf("check 1=%d\n", ret)
        
	// get tid
	tid := int( Gettid() )
        Printf("Waiting: tid = %d\n", tid)

        for i := 0;  ; i++ {
		lt.Sync()
/*
		if(i % (2*mod) == 0){
			//starttime := time.Now()
			Goswitch(uint32(qid2))
			//Println("Start Time: " + starttime.String())
		}
*/
		if i%mod == 0 {
			Printf("waiting = %d\n", int(i/mod))
		}
	}
	Println("exit waiting")
}

func task(id int) {
//	endtime := time.Now()
//	Println("End Time:   " + endtime.String())
//	ret := CheckRT()
//	Printf("check 2=%d\n", ret)

	// get tid
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	var starttime, endtime time.Time
	var mid uint32
	starttime = time.Now()
	Printf("ID %d ", id)
	Println("first Time:   " + starttime.String() + "\n")

        for i := 0; ; i++ {
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if asyncSig[mid] == 1 {
			asyncSig[mid] = 0
			runnableCheck_async() 
		}
		// ========= switch in ==========

		//if(i % (3*mod) == 0){
		if time.Now().After(starttime.Add(time.Duration(2*second))) {
			// ====== task body ======
			Printf("test %d = %d\n", id, int(i/mod))
			// ======== end =========

			endtime = time.Now()
			Printf("ID %d ", id)
			Println("end Time:   " + endtime.String() + "\n")

			mid = Getmid()
			// update deadline
			taskCurr[mid].ddl = taskCurr[mid].ddl.Add(time.Duration(taskCurr[mid].timespec.p))

			// turn on successors (successors have ddl earlier than runnable)
			// put successors into runnable list, inform other threads to check
			if taskCurr[mid].succHead != nil {
				succPut(taskCurr[mid])
				asyncSig = [...]int{1}
			}

			// ======== switch out (yield) =======
			if runnableCheck() == false { // check runnable task. if jumped, start (not return to 1)
				// no switch, no need for Getmid() here
				taskCurr[mid] = &taskIdle[mid]
				Goswitch(uint32(taskCurr[mid].qid))
			}

			// ============ switch in  ===========
			starttime = time.Now()
			Printf("ID %d ", id)
			Println("Start Time:   " + starttime.String())
		}
/*
		if i%mod == 0 {
			Printf("test %d = %d\n", id, int(i/mod))
		}
*/
	}
        Println("exit task")
}


func testPrint(arg unsafe.Pointer) {
	task := (* Task) (arg)

	Printf("ID: %d    ", task.taskid)
	Println("time-out: " + time.Now().String())
}

func syncTimer() {

	// get tid
	tid := int( Gettid() )
        Printf("Timer: tid = %d\n", tid)

	for {
		lt.Sync()
	}
}

/*
func setTimer() {
	f := testPrint // timeout func handler
	arg := 9
	TimerNode := lt.Create()
	period := time.Duration(period) // 1s

	lt.Set(TimerNode, time.Now(), period, f, arg, true) // start timer
}
*/
