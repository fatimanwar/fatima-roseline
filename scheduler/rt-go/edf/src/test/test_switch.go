// test.go
package main

import (
. "runtime"
. "fmt"
  "time"
//  "image"
//_ "image/jpeg" // Register JPEG format
//  "image/png" // register the PNG format with the image package
//  "os"
)

func testRT1(c chan bool) {
/*
        infile, err := os.Open("/home/eric/Pictures/lena.jpg")
        if err != nil {
            Printf("reading pic error\n")
    	}

        src, _, err := image.Decode(infile)
        if err != nil {
            Printf("decoding pic error\n")
        }

        // Create a new grayscale image
        bounds := src.Bounds()
        image.NewGray(bounds)
*/
	mod := 100000000

	ret := CheckRT()
	Printf("check 1=%d\n", ret)

//	pid := Getprocid()
//        Printf("pid=%d\n", pid)

	// get tid
	//qid := Getqid()
	/// tid_os := uintptr(tid)
	//Printf("runq id=%d\n", qid)

	// print old sched
	//sched  := Getsched(tid)
	//Printf("old sched=%d\n", sched)
	
	// set sched
	//Setsched(tid, 2, 1) // tid, policy, priority
	
	// print new sched
	//sched  = Getsched(tid)
	//Printf("new sched=%d\n", sched)


        for i := 0;  ; i++ {

		if(i == 2*mod){
			starttime := time.Now()
			Goswitch(uint32(qid2))
			Println("Start Time: " + starttime.String())
		}


		if i%mod == 0 {
			Printf("test 1 = %d\n", int(i/mod))
		}
	}

	Println("exit 1")
	// c <- true
}

func testRT2(c chan bool) {
	endtime := time.Now()
	Println("End Time:   " + endtime.String())

	mod := 100000000

	ret := CheckRT()
	Printf("check 2=%d\n", ret)
        for i := 0; ; i++ {

		if(i == 2*mod){
			Goswitch(uint32(qid1))
		}

		if i%mod == 0 {
			Printf("test 2 = %d\n", int(i/mod))
		}
	}
        Println("exit 2")
	// c <- true
}

func testRT3(c chan bool) {
	mod := 100000000

	ret := CheckRT()
	Printf("check 3=%d\n", ret)

        for i := 0;  ; i++ {

		if(i == 2*mod){
			starttime := time.Now()
			Goswitch(uint32(qid4))
			Println("Start Time: 3" + starttime.String())
		}


		if i%mod == 0 {
			Printf("test 3 = %d\n", int(i/mod))
		}
	}

	Println("exit 3")
	// c <- true
}

func testRT4(c chan bool) {
	endtime := time.Now()
	Println("End Time: 4  " + endtime.String())

	mod := 100000000

	ret := CheckRT()
	Printf("check 4=%d\n", ret)
        for i := 0; ; i++ {

		if(i == 2*mod){
			Goswitch(uint32(qid3))
		}

		if i%mod == 0 {
			Printf("test 4 = %d\n", int(i/mod))
		}
	}
        Println("exit 4")
	// c <- true
}
 
var qid1, qid2, qid3, qid4 int32

func main() {
	qid1 = 0
	qid2 = 1
	qid3 = 3
	qid4 = 4

	c1 := make(chan bool)
//	c2 := make(chan bool)
  
  	SetRT(qid1, 0, 0, true)
	go testRT1(c1)

	SetRT(qid2, 0, 0, false)
	go testRT2(c1)

  	SetRT(qid3, 1, 0, true)
	go testRT3(c1)

	SetRT(qid4, 1, 0, false)
	go testRT4(c1)

	
	//<- c1
	
	// ret := CheckRT()
	// fmt.Printf("result=%d\n", ret)

         for {
	}
        Println("exit main")
	//<- c2
} 
