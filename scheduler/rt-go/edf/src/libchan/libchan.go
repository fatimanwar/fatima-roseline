package libchan

import (
	. "libtask"
	. "sync/atomic"
	. "runtime"
	. "fmt"
)

// ATT: the lock here is not perfectly safe!!!
// ATT: successors can not be scheduled by timer. Need to add 'task state = working/waiting/idle/channel...' 

// sendq/recvq can at most has 1 task
// it is true for synchronization, 1 channel is only used by a pair of tasks
// Multiple channel avoids unnecessary task switching
type Chan struct {
	bufsize int
	data    int
	//buf     [10]int  // TODO: buffered/NB channel
	//head    int32
	//tail    int32
	sendq   *ChanTask
	recvq   *ChanTask
	lock    int32
}

type ChanTask struct {
	id      int   // for sending task, the sequence id of this channel, used to send data
	task    *Task
	next    *ChanTask
}

// ================== chan ==========================

func Chansend(data int, ch *Chan) {
	mid := Getmid()

	if ch.bufsize == 0 { // sync channel
		// store data
		for ; !CompareAndSwapInt32(&ch.lock, 0, 1) ; {} // lock
		ch.data = data
		if ch.recvq == nil { // no recv task
			// enqueue
			newtask := new(ChanTask)
			newtask.next = ch.sendq
			ch.sendq = newtask
			newtask.task = TaskCurr[mid]

			StoreInt32(&ch.lock, 0) // unlock
			YieldTask() // block & switch
			// someone woke us up.
			return
		} else {
			taskWait := ch.recvq.task
			i        := ch.recvq.id
			ch.recvq = ch.recvq.next
			StoreInt32(&ch.lock, 0) // unlock
			taskWait.CountRecv --
			taskWait.ChanBuf[i] = data
			if taskWait.CountRecv == 0 {
				RunnablePut(taskWait) 	// wake up recv task
			}
			// no block/switch
			return
		}
	} else { // async channel
		Println("NB Chan: Under Construction")
	}
}

func Chanrecv(ch *Chan) int {
	mid := Getmid()

	if ch.bufsize == 0 { // sync channel
		for ; !CompareAndSwapInt32(&ch.lock, 0, 1) ; {} // lock
		if ch.sendq == nil { // no send task
			// enqueue
			newtask := new(ChanTask)
			newtask.next = ch.recvq
			ch.recvq = newtask
			newtask.task = TaskCurr[mid]
			newtask.id   = 0
			TaskCurr[mid].CountRecv = 1 // when current task call Chanrecv, CountRecv must be 0
			TaskCurr[mid].ChanBuf = make([]int, 1)	

			StoreInt32(&ch.lock, 0) // unlock
			YieldTask() // block & switch
			// someone woke us up.
		} else {
			taskWait := ch.sendq.task
			ch.sendq = ch.sendq.next
			StoreInt32(&ch.lock, 0) // unlock
			taskWait.CountRecv --
			// wake up recv task
			if taskWait.CountRecv == 0 {
				RunnablePut(taskWait) 	// wake up recv task
			}
			RunnablePut(taskWait)   // wake up send task
			// no block/switch
		}
		data := ch.data
		ch.data = 0	// is it needed?		
		return data
	} else { // async channel
		Println("NB Chan: Under Construction")
		return -1
	}	
}

// send to multiple channel
func ChansendM(data int, chM ...*Chan) {
	var taskWait *Task
	var id int

	mid    := Getmid()
	TaskCurr[mid].CountRecv = len(chM) // init reference count

	for _, ch := range chM {
		for ; !CompareAndSwapInt32(&ch.lock, 0, 1) ; {} // lock
		ch.data = data
		if ch.recvq == nil { // no send task
			// enqueue
			newtask := new(ChanTask)
			newtask.next = ch.sendq
			ch.sendq = newtask
			newtask.task = TaskCurr[mid]
		} else {
			taskWait = ch.recvq.task
			id       = ch.recvq.id
			ch.recvq = ch.recvq.next
			taskWait.CountRecv --
			taskWait.ChanBuf[id] = data
			// wake up recv task
			if taskWait.CountRecv == 0 {
				RunnablePut(taskWait) 	// wake up recv task
			}
			// no block/switch
			TaskCurr[mid].CountRecv --
		}
		StoreInt32(&ch.lock, 0) // unlock
	}

	if TaskCurr[mid].CountRecv !=0 {
		YieldTask() // block & switch
		// someone woke us up.
	}	
}

// receive from multiple channels
func ChanrecvM(chM ...*Chan) []int {
	var taskWait *Task 

	mid    := Getmid()
	length := len(chM)
	TaskCurr[mid].CountRecv = length // init reference count
	TaskCurr[mid].ChanBuf = make([]int, length)

	for i, ch := range chM {
		for ; !CompareAndSwapInt32(&ch.lock, 0, 1) ; {} // lock
		if ch.sendq == nil { // no send task
			// enqueue
			newtask := new(ChanTask)
			newtask.next = ch.recvq
			ch.recvq = newtask
			newtask.task = TaskCurr[mid]
			newtask.id   = i
		} else {
			taskWait = ch.sendq.task
			ch.sendq = ch.sendq.next

			taskWait.CountRecv --
			// wake up recv task
			if taskWait.CountRecv == 0 {
				RunnablePut(taskWait) 	// wake up recv task
			}

			// no block/switch
			TaskCurr[mid].CountRecv --
			TaskCurr[mid].ChanBuf[i] = ch.data
			ch.data = 0     // is it needed?
		}
		StoreInt32(&ch.lock, 0) // unlock
	}

	if TaskCurr[mid].CountRecv !=0 {
		YieldTask() // block & switch
		// someone woke us up.
	}	
	
	mid = Getmid() // M has been changed!
	return TaskCurr[mid].ChanBuf
}
