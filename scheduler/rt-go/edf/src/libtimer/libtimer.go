package libtimer

import (
	. "time"
	//. "fmt"
	"unsafe"
	. "sync/atomic"	
	//"os"
	//"strconv"
)

type TimerNode struct {
	t Time
	d Duration
	f func(unsafe.Pointer)
	//arg int  // just use int argument
	arg unsafe.Pointer 
	isperiod bool

	next, prev *TimerNode
}

var lockTimer int32 = 0

var timerOnHead *TimerNode = nil    // head of On Timer list
var timerOffHead *TimerNode = nil   // head of Off Timer list

//var Testfile *os.File

// create a timer
func Create() *TimerNode {
	if timerOffHead == nil { // grab from off timer
		timer := new(TimerNode)
		timer.next = nil
		timer.prev = nil
		return timer
	} else {
		timer := timerOffHead
		timerOffHead = timer.next
		timer.next = nil
		// timer.prev = nil
		return timer
	}
}

var old Time

// check timer
// lock area takes ~500ns, it is ~200ns if no 'insert'
func Sync() {
	if timerOnHead != nil {
		//if getLock(&lockTimer) {
		if CompareAndSwapInt32(&lockTimer, 0, 1) {
			//start := Now()
			if Now().After(timerOnHead.t) { // time out
				// remove
				timer := timerOnHead
				timerOnHead = timerOnHead.next
				if timerOnHead != nil { 
					timerOnHead.prev = nil 
				} 
				// else if !timer.isperiod { return }
				
				// update timer list
				if timer.isperiod {
					timer.t = timer.t.Add(timer.d) // next time out				
					insert(timer) // timer.next will be changed here
				}
				StoreInt32(&lockTimer, 0)

				//diff := start.Sub(old).Nanoseconds()
				//Testfile.WriteString(strconv.Itoa(int(diff)) + "\n")
				//old = start

				timer.f(timer.arg)
			} else {
				//diff := start.Sub(old).Nanoseconds()
				//Testfile.WriteString(strconv.Itoa(int(diff)) + "\n")
				//old = start

				StoreInt32(&lockTimer, 0)
				return 
			}		
		} else { return }	
	}
}

// Set timer
// TODO: 'prev' is used to speed up 'Set'
func Set(timer *TimerNode, t Time, d Duration, f func(unsafe.Pointer), arg unsafe.Pointer, isperiod bool) {
	timer.t = t
	timer.d = d
	timer.f = f
	timer.arg = arg
	timer.isperiod = isperiod
	
	// adjust timer, now it is the same as 'remove' 
	// TODO: compare new timer with old timer, insert from old timer in one direction
	// remove(timer)
 
        // remove
	for ; !CompareAndSwapInt32(&lockTimer, 0, 1) ; {}
	if timer.next != nil && timer.prev != nil { 
		timer.prev.next = timer.next 
	} else if timer.prev != nil { 
		timer.prev.next = nil    // tail
	} else if timer.next != nil {
		timerOnHead = timer.next // head
		timer.next.prev = nil
	}

	insert(timer)
	StoreInt32(&lockTimer, 0)
}

func insert(timer *TimerNode) {
	timer.prev = nil
	timer.next = nil

	if timerOnHead == nil {
		timerOnHead = timer
	} else {
		for timerCur := timerOnHead; ; timerCur = timerCur.next {
			if timer.t.Before(timerCur.t) {
				timer.prev = timerCur.prev
				timer.next = timerCur
				if timerCur.prev != nil {
					timerCur.prev.next = timer
				} else {
					timerOnHead = timer
				}
				timerCur.prev = timer
				return
			}

			if timerCur.next == nil {
				timerCur.next = timer
				timer.prev = timerCur
				return
			}			
		}
	}
}

// Remove timer
func Remove(timer *TimerNode)  {
	for ; !CompareAndSwapInt32(&lockTimer, 0, 1) ; {}
	if timer.next != nil && timer.prev != nil { 
		timer.prev.next = timer.next 
	} else if timer.prev != nil { 
		timer.prev.next = nil    // tail
	} else if timer.next != nil {
		timerOnHead = timer.next // head
		timer.next.prev = nil
	} 
	//else { timerOnHead = nil }

	timer.next = timerOffHead
	timerOffHead = timer
	timer.prev = nil
	// prev is nil for all nodes in Off list
	StoreInt32(&lockTimer, 0)
}
