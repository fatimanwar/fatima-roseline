package libcgo

// #cgo LDFLAGS: libcgo.so
// #include <stdio.h>     
// #include <stdlib.h> 
// #include <sched.h>
// #include "libcgo.h"
import "C"     
import "unsafe"
     
func Print(s string) {        
	cs := C.CString(s)           
	C.fputs(cs, (*C.FILE)(C.stdout))              
	C.free(unsafe.Pointer(cs))     
}


func GetSched(pid int) int {
        pid2 := C.__pid_t(pid)
	sched := int(C.sched_getscheduler(pid2))
	return sched
}


func SetSched(pid int, policy int, priority int) int {
	return int( C.setSched(C.int(pid), C.int(policy), C.int(priority)) )
}

func CreateTimerFd(clockid int, flags int) int{
	timerfd:= int( C.createTimerFd(C.int(clockid), C.int(flags)) )
	return timerfd
}

func SetTimerFd(timerfd int, flags int, v_s int, v_ns int, i_s int, i_ns int) {
	C.setTimerFd(C.int(timerfd), C.int(flags), C.int(v_s), C.int(v_ns), C.int(i_s), C.int(i_ns)) 
}

func TestTimer(timerfd int, id int) {
	C.testTimer(C.int(timerfd), C.int(id)) 
}

func DeamonTimer(timerfdUp int, timerfdDw int, id int, tid int) {
	C.deamonTimer(C.int(timerfdUp), C.int(timerfdDw), C.int(id), C.int(tid)) 
}

func BindCPU(coreid int) {
	C.bindCPU(C.int(coreid))
}

func CheckCPU() {
	C.checkCPU()
}
