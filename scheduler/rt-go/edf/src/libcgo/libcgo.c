#include "libcgo.h"

#define _GNU_SOURCE 
#include <pthread.h>
#include <stdio.h>     
#include <stdlib.h> 
#include <sched.h>
#include <sys/timerfd.h>
#include <stdint.h>        /* Definition of uint64_t */

// change os scheduler
int setSched(int pid, int policy, int priority) {
  struct sched_param param;
  param.sched_priority = priority;
  return sched_setscheduler(pid, policy, &param);
}

// timer (file descriptor)
int createTimerFd(int clockid, int flags) {
  int timerfd = timerfd_create(CLOCK_REALTIME, 0); 
  if(timerfd == -1){
    printf("Create timer fails\n");
  }
  return timerfd;
}

void setTimerFd(int timerfd, int flags, int v_s, int v_ns, int i_s, int i_ns) {
  struct itimerspec new_value;  
  new_value.it_value.tv_sec = v_s;    // initial time
  new_value.it_value.tv_nsec = v_ns;
  new_value.it_interval.tv_sec = i_s; //interval time
  new_value.it_interval.tv_nsec = i_ns;

  if(timerfd_settime(timerfd, 0, &new_value, NULL) == -1) 
    printf("timerfd_settime"); 
}

void testTimer(int timerfd, int id) {
  uint64_t exp; 
  int len; 
  printf("Timer %d: test start\n", id);
  while((len = read(timerfd, &exp, sizeof(uint64_t))) > 0) { 
    printf("Timer %d: timeout %llu\n", id, (unsigned long long) exp); // 
    //print_elapsed_time();
  } 
  
  close(timerfd); 
}

void printTime(struct timespec *start, struct timespec *curr, int *first_call){
   int secs, nsecs;

   if (*first_call) {
     (*first_call) = 0;
     if (clock_gettime(CLOCK_MONOTONIC, start) == -1)
       printf("Error: clock_gettime\n");
   }

   if (clock_gettime(CLOCK_MONOTONIC, curr) == -1)
     printf("Error: clock_gettime\n");

   secs = (*curr).tv_sec - (*start).tv_sec;
   nsecs = (*curr).tv_nsec - (*start).tv_nsec;
   if (nsecs < 0) {
       secs--;
       nsecs += 1000000000;
   }
   printf("%ds %dus\n", secs, nsecs/1000);

}

void deamonTimer(int timerfdUp, int timerfdDw, int id, int tid) {
  uint64_t exp; 
  int len; 
  int schedVal, sched, priority;
  struct timespec start;
  struct timespec curr;
  int first_call = 1;

  int schedUp = 1;
  int priorityUp = 30;
  int schedDw = 0;
  int priorityDw = 0;  

  printf("Deamon %d start\n", id);

  while(1) { 
    // timer up
    if(read(timerfdUp, &exp, sizeof(uint64_t)) != 8) {
      printf("Deamon %d: Timer read error", id);
      break;
    } else {
      printf("\nDeamon %d: UP %llu  ", id, (unsigned long long) exp); 
      printTime(&start, &curr, &first_call);
      checkCPU();
      // change scheduler of thread 'tid'
      if (setSched(tid, schedUp, priorityUp) != 0) {
	printf("Deamon %d: sched change fail\n", id);
      } else {
	schedVal = sched_getscheduler(tid);
	printf("Deamon %d: tid %d sched = %d\n\n", id, tid, schedVal);
      }    
    }

    // timer up
    if(read(timerfdDw, &exp, sizeof(uint64_t)) != 8) {
      printf("Deamon %d: Timer read error", id);
      break;
    } else {
      printf("\nDeamon %d: DW %llu  ", id, (unsigned long long) exp); 
      printTime(&start, &curr, &first_call);
      checkCPU();
      // change scheduler of thread 'tid'
      if (setSched(tid, schedDw, priorityDw) != 0) {
	printf("Deamon %d: sched change fail\n", id);
      } else {
	schedVal = sched_getscheduler(tid);
	printf("Deamon %d: tid %d sched = %d\n\n", id, tid, schedVal);
      }    
    }

  } 
  
  close(timerfdUp); 
  close(timerfdDw); 
}

void bindCPU(int coreid) {
  // bind to a processor
  //unsigned long mask = 1; /* processor 0 */
  cpu_set_t my_set; /* Define your cpu_set bit mask. */
  CPU_ZERO(&my_set); /* Initialize it all to 0, i.e. no CPUs selected. */
  CPU_SET(coreid, &my_set); /* set the bit that represents core 1:  0 0 0 1. */

  // bind process to processor
  if (sched_setaffinity(0, sizeof(my_set), &my_set) < 0) {
    printf("Error: sched_setaffinity\n");
  }
  
  /*
  pthread_t thread = pthread_self();
  if (pthread_setaffinity_np(thread, sizeof(cpu_set_t), &my_set) < 0) {
    printf("Error: sched_setaffinity\n");
  }
  */
}

void checkCPU(){
  cpu_set_t my_set;
  //pthread_t thread = pthread_self();
  int j;

  if (sched_getaffinity(0, sizeof(my_set), &my_set) <0 ) {
    printf("Error: sched_setaffinity\n");
  } else {
    printf(" CPU: ");
    for (j = 0; j < CPU_SETSIZE; j++)
      if (CPU_ISSET(j, &my_set)) printf(" %d", j);
    printf("\n");
  }

}
