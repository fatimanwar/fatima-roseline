int setSched(int pid, int policy, int priority);
int createTimerFd(int clockid, int flags);
void setTimerFd(int timerfd, int flags, int v_s, int v_ns, int i_s, int i_ns);
void testTimer(int timerfd, int id);
void deamonTimer(int timerfdUp, int timerfdDw, int tid, int flags);
void bindCPU(int coreid);
void checkCPU();
