package libtask

import(
	"time"
	lt "libtimer"	
	"unsafe"
	. "sync/atomic"
	. "runtime"
	. "fmt"
)

type TimeSpec struct {
	S, P, D int64   // starting time/ period/ relative deadline
}

type Task struct {
	Timespec TimeSpec
	Taskid, Mid, Qid int
	Ddl time.Time // absolute deadline, used for EDF
	Next *Task

	// for multiple channels
	CountRecv int // =0 means no need to block
	ChanBuf   []int // slice
	
	isworking bool // does the work finish
	IsIdle    bool // if it is background task (waiting state)
}

type Worker struct {
	Ddl time.Time // absolute deadline, used for EDF
	Mid int
	Prev *Worker
	Next *Worker
}

var second int64 = 1000000000
var timespIdle TimeSpec = TimeSpec{S:0*second, P:0*second, D:9999*second} // TODO: idle task should be removed
var TaskIdle = Task{Timespec:timespIdle, Taskid:-1, Qid:0, IsIdle:true}

var TaskCurr  [10]*Task
var workerAll [10]Worker
var SigAsync  [10]int32 // asynchronous signals between M
var SigAllSet [10]int32

var runnableHead *Task    // head of runnable task list
var workerHead   *Worker  // a sorted (ddl) list of all threads

var lockRunq, lockWorker int32

func TaskInit () {
	// init worker
	for i := 0; i < 10; i++ {
		workerAll[i] = Worker{Mid:i}
		if i == 0 {
			workerHead = &workerAll[i]
		} else {
			workerAll[i-1].Next = &workerAll[i]
			workerAll[i].Prev   = &workerAll[i-1]
		}
		SigAllSet[i] = int32(i)
	}
	// init idle ddl
	TaskIdle.Ddl = time.Now().Add(time.Duration(10000*second))
}

func Schedule(task *Task) {
	f := taskSwitch // timeout func handler
	//f := testPrint
	arg := unsafe.Pointer(task)
	var isperiod bool
	if task.Timespec.P == 0 { isperiod = false
	} else { isperiod = true }
	period := time.Duration(task.Timespec.P)

	TimerNode := lt.Create()

	lt.Set(TimerNode, time.Now().Add(time.Duration(task.Timespec.S)), period, f, arg, isperiod) // start timer	
}

// for timer
func taskSwitch(arg unsafe.Pointer) {
	waitTask := (* Task) (arg)
	mid := Getmid()

	//Printf("wait ID: %d ; curr ID: %d  time-out: %s", waitTask.qid, TaskCurr[0].qid, time.Now().String())

	// do not switch to itself
	if waitTask.Taskid == TaskCurr[mid].Taskid { return }
	if waitTask.isworking == true {
		Printf("Task: %d ===> ddl miss\n", waitTask.Taskid)
		return
	}
	waitTask.isworking = true

	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
	RunnablePut(waitTask)
	StoreInt32(&lockRunq, 0)

	if waitTask.Ddl.Before(workerHead.Ddl) { // compare with the latest ddl
		SigAsync[workerHead.Mid] = 1  // inform all workers to check
	}
}

// check if there is runnable task, use it before yielding
func RunnableCheck() bool {
	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
	if runnableHead == nil { 
		//Printf("runnable queue is empty\n")
		StoreInt32(&lockRunq, 0)
		return false
	} else {
		task := runnableHead
		runnableHead = runnableHead.Next // remove
		StoreInt32(&lockRunq, 0)

		mid := Getmid()
		TaskCurr[mid] = task
		SetWorker (mid, TaskCurr[mid].Ddl)
		Goswitch(uint32(task.Qid))
		return true
	}
}

// check if there is runnable task, async
func RunnableCheck_async() bool {
	mid := Getmid()
	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
	if runnableHead == nil || !runnableHead.Ddl.Before(TaskCurr[mid].Ddl) {
		StoreInt32(&lockRunq, 0)
		return false
	} else {
		task := runnableHead
		runnableHead = runnableHead.Next // remove
		RunnablePut(TaskCurr[mid])
		StoreInt32(&lockRunq, 0)

		TaskCurr[mid] = task
		SetWorker (mid, TaskCurr[mid].Ddl)
		Goswitch(uint32(task.Qid))
		return true
	} 
}

// put runnable task into queue
func RunnablePut(task *Task) {
	if task.IsIdle == true { return } // do not put 'idle' (background) task into runnable queue

	task.Next = nil	
	if runnableHead == nil {
		runnableHead = task
	} else if task.Ddl.Before(runnableHead.Ddl) {
		task.Next = runnableHead
		runnableHead = task
	} else {
		for taskCur := runnableHead; ; taskCur = taskCur.Next {
			if taskCur.Next == nil {
				taskCur.Next = task
				return
			}		

			if task.Ddl.Before(taskCur.Next.Ddl) {
				task.Next = taskCur.Next
				taskCur.Next = task
				return
			}	
		}
	}
}

// change it into GroupPut()
// put successor tasks into runnable queue
// successors have the earilest ddl
//func SuccPut(task *Task) {
//	for ; !CompareAndSwapInt32(&lockRunq, 0, 1) ; {}
//	head := runnableHead
//	runnableHead = task.SuccHead
//	task.SuccTail.Next = head
//	StoreInt32(&lockRunq, 0)
//}

// reset ddl of workers
// to find the worker with earliest ddl to preempt
func SetWorker (mid uint32, ddl time.Time) {
	worker := &workerAll[mid]
	worker.Ddl = ddl
	for ; !CompareAndSwapInt32(&lockWorker, 0, 1) ; {}
	// deletion
	if worker.Prev == nil {
		workerHead = worker.Next
		if worker.Next != nil { worker.Next.Prev = nil }
	} else {
		worker.Prev.Next = worker.Next
		if worker.Next != nil { worker.Next.Prev = worker.Prev }
	}
	// insertion
	if worker.Ddl.After(workerHead.Ddl) {
		worker.Next = workerHead
		workerHead.Prev = worker
		worker.Prev = nil
		workerHead = worker
		StoreInt32(&lockWorker, 0)
	} else {
		for workerCur := workerHead; ; workerCur = workerCur.Next {
			if workerCur.Next == nil {
				workerCur.Next = worker
				worker.Prev = workerCur
				worker.Next = nil
				StoreInt32(&lockWorker, 0)
				return
			}
			if worker.Ddl.After(workerCur.Next.Ddl) {
				worker.Next = workerCur.Next
				worker.Prev = workerCur
				workerCur.Next.Prev = worker
				workerCur.Next = worker
				StoreInt32(&lockWorker, 0)
				return
			}
		}
	}
}

func YieldTask() {
	mid := Getmid()

	// update deadline
	TaskCurr[mid].Ddl = TaskCurr[mid].Ddl.Add(time.Duration(TaskCurr[mid].Timespec.P))
	TaskCurr[mid].isworking = false

	if RunnableCheck() == false {
		TaskCurr[mid] = &TaskIdle
		SetWorker (mid, TaskIdle.Ddl)
		Yieldidle()
		//SetWorker (mid, TaskCurr[mid].Ddl)
		//Goswitch(uint32(TaskCurr[mid].Qid))
	}	
}
