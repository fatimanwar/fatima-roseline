export GOPATH=$GOPATH:/home/eric/wg/edf
export GOBIN=/home/eric/wg/edf/bin
export GOMAXPROCS=100
export GOGC=off

gcc -shared -fPIC ../libcgo/libcgo.c -o ../libcgo/libcgo.so

go build ../libcgo/libcgo.go
go build main.go

go install main
