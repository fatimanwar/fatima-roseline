package main

import (
	. "fmt"
	"time"
	lt "libtimer"
	//lc "libcgo"
	. "libtask"
	. "libchan"
	. "runtime"
	"unsafe"
	. "sync/atomic"	
	//"runtime/pprof"
	//"os"	
)

var second int64 = 1000000000
var modt, modw int = 300000, 30000000
var lockPrint int32
var lockcpu int32

// tasks
var timesp1 TimeSpec = TimeSpec{S:6*second, P:10*second, D:10*second} 
var timesp2 TimeSpec = TimeSpec{S:3*second, P:0*second, D:6*second} 
var task [10]Task
var chanrt [2]Chan

// test case1: dense tasks
func goinit_spawn() {
	var qid int32
	nowTime := time.Now()
	for i := 0; i < 10; i++ {
		qid = int32(10 + i)
		task[i] = Task{Timespec:timesp1, Taskid:i, Qid:int(qid)}
		task[i].Ddl = nowTime.Add(time.Duration(task[i].Timespec.D)) // initialize ddl
		// SetRT(qid, mid, pid, isnewm)
		SetRT(qid, 0, 0, false)
		//go taskChSend(1, &chanrt)
		go taskfn(i)
	}
}

// test case2: multiple channel
func goinit_ch() {
	var qid int32
	nowTime := time.Now()

	qid = 10 + 0
	task[0] = Task{Timespec:timesp1, Taskid:0, Qid:int(qid)}
	task[0].Ddl = nowTime.Add(time.Duration(task[0].Timespec.D)) // initialize ddl
	SetRT(qid, 0, 0, false)
	go taskChSendM(0, &chanrt[0], &chanrt[1])

	qid = 10 + 1
	task[1] = Task{Timespec:timesp2, Taskid:1, Qid:int(qid)}
	task[1].Ddl = nowTime.Add(time.Duration(task[1].Timespec.D)) // initialize ddl
	SetRT(qid, 0, 0, false)
	go taskChRecv(1, &chanrt[0])

	qid = 10 + 2
	task[2] = Task{Timespec:timesp2, Taskid:2, Qid:int(qid)}
	task[2].Ddl = nowTime.Add(time.Duration(task[2].Timespec.D)) // initialize ddl
	SetRT(qid, 0, 0, false)
	go taskChRecv(2, &chanrt[1])
}

func main() {
	//f, _ := os.Create("profile_file")
	//pprof.StartCPUProfile(f)
	//defer pprof.StopCPUProfile()

	//lt.Testfile, _ = os.Create("test_timer")
	//defer lt.Testfile.Close()

	TaskInit()   // for tasks worker
	goinit_ch()     // for goroutine	

	// create threads (M)
	for i := 0; i < 2; i ++ { // qid == i
		SetRT(int32(i), int32(i), 0, true)
		go waiting(i, i)

		for k := 0; k < modw; k++ {} // need to waste some time between creating thread. why?
	}

	// get tid
	//tid := int( Gettid() )
        //Printf("Main: tid = %d\n", tid)

	for i := 0; i < 3; i++ { Schedule(&task[i]) }

	endtime := time.Now().Add(time.Duration(100*second))
	for ; time.Now().Before(endtime); {	}

        Println(" ==== Exit main ====")

}

func testPrint(arg unsafe.Pointer) {
	//task := (* Task) (arg)
	//mid := Getmid()
	//Printf("ID: %d  mid: %d   ", task.taskid, mid)
	Println("time-out: " + time.Now().String())
}

func PrintlnLock(a ...interface{}) {
	for ; !CompareAndSwapInt32(&lockPrint, 0, 1); {}
	Println(a ...)
	StoreInt32(&lockPrint, 0)
}

func PrintfLock(format string, a ...interface{}) {
	for ; !CompareAndSwapInt32(&lockPrint, 0, 1); {}
	Printf(format, a ...)
	StoreInt32(&lockPrint, 0)
}

// ===================== test case ==========================

func waiting(id int, core int) {
	//for ; !CompareAndSwapInt32(&lockcpu, 0, 1) ; {}
	//lc.BindCPU(core)
	//StoreInt32(&lockcpu, 0)
	//Printf("Check RT: id: %d, RT: %d\n", id, CheckRT())

	//for j := 0; j < 100*mod; j++ {}

	mid := Getmid()
	Setidle()
	TaskCurr[mid] = &TaskIdle
	SetWorker (mid, TaskIdle.Ddl)
        
	// get tid
	tid := int( Gettid() )
        PrintfLock("Waiting: tid = %d\n", tid)

        for i := 0;  ; i++ {
		lt.Sync()
		if CompareAndSwapInt32(&SigAsync[mid], 1, 0) { RunnableCheck_async() }

		for k := 0; k < 100; k ++ {}

		if i% modw == 0 {
			mid = Getmid()
			PrintfLock("waiting %d mid %d = %d\n", id, mid, int(i/modw))
			//lc.CheckCPU()
		}
	}
	PrintlnLock("exit waiting")
}

func taskfn(id int) {
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	var mid uint32
	PrintfLock("ID %d first\n", id)
	PrintlnLock("first Time:   " + time.Now().String() + "\n")

        for i := 0; ; i++ {
		//Printf("Check RT: id: %d, RT: %d, loop:%d\n", id, CheckRT(), i)
		
		for k := 0; k < 100; k ++ {}
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if CompareAndSwapInt32(&SigAsync[mid], 1, 0) { RunnableCheck_async() } // response to async signal
		// ========= switch in ==========

		if(i % modt == 0){
		//if time.Now().After(starttime.Add(time.Duration(2*second))) {

			// ====== task body ======
			//Printf("test %d = %d\n", id, int(i/mod))
			// ======== end =========

			PrintfLock("ID %d end\n", id)
			PrintlnLock("end Time:   " + time.Now().String() + "\n")

			//lc.CheckCPU()

			// ======== switch out (yield) =======
			YieldTask()

			// ============ switch in  ===========
			PrintfLock("ID %d  start\n", id)
			PrintlnLock("Start Time:   " + time.Now().String())
		}
	}
        PrintlnLock("exit task")
}

func taskChSend(id int, ch *Chan) {
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	var mid uint32
	starttime := time.Now()

        for i := 0; ; i++ {
			
		for k := 0; k < 100; k ++ {}
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if CompareAndSwapInt32(&SigAsync[mid], 1, 0) { RunnableCheck_async() } // response to async signal
		// ========= switch in ==========

		//if(i % (1*mod) == 0){
		if time.Now().After(starttime.Add(time.Duration(1*second))) {
			starttime = time.Now()
			// ====== task body ======
			//Printf("test %d = %d\n", id, int(i/mod))
			// ======== end =========

			PrintfLock("ID %d end\n", id)
			PrintlnLock("end Time:   " + time.Now().String() + "\n")

			//lc.CheckCPU()
			PrintlnLock("Send on CH")
			Chansend(9, ch)

			// ======== switch out (yield) =======				
			YieldTask()
		}

	}

        PrintlnLock("exit task")
}

func taskChSendM(id int, chM ...*Chan) {
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	var mid uint32
	starttime := time.Now()

        for i := 0; ; i++ {
			
		for k := 0; k < 100; k ++ {}
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if CompareAndSwapInt32(&SigAsync[mid], 1, 0) { RunnableCheck_async() } // response to async signal
		// ========= switch in ==========

		//if(i % (1*mod) == 0){
		if time.Now().After(starttime.Add(time.Duration(1*second))) {
			starttime = time.Now()
			// ====== task body ======
			//Printf("test %d = %d\n", id, int(i/mod))
			// ======== end =========

			PrintfLock("ID %d end\n", id)
			PrintlnLock("end Time:   " + time.Now().String() + "\n")

			//lc.CheckCPU()
			PrintlnLock("Send on MCH")
			ChansendM(9, chM[0], chM[1])

			// ======== switch out (yield) =======				
			YieldTask()
		}

	}

        PrintlnLock("exit task")
}

func taskChRecv(id int, ch *Chan) {
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	var mid uint32
	starttime := time.Now()

        for i := 0; ; i++ {
			
		for k := 0; k < 100; k ++ {}
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if CompareAndSwapInt32(&SigAsync[mid], 1, 0) { RunnableCheck_async() } // response to async signal
		// ========= switch in ==========

		if time.Now().After(starttime.Add(time.Duration(1*second))) {
			starttime = time.Now()
			// ============ switch in  ===========
			PrintfLock("ID %d  start\n", id)
			PrintlnLock("Start Time:   " + time.Now().String())

			//lc.CheckCPU()
			PrintlnLock("Recv on CH")
			data := Chanrecv(ch)
			PrintfLock("Recv from CH: %d\n", data)

			PrintfLock("ID %d end\n", id)
			PrintlnLock("end Time:   " + time.Now().String() + "\n")

			mid = Getmid()

			// update deadline
			TaskCurr[mid].Ddl = TaskCurr[mid].Ddl.Add(time.Duration(TaskCurr[mid].Timespec.P))

			// do not yield, wait for next chan
		}

	}

        PrintlnLock("exit task")
}

func taskChRecvM(id int, chM ...*Chan) {
	//tid := int( Gettid() )
        //Printf("Task %d: tid = %d\n", id, tid)

	var mid uint32
	starttime := time.Now()

        for i := 0; ; i++ {
			
		for k := 0; k < 100; k ++ {}
		
		// ===== switch out (timer) =====
		lt.Sync()
		mid = Getmid()
		if CompareAndSwapInt32(&SigAsync[mid], 1, 0) { RunnableCheck_async() } // response to async signal
		// ========= switch in ==========

		if time.Now().After(starttime.Add(time.Duration(1*second))) {
			starttime = time.Now()
			// ============ switch in  ===========
			PrintfLock("ID %d  start\n", id)
			PrintlnLock("Start Time:   " + time.Now().String())

			//lc.CheckCPU()
			PrintlnLock("Recv on MCH")
			data := ChanrecvM(chM[0], chM[1])
			PrintfLock("Recv from MCH: %d, %d\n", data[0], data[1])

			PrintfLock("ID %d end\n", id)
			PrintlnLock("end Time:   " + time.Now().String() + "\n")

			mid = Getmid()

			// update deadline
			TaskCurr[mid].Ddl = TaskCurr[mid].Ddl.Add(time.Duration(TaskCurr[mid].Timespec.P))

			// do not yield, wait for next chan
		}

	}

        PrintlnLock("exit task")
}


